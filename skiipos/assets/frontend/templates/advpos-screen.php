
<div class="left_section">
    <div class="page_section">
        <div class="page_left_div page_top">
            <div class="page_tab">
                <div class="page_tab_d">Advance Booking <span class="pull-right"><i class="fa fa-calendar" aria-hidden="true"></i> {{dateval1_list}}</span></div>
            </div>
            <div class="page_tab_content">


                <div class="second-scrol">

                    <div class="cont_left scol-top">

                        <div class="nodatatxt" ng-if="list.length===0">No Records Found</div>

                        <div class="page_row" ng-repeat="(key, value) in list">

                            <div class="tab_con_box_1">
                                <div class="tab_box_1">

                                    <button class="button-all">
                                        <div class="button_box_left">


                                            <div ng-repeat="(x,y) in  value.package_iteamcount">


                                                <div ng-if="value.package_iteamcount[x] != ''" class="back-per-iner" style="width:{{ (y/value.dailyinventory_seats)*100 }}%; background-color:{{value.package_color[x]}};  " ></div>
                                            </div>

                                            <div class="app-package">
                                                <div class="session_name">Packages</div>
                                                <div class="session_time"><span class="left">{{value.dailyinventory_seats}} /0/{{value.prnum}}</span><span class="right">{{value.dailyinventory_from}}:{{value.dailyinventory_minfrom}}- {{value.dailyinventory_to}}:{{value.dailyinventory_minto}} Hrs</span></div>
                                            </div>
                                        </div>

                                    </button>
                                </div>
                            </div>





                            <div class="tab_con_box_2">
                                <div class="tab_box" ng-repeat="(k,v) in  value.package_name">
                                    <div class="button_box " style="background-color: {{ value.package_color[k] }}; color: {{ value.package_fontcolor[k] }}" ng-click="AddPackage(value,value.dailyinventory_id,value.package_id[k],value.package_name[k],value.package_price[k],value.dailyinventory_seats,value.dailyinventory_from,value.dailyinventory_to,value.dailyinventory_minfrom,value.dailyinventory_minto,value.dailyinventory_date,value.prnum)">
                                        <span class="type_text">{{v}}</span>
                                        <span class="price_text">Rs. {{ value.package_price[k] }}.00</span>
                                    </div>
                                </div>



                            </div>

                        </div>


                        <!--new More Menu-->
                        <!--NEw More Menu End-->

                    </div>




                </div>




                <div class="cont_right">
                    <div class="cont_right_tab">
                        <ul>

                            <li><div class="right_box clock_2"></div></li>
                            <li><div class="right_box calender_1"></div></li>
                            <li><div class="right_box r_box">Now</div></li>
                            <li>
                                <input id="datepicker12" data-date-format="dd/mm/yyyy" class="date-pick right_box calender" name="txtDepartDate" type="text" ng-model="txtDepartDate" ng-click="txtDepartDate" ng-blur="getFirstName()">
                                <span class="input-icon1"><i class="icon-calendar"></i></span>
                            </li>


                            <li><div class="right_box calender_2"></div></li>

                            <li><div class="right_box clock_4"></div></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>






        <div ng-include="'assets/frontend/templates/footer.php'"></div>

    </div>

</div>
<div ng-include="'assets/frontend/templates/right.php'"></div>
<div ng-include="'assets/frontend/templates/bottom.php'"></div>




</body>
</html>
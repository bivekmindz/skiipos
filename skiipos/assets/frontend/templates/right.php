<div class="right_section">
    <div class="page_right_section">
        <div class="page_right_div right_top1">


            <table class="account">
                <tr>
                    <th scope="col">Description</th>
                    <th scope="col">Value</th>
                </tr>

            </table>

            <div class="scrol-hover">
                <div class="min-hei">
                    <table class="account ">

                        <tr ng-repeat="(key, value) in packages  track by $index" id="red-col"
                            ng-class="{sel: $index == selected}">
                            <td ng-click="removebyid($index,value,value.inventory_id,value.packageId,value.package_name,value.package_price,value.inventory_seats,value.inventory_from,value.inventory_to,value.inventory_minfrom,value.inventory_minto)">{{value.package_name}}</td>
                            <td >Rs.{{value.package_price}}</td>
                        </tr>
                        <tr ng-repeat="i in [1, 2, 3, 4, 5,6,7,8]"  ng-if="i >packages.length"><td>&nbsp;&nbsp;</td><td></td></tr>




                    </table>


                </div>



            </div>



        </div>

        <div class="page_right_div right_top2">
            <table class="account1">

                <tr>
                    <td class="border_none">
                        <strong>Total- {{packages.length}}</strong>

                    </td>
                    <td style="border-left: 1px solid #CCCCCC;">
                        <strong>Rs.  {{ getTotal() }}</strong>
                    </td>
                </tr>

            </table>
        </div>

        <div class="page_right_div right_top3">
            <ul class="right_button">
                <li  ng-click="downitem()">
                    <div class="right_button_1"><img src="assets/frontend/images/down_arrow.png"></div>
                </li>
                <li  ng-click="upitem()"><div class="right_button_2"><img src="assets/frontend/images/up_arrow.png"></div></li>


                <li ng-if="packages.length==0" { ng-click="repeatorder()" }>
                    <div>
                        <span><img src="assets/frontend/images/repeat_order.png"></span>
                        <span>Repeat Order</span>
                    </div>
                </li>
                <li ng-if="packages.length>0" { } >
                    <div>
                        <span><img src="assets/frontend/images/repeat_order.png"></span>
                        <span>Repeat Order</span>
                    </div>
                </li>
                <li ng-click="deleteitem()">
                    <div>
                        <span><img src="assets/frontend/images/delete.png"></span>
                        <span>Delete</span>
                    </div>
                </li>
            </ul>
        </div>

        <div class="page_right_div right_top4">
            <div class="advance-boo">Advance Booking: {{Advanced_list.length}}</div>



        </div>

        <div class="page_right_div right_top5">

            <div class="scrol-hover-right-side">

                <ul class=" scrool-all-datat">



                    <li  ng-repeat="(key, value) in Advanced_list" style="box-shadow: -3px -3px 6px 2px {{value.package_color}} inset;margin-bottom:3px;background-color:{{value.package_color}};color:{{value.package_fontcolor}} " data-toggle="modal" data-target=".bs-example-modal-smm{{value.advancepayment_id}}" > {{value.package_name}} No- {{value.advancepayment_noofpersons}} Cost - {{value.package_price}} /- Time {{value.dailyinventory_from}}:{{value.dailyinventory_minfrom}}-{{value.dailyinventory_to}}:{{value.dailyinventory_minto}} - {{value.advancepayment_name}}
                  <span ng-if="value.advancepayment_orderid!='0'" >-Paid</span> </li><li>&nbsp;</li>


                </ul>
 <style>
     .t_field{
        margin-right:8px;
        min-width:130px;
        float:left;
     }
     .float_l{
        float:left;
        width:100%;
     }
 </style>

                <div  ng-repeat="(key, value) in Advanced_list" class="modal fade bs-example-modal-smm{{value.advancepayment_id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" >
                    <form method="post" name="myForm">
                        <div class="modal-dialog modal-sm " role="document" style="width:780px">
                            <div class="modal-content float_l">
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel">Advance Booking Detail - {{value.advancepayment_name}}</h4> </div>

                                <div class="modal-body float_l" >
                                    <div id="printableAreaadvance">

                                        <div class="col-lg-6">
                                             <div class="form-group" >
                                        <label><span class="t_field">Name:</span>{{value.advancepayment_name}}</label>

                                    </div>
                                    <div class="form-group">
                                        <label><span class="t_field">Mobile:</span>{{value.advancepayment_mobile}}</label>

                                    </div>
                                        <div class="form-group">
                                            <label><span class="t_field">Email:</span> {{value.advancepayment_email}}</label>

                                        </div>

                                    <div class="form-group">
                                        <label><span class="t_field">No. Of Persons:</span>{{value.advancepayment_noofpersons}}</label>

                                    </div>




                                    <div class="form-group">
                                        <label><span class="t_field">Session Date :</span>{{value.advancepayment_entrydate}}</label>

                                    </div>



                                    <div class="form-group">
                                        <label><span class="t_field">Session Time:</span>{{value.dailyinventory_from}}:{{value.dailyinventory_minfrom}}-{{value.dailyinventory_to}}:{{value.dailyinventory_minto}}</label>

                                    </div>


                                    <div class="form-group">
                                        <label><span class="t_field">Package Name:</span> {{value.package_name}}</label>
                                    </div>
                                            



                                    </div>
                                         <div class="col-lg-6">
                                             <div class="form-group">
                                        <label><span class="t_field">Package Price:</span> {{value.package_price}}</label></div>
                                        <div class="form-group">
                                        <label><span class="t_field">Amount:</span>{{value.advancepayment_amount}}</label>

                                    </div>
                                     <div class="form-group">
                                            <label><span class="t_field">Advance Payment:</span> {{value.advancepayment_advpayment}}</label>

                                        </div>
                                         <div class="form-group">
                                            <label><span class="t_field">Discount Amount:</span> {{value.advancepayment_discamount}}</label>

                                        </div>
                                        <div class="form-group">
                                            <label><span class="t_field">Balance Amount:</span> {{+value.advancepayment_balanceamount+ - +value.advancepayment_balanceamountpaid}}</label>

                                        </div>

                                             <div class="form-group"  ng-if="value.advancepayment_orderid!='0'">
                                                 <label><span class="t_field">Order Id:</span> {{value.advancepayment_orderid}}</label>

                                             </div>

                                         <!--<div class="form-group">
                                        <label><span class="t_field">Remaining Balance:</span>{{(value.package_price*value.advancepayment_noofpersons)-value.advancepayment_payment}} </label>

                                    </div>-->
                                         </div>
                                   
                                    


                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                                <label><span class="t_field">Package Addons:</span> {{value.addonsname}}</label>
                                            </div>
                                    </div>
                                       
                                       
                                        

                                   
                                </div>
                                 <div class="col-lg-12">
                                    <div class="row">
                                <div class="col-lg-6">
                                     <div class="form-group">
                                        <label>Remaining Balance Paid By</label>
                                       <input type="hidden" class="form-control" name="rembalancevalue"
                                               value="{{(value.package_price*value.advancepayment_noofpersons)-value.advancepayment_payment}}">
                                        </div>
                                </div>

                                        <div class="col-lg-6" ng-if="value.advancepayment_orderid!='0'">
                                            <div class="form-group">
                                                <label>Paid Status</label>

                                            </div>
                                        </div>
                                <div class="col-lg-6">
                                     <div class="form-group">
                                         <div ng-if="value.advancepayment_orderid!='0'">{{value.usersnamename}}</div>
                                         <div ng-if="value.advancepayment_orderid=='0'">
                                             <input type="text"  id="rembalancepaidby" data-ng-model="dataa.rembalancepaidby"   data-msg-required="Please enter the Remaining Balance Paid by." maxlength="100" class="form-control" name="rembalancepaidby" required  >
                                         </div>



                                    </div>
                                </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div ng-if="value.advancepayment_orderid!='0'">Paid</div>




                                            </div>
                                        </div>
                            </div>
                            </div>
                                   
                                </div>
                                <div class="modal-footer float_l" ng-if="value.advancepayment_orderid=='0'">

                                    <a class="btn btn-default pull-right" onclick="printDiv('printableAreaadvance')">Print</a>


                                    <button class="cash_close btn btn-default pull-right" style="margin-right: 15px;" name="submit" ng-click="addvanceFuncbooking(value.package_id,value.dailyinventory_id,value.dailyinventory_seats,value.advancepayment_id,value.advancepayment_name,value.advancepayment_mobile,value.advancepayment_noofpersons,value.advancepayment_email,value.advancepayment_entrydate,value.dailyinventory_from,value.dailyinventory_minfrom,value.dailyinventory_to,value.dailyinventory_minto,value.package_name,value.package_price,value.advancepayment_payment,dataa.rembalancepaidby,value.advancepayment_id)"  ng-disabled="myForm.$invalid">Convert To Order</button>
                                </div>

                                <div class="modal-footer float_l" ng-if="value.advancepayment_orderid!='0'">

                                    </div>


                            </div>
                        </div>
                        <div id="printableAreapackages">
                            <style>
                                #print_table{
                                margin:{{sprintingpage_data.printsetting_printtablemargin}}px;
                                padding:{{sprintingpage_data.printsetting_printtablepadding}}px;
                                border-collapse:{{sprintingpage_data.printsetting_printtablebordercollapse}};


                                }
                                #print_table td.par_t{

                                /*height: 100px;*/
                                vertical-align: {{sprintingpage_data.printsetting_printtabletdvertical_align}};
                                height: {{sprintingpage_data.printsetting_printtabletdheight}}px;

                                padding:{{sprintingpage_data.printsetting_printtabletdpadding}} {{sprintingpage_data.printsetting_printtabletdpadding1}}px;

                                }
                                /* #print_table td div{
                                    transform:rotate(90deg);
                                     height: 200px;
                                     width: 180px;


                                     margin-left:40px;
                                     font-size:13px;
                                 }*/
                                #print_table td table{
                                transform: rotate({{sprintingpage_data.printsetting_printtabletdrotate}}deg);
                                display: {{sprintingpage_data.printsetting_printtabletddisplay}};
                                width: {{sprintingpage_data.printsetting_print_tabletdtablewidth}}px;
                                font-size: {{sprintingpage_data.printsetting_print_tabletdtablefont_size}}px;
                                height: {{sprintingpage_data.printsetting_print_tabletdtableheight}}px;

                                margin-left: {{sprintingpage_data.printsetting_print_tabletdtablemargin_left}}px;
                                margin-top: -{{sprintingpage_data.printsetting_print_tabletdtablemargin_top}}px;
                                margin-bottom: -{{sprintingpage_data.printsetting_print_tabletdtablemargin_bottom}}px;
                                margin-right: {{sprintingpage_data.printsetting_print_tabletdtablemargin_right}}px;


                                }
                                #print_table td table td{
                                padding:{{sprintingpage_data.printsetting_padding}}px {{sprintingpage_data.printsetting_padding1}}px;
                                height:{{sprintingpage_data.printsetting_height}};
                                text-align: {{sprintingpage_data.printsetting_text_align}};
                                width:{{sprintingpage_data.printsetting_width}}%;
                                }
                                /*#print_table{
                                    height: 384px;
                                }*/

                            </style>


                            <table id="print_table" ng-repeat="(x,y) in printpackagelist" width="{{sprintingpage_data.printsetting_tablewidth}}" align="center"  >
                                <tr>
                                    <td class="par_t">

                                        <table>
                                            <tr>
                                                <td colspan="2">{{y.comp_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_GSTNO}}.</td>
                                                <td>{{y.comp_gstname}}</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;{{sprintingpage_data.printsetting_DATE}}</td>
                                                <td>{{y.orderproduct_date}}</td>
                                            </tr>

                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_SESSIONTIME}}</td>
                                                <td>{{y.orderproduct_inventory_from}}:{{y.orderproduct_inventory_minfrom}} {{y.txtfromd}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_SESSIONEXIT}}</td>
                                                <td>{{y.orderproduct_inventory_to}}:{{y.orderproduct_inventory_minto}} {{y.txttod}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_ENTRYFEES}}</td>
                                                <td>Rs {{y.orderproduct_package_price-((y.orderproduct_package_price*(+y.order_taxvalue +
                                                    +y.order_tax_percentage2 + +y.order_tax_percentage3))/100)}}
                                                </td>
                                            </tr>
                                            <tr ng-if="y.order_taxname!=''">
                                                <td >{{y.order_taxname}}</td>
                                                <td>Rs {{(y.orderproduct_package_price*(+y.order_taxvalue + +y.order_tax_percentage2 +
                                                    +y.order_tax_percentage3))/100}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_TotalAMOUNT}}</td>
                                                <td><span style="font-size: 10px;font-weight: bold;">Rs {{y.orderproduct_package_price}}</span></td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_TRANSNO}}.</td>
                                                <td>{{y.orderproduct_prid}}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td class="par_t">

                                        <table>
                                            <tr>
                                                <td colspan="2">{{y.comp_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_GSTNO}}.</td>
                                                <td>{{y.comp_gstname}}</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;{{sprintingpage_data.printsetting_DATE}}</td>
                                                <td>{{y.orderproduct_date}}</td>
                                            </tr>

                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_SESSIONTIME}}</td>
                                                <td>{{y.orderproduct_inventory_from}}:{{y.orderproduct_inventory_minfrom}} {{y.txtfromd}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_SESSIONEXIT}}</td>
                                                <td>{{y.orderproduct_inventory_to}}:{{y.orderproduct_inventory_minto}} {{y.txttod}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_ENTRYFEES}}</td>
                                                <td>Rs {{y.orderproduct_package_price-((y.orderproduct_package_price*(+y.order_taxvalue +
                                                    +y.order_tax_percentage2 + +y.order_tax_percentage3))/100)}}
                                                </td>
                                            </tr>
                                            <tr ng-if="y.order_taxname!=''">
                                                <td >{{y.order_taxname}}</td>
                                                <td>Rs {{(y.orderproduct_package_price*(+y.order_taxvalue + +y.order_tax_percentage2 +
                                                    +y.order_tax_percentage3))/100}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_TotalAMOUNT}}</td>
                                                <td><span style="font-size: 10px;font-weight: bold;">Rs {{y.orderproduct_package_price}}</span></td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_TRANSNO}}.</td>
                                                <td>{{y.orderproduct_prid}}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>


                                    </td>
                                </tr>
                                <tr>
                                    <td class="par_t">

                                        <table>
                                            <tr>
                                                <td colspan="2">{{y.comp_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_GSTNO}}.</td>
                                                <td>{{y.comp_gstname}}</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;{{sprintingpage_data.printsetting_DATE}}</td>
                                                <td>{{y.orderproduct_date}}</td>
                                            </tr>

                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_SESSIONTIME}}</td>
                                                <td>{{y.orderproduct_inventory_from}}:{{y.orderproduct_inventory_minfrom}} {{y.txtfromd}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_SESSIONEXIT}}</td>
                                                <td>{{y.orderproduct_inventory_to}}:{{y.orderproduct_inventory_minto}} {{y.txttod}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_ENTRYFEES}}</td>
                                                <td>Rs {{y.orderproduct_package_price-((y.orderproduct_package_price*(+y.order_taxvalue +
                                                    +y.order_tax_percentage2 + +y.order_tax_percentage3))/100)}}
                                                </td>
                                            </tr>
                                            <tr ng-if="y.order_taxname!=''">
                                                <td >{{y.order_taxname}}</td>
                                                <td>Rs {{(y.orderproduct_package_price*(+y.order_taxvalue + +y.order_tax_percentage2 +
                                                    +y.order_tax_percentage3))/100}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_TotalAMOUNT}}</td>
                                                <td><span style="font-size: 10px;font-weight: bold;">Rs {{y.orderproduct_package_price}}</span></td>
                                            </tr>
                                            <tr>
                                                <td>{{sprintingpage_data.printsetting_TRANSNO}}.</td>
                                                <td>{{y.orderproduct_prid}}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>


                                    </td>
                                </tr>


                            </table>


                        </div>
                    </form>
                </div>
            </div>

        </div>


        <div class="page_right_div right_top7">

            <ul class="image-boto">
                <li ng-if="packages.length>0"  ng-click="abortitem()" >
                    <div style="font-weight: 700;
    font-size: 17px;">Abort
                    </div>
                </li>
                <li ng-if="packages.length==0" { }>
                    <div style="font-weight: 700;
    font-size: 17px;opacity: 0.5">Abort
                    </div>
                </li>
                
               <!-- ng-click="additem()"-->
                <li ng-if="packages.length>0" { ng-click="additemprint()" }>
                    <div style="font-weight: 700;
    font-size: 17px;">Print
                    </div>
                </li>
                <li ng-if="packages.length==0" { }>
                    <div style="font-weight: 700;
    font-size: 17px;opacity: 0.5">Print
                    </div>
                </li>
            </ul>
        </div>


    </div>
</div>
<script type="text/javascript">

    function printDiv(divName) {

        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;

        window.location.reload();

    }

    var refresh = $window.localStorage.getItem('refresh');

    //console.log(refresh);
    if (refresh === null) {
        window.location.reload();
        $window.localStorage.setItem('refresh', "1");
    }
</script>
<div class="left_section">
    <div class="page_section">
        <div class="page_left_div page_top">
            <div class="page_tab">

                <a href="pos">
                    <div class="page_tab_d">Tickets <span class="pull-right"><i class="fa fa-calendar" aria-hidden="true"></i> {{dateval1_list}}</span></div>
                </a>
                <a href="pos#!addons">
                    <div class="page_tab_d top-left-he" style="    color: white;
    background: palevioletred;">Addons <span class="pull-right"><i class="fa fa-calendar" aria-hidden="true"></i> {{dateval1_list}}</span>
                    </div>

                </a>       </div>
            <div class="page_tab_content">


                <div class="second-scrol">

                    <div class="cont_left scol-top">

                        <div class="nodatatxt" ng-if="list.length===0">No Records Found</div>


                        <div class="tab_con_box">
                            <div class="tab_box" ng-repeat="(key, value) in list">
                                <div class="button_box "
                                     ng-click="AddPackage(noofpersons_list,value,value.package_id,value.package_name,value.package_price,value.package_createdon,value.package_description,value.package_complementory,complval)"
                                     style="background-color: {{ value.package_color }}; color: {{ value.package_fontcolor }}">
                                    <span class="type_text">{{value.package_name}}</span>
                                    <span class="price_text" ng-if="value.package_complementory=='0'">Rs. {{ value.package_price }}</span>
                                    <span class="price_text" ng-if="value.package_complementory=='1'">(Complement)Rs. {{ value.package_price=0 }}</span>
                                </div>
                            </div>


                        </div>


                        <!--new More Menu-->
                        <!--NEw More Menu End-->

                    </div>


                </div>

                <div ng-include="'assets/frontend/templates/rightmenu.php'"></div>


            </div>
        </div>


        <div ng-include="'assets/frontend/templates/footer.php'"></div>

    </div>

</div>
<div ng-include="'assets/frontend/templates/right.php'"></div>
<div ng-include="'assets/frontend/templates/bottom.php'"></div>


<script language="javascript">

    $(document).ready(function () {
        $(".click-fun").click(function () {
            $(".click-fun-none").css({ "display": "block" });
        });
        $(".close_more").click(function () {
            $(".click-fun-none").css({ "display": "none" });
        });

    });


</script>

<script>
    $( function() {
        // $.noConflict();
        $( "#datepicker12" ).datepicker({ minDate: 0, maxDate: "+1M " });
    });
    $("#datepicker12").click(function () {


    });
</script>

</body>
</html>	
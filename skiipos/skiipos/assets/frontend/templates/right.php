<div class="right_section">
    <div class="page_right_section">
        <div class="page_right_div right_top1">


            <table class="account">
                <tr>
                    <th scope="col">Description</th>
                    <th scope="col">Value</th>
                </tr>

            </table>

            <div class="scrol-hover">
                <div class="min-hei">
                    <table class="account ">

                        <tr ng-repeat="(key, value) in packages  track by $index" id="red-col"
                            ng-class="{sel: $index == selected}">
                            <td ng-click="removebyid($index,value,value.inventory_id,value.packageId,value.package_name,value.package_price,value.inventory_seats,value.inventory_from,value.inventory_to,value.inventory_minfrom,value.inventory_minto)">{{value.package_name}}</td>
                            <td >Rs.{{value.package_price}}</td>
                        </tr>
                        <tr ng-repeat="i in [1, 2, 3, 4, 5,6,7,8]"  ng-if="i >packages.length"><td>&nbsp;&nbsp;</td><td></td></tr>




                    </table>


                </div>



            </div>



        </div>

        <div class="page_right_div right_top2">
            <table class="account1">

                <tr>
                    <td class="border_none">
                        <strong>Total- {{packages.length}}</strong>

                    </td>
                    <td style="border-left: 1px solid #CCCCCC;">
                        <strong>Rs.  {{ getTotal() }}</strong>
                    </td>
                </tr>

            </table>
        </div>

        <div class="page_right_div right_top3">
            <ul class="right_button">
                <li  ng-click="downitem()">
                    <div class="right_button_1"><img src="assets/frontend/images/down_arrow.png"></div>
                </li>
                <li  ng-click="upitem()"><div class="right_button_2"><img src="assets/frontend/images/up_arrow.png"></div></li>


                <li ng-if="packages.length==0" { ng-click="repeatorder()" }>
                    <div>
                        <span><img src="assets/frontend/images/repeat_order.png"></span>
                        <span>Repeat Order</span>
                    </div>
                </li>
                <li ng-if="packages.length>0" { } >
                    <div>
                        <span><img src="assets/frontend/images/repeat_order.png"></span>
                        <span>Repeat Order</span>
                    </div>
                </li>
                <li ng-click="deleteitem()">
                    <div>
                        <span><img src="assets/frontend/images/delete.png"></span>
                        <span>Delete</span>
                    </div>
                </li>
            </ul>
        </div>

        <div class="page_right_div right_top4">
            <div class="advance-boo">Advance Booking:</div>



        </div>

        <div class="page_right_div right_top5">

            <div class="scrol-hover-right-side">

                <ul class=" scrool-all-datat">



                    <li  ng-repeat="(key, value) in Advanced_list" style="background-color:{{value.package_color}};color:{{value.package_fontcolor}}" data-toggle="modal" data-target=".bs-example-modal-smm{{value.advancepayment_id}}" > {{value.package_name}} No- {{value.advancepayment_noofpersons}} Cost - {{value.package_price}} /- Time {{value.dailyinventory_from}}:{{value.dailyinventory_minfrom}}-{{value.dailyinventory_to}}:{{value.dailyinventory_minto}} - {{value.advancepayment_name}}




                    </li>


                </ul>


                <div  ng-repeat="(key, value) in Advanced_list" class="modal fade bs-example-modal-smm{{value.advancepayment_id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" >
                    <form method="post" name="myForm">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel">Advance Booking Detail - {{value.advancepayment_name}}</h4> </div>

                                <div class="modal-body" >
                                    <div id="printableAreaadvance">
                                    <div class="form-group" >
                                        <label>Name:{{value.advancepayment_name}}</label>

                                    </div>
                                    <div class="form-group">
                                        <label>Mobile: {{value.advancepayment_mobile}}</label>

                                    </div>


                                    <div class="form-group">
                                        <label>No. Of Persons: {{value.advancepayment_noofpersons}}</label>

                                    </div>



                                    <div class="form-group">
                                        <label>Email: {{value.advancepayment_email}}</label>

                                    </div>
                                    <div class="form-group">
                                        <label>Date :{{value.advancepayment_entrydate}}</label>

                                    </div>



                                    <div class="form-group">
                                        <label>Time: {{value.dailyinventory_from}}:{{value.dailyinventory_minfrom}}-{{value.dailyinventory_to}}:{{value.dailyinventory_minto}}</label>

                                    </div>


                                    <div class="form-group">
                                        <label> Package Name {{value.package_name}}</label></div>
                                    <div class="form-group">
                                        <label> Package Price {{value.package_price}}</label></div>


                                    <div class="form-group">
                                        <label> Package advance payment: {{value.advancepayment_payment}}</label>

                                    </div>

                                    <div class="form-group">
                                        <label> Remaining Balance:{{(value.package_price*value.advancepayment_noofpersons)-value.advancepayment_payment}} </label>

                                    </div></div>
                                    <div class="form-group">
                                        <label>Remaining Balance Paid By</label>
                                        <input type="text"  id="rembalancepaidby" data-ng-model="dataa.rembalancepaidby"   data-msg-required="Please enter the Remaining Balance Paid by." maxlength="100" class="form-control" name="rembalancepaidby" required  >
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <a onclick="printDiv('printableAreaadvance')">Print</a>


                                    <button class="cash_close btn btn-default pull-right" style="margin-right: 15px;" name="submit" ng-click="addvanceFuncbooking(value.package_id,value.dailyinventory_id,value.dailyinventory_seats,value.advancepayment_id,value.advancepayment_name,value.advancepayment_mobile,value.advancepayment_noofpersons,value.advancepayment_email,value.advancepayment_entrydate,value.dailyinventory_from,value.dailyinventory_minfrom,value.dailyinventory_to,value.dailyinventory_minto,value.package_name,value.package_price,value.advancepayment_payment,dataa.rembalancepaidby)"  ng-disabled="myForm.$invalid">Convert To Order</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>


        <div class="page_right_div right_top7">

            <ul class="image-boto">
                <li ng-click="refreshitem()">
                    &nbsp;
                    <div style="font-weight: 700;
    font-size: 17px;">Multi Delete
                    </div>
                </li>
                <li ng-if="packages.length>0" { ng-click="additem()" }>
                    <div style="font-weight: 700;
    font-size: 17px;">Print
                    </div>
                </li>
                <li ng-if="packages.length==0" { }>
                    <div style="font-weight: 700;
    font-size: 17px;">Print
                    </div>
                </li>
            </ul>
        </div>


    </div>
</div>
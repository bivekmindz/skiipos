<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        window.location.reload();
    }
    var refresh = $window.localStorage.getItem('refresh');
    //console.log(refresh);
    if (refresh===null){
        window.location.reload();
        $window.localStorage.setItem('refresh', "1");
    }
</script>

<div class="left_div">
    <div class="page_left_1">
        <div class="div_box height_1">
            <div class="box_data">
                <table class="box_tbl">
                    <tr>
                        <td>Concessions</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Tickets</td>
                        <td>Rs.{{ getTotal() }}</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>Rs.{{ getTotal() }}</td>
                    </tr>
                </table>



                <table class="box_tbl">

                   <tr style="
background: #FEC900; color: red" ng-if="s_cashamountlist.order_statusdetail=='Cash'">
                        <td>Cash</td>
                       <td>Rs. {{+s_cashamountlist.cashtot + +0}}</td>


                   </tr>

                    <tr style="
background: #FEC900;color: red;"
                        ng-if="s_cashamountlist.order_statusdetail!='Cash' && s_cashamountlist.order_statusdetail!=''">
                        <td>{{s_cashamountlist.order_statusdetail}}</td>
                        <td>Rs. {{ getTotal() }}</td>
                    </tr>


                </table>


            </div>
            <table width="100%" cellpadding="1px" cellspacing="1px" border="1px" ng-if="cashamountdisplaylist.length>0">

                <tr>
                    <td>Cash Name</td>
                    <td>Cash QTY</td>
                    <td>Cash Total</td>
                    <td>Action</td>
                </tr>

                <tr ng-repeat="(m,n) in cashamountdisplaylist">
                    <td>{{n.cash_name}}</td>
                    <td>{{n.cash_counter}}</td>
                    <td>{{n.cash_total}}</td>
                    <td><a href="#" ng-click="deletecash(n.cash_id)">Delete</a></td>
                </tr>


            </table>
        </div>


        <div class="div_box split_box_height">
            <div class="sep_div">
                <ul class="split_ul">
                    <li>Amount to pay</li>
                    <li class="ng-binding">Rs. {{ getTotal()-( getTotal()*(s_cashamountlist.order_discount))/100}}</li>

                </ul>
                <ul class="split_ul">
                    <li ng-if="s_cashamountlist.order_statusdetail=='Cash'">
                        Change
                    </li>
                    <li ng-if="s_cashamountlist.order_statusdetail=='Cash'" style="
background: #FEC900;color: red" > Rs. {{ s_cashamountlist.cashtot -getTotal()}}
                    </li>

                </ul>
            </div>


            <div class="sep_div border-top outstanding_padding">
                <ul class="outstanding">
                    <li>Outstanding</li>
                    <li>Rs.{{ getTotal()-( getTotal()*(s_cashamountlist.order_discount))/100}}</li>
                    </li>



                </ul>
            </div>
        </div>
        <div class="option_btn">
            <div class="option_btn_l">
                <div class="div_box option_btn_height">
                    <ul class="option_ul">
                        <li>
                           <img src="assets/frontend/images/down_arrow.png" ng-click="downitem()"/>
                        </li>
                        <li>
                          <img src="assets/frontend/images/up_arrow.png" ng-click="upitem()"/>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="option_btn_r">
                <div class="div_box option_btn_height">
                    <ul class="option_btn_r_ul">
                        <li></li>
                        <li></li>
                        <li ng-if="s_cashamountlist.order_statusdetail=='Cash'" ng-click="deleteitem(orderid)">Delete
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page_left_2">
        <div class="div_box height_3">
            <ul class="account_btn">


                <li ng-click="addFunccash(dataa)"><span>Cash</span></li>

   <li class="fadeandscale_open type_text" data-toggle="modal" data-target=".bs-example-modal-sm3" ><span>Credit Card</span>
                    </li>
                <li class="fadeandscale_open type_text" data-toggle="modal" data-target=".bs-example-modal-sm2"   ><span>Debtors</span></li>


                <li ng-repeat="(x,y) in s_paymentcashamountlist" ng-click="addFuncpaymentdynamic(orderid,y.printpayment_name,y.printpayment_value,getTotal())"><span>{{y.printpayment_name}}</span></li>

            </ul>
        </div>


        <div class="modal fade bs-example-modal-sm3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <form method="post" name="myForm">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel">Credit Card Payment</h4> </div>

                        <div class="modal-body">





                            <div class="form-group">
                                <label>Credit Cart No</label>
                                <input type="text" id="desc" data-ng-model="dataa.desc"   data-msg-required="Please enter the Debit Cart No." maxlength="100" class="form-control" name="desc" required aria-required="true" name="desc" required >
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button class="cash_close btn btn-default pull-right" style="margin-right: 15px;" name="submit" ng-click="addFunccredit(dataa)" >Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="modal fade bs-example-modal-sm2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <form method="post" name="myForm">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel">Debit Card Payment</h4> </div>

                        <div class="modal-body">





                            <div class="form-group">
                                <label>Debit Cart No</label>
                                <input type="text" id="desc" data-ng-model="dataa.desc"   data-msg-required="Please enter the Debit Cart No." maxlength="100" class="form-control" name="desc" required aria-required="true" name="desc" required >
                            </div>

                        </div>
                        <div class="modal-footer">

                            <button class="cash_close btn btn-default pull-right" style="margin-right: 15px;" name="submit" ng-click="addFuncdebdit(dataa)" >Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="option_btn">
            <div class="div_box option_btn_height">
                <ul class="option_ul">
                    <li>
                       <img src="assets/frontend/images/down_arrow.png" ng-click="downitem()" />
                    </li>
                    <li>
                     <img src="assets/frontend/images/up_arrow.png" ng-click="upitem()"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="option_btn01">

        <div class="div_box option_btn_height2">
            <ul class="option_btn_first_bottom">
                <li></li>
                <li ng-repeat="(x,y) in s_orderdatarderlist" ng-if="y.order_statusdetail!=''">

                    <a onclick="printDiv('printableAreaaddons')">Print Addons</a></li>
                <li>
                <li ng-repeat="(x,y) in s_orderdatarderlist" ng-if="y.order_statusdetail!=''">

                    <a onclick="printDiv('printableAreapackages')">Print Packages</a></li>
                <li></li>
                <li ><a ng-click="cancelitem(dataa)">Cancel</a></li>
            </ul>
        </div>

    </div>
</div>
<div id="printableAreapackages" style="display:none;">

 <style>
        #print_table{
            margin:0px;
            padding:0px;
            border-collapse:collapse;


        }
         #print_table td.par_t{

             /*height: 100px;*/
             vertical-align: middle;
             height: 180px;

            padding:0 5px;

         }
       /* #print_table td div{
           transform:rotate(90deg);
            height: 200px;
            width: 180px;


            margin-left:40px;
            font-size:13px;
        }*/
        #print_table td table{
             transform:rotate(90deg);
            display: block;
            width: 180px;
            font-size:9px;
           height: 190px;

                margin-left: 63px;
            margin-bottom: -70px;

        }
         #print_table td table td{
             padding:0px 5px;
             height:inherit;
             text-align: right;
             width:50%;
         }
         /*#print_table{
         	height: 384px;
         }*/

    </style>


    <table id="print_table" ng-repeat="(x,y) in printpackagelist" width="284">
        <tr>
            <td class="par_t">

                <table>
                    <tr>
                        <td colspan="2">{{y.comp_name}}</td>
                    </tr>
                    <tr>
                        <td>GST NO.</td>
                        <td>{{y.comp_gstname}}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;DATE</td>
                        <td>{{y.orderproduct_date}}</td>
                    </tr>

                    <tr>
                        <td>SESSION&nbsp;TIME</td>
                        <td>{{y.orderproduct_inventory_from}}:{{y.orderproduct_inventory_minfrom}} Hrs</td>
                    </tr>
                    <tr>
                        <td>SESSION&nbsp;EXIT</td>
                        <td>{{y.orderproduct_inventory_to}}:{{y.orderproduct_inventory_minto}} Hrs</td>
                    </tr>
                    <tr>
                        <td>ENTRY FEES</td>
                        <td>Rs {{y.orderproduct_package_price-((y.orderproduct_package_price*(+y.order_taxvalue +
                            +y.order_tax_percentage2 + +y.order_tax_percentage3))/100)}}
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:7px;">{{y.order_taxname}}</td>
                        <td>Rs {{(y.orderproduct_package_price*(+y.order_taxvalue + +y.order_tax_percentage2 +
                            +y.order_tax_percentage3))/100}}
                        </td>
                    </tr>
                    <tr>
                        <td>TOT &nbsp;AMOUNT</td>
                        <td>Rs {{y.orderproduct_package_price}}</td>
                    </tr>
                    <tr>
                        <td>TRANS NO.</td>
                        <td>{{y.orderproduct_prid}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>

            </td>
        </tr>

    <tr>
        <td class="par_t">

            <table>
                <tr>
                    <td colspan="2">{{y.comp_name}}</td>
                </tr>
                <tr>
                    <td>GST NO.</td> <td >{{y.comp_gstname}}</td>
                </tr>
                <tr>
                    <td>&nbsp;DATE</td><td>{{y.orderproduct_date}}</td>
                </tr>

                <tr>
                    <td>SESSION&nbsp;TIME</td>
                    <td>{{y.orderproduct_inventory_from}}:{{y.orderproduct_inventory_minfrom}} Hrs</td>
                </tr>
                <tr>
                    <td>SESSION&nbsp;EXIT</td>
                    <td>{{y.orderproduct_inventory_to}}:{{y.orderproduct_inventory_minto}} Hrs</td>
                </tr>
                <tr>
                    <td>ENTRY FEES</td>
                    <td>Rs {{y.orderproduct_package_price-((y.orderproduct_package_price*(+y.order_taxvalue +
                        +y.order_tax_percentage2 + +y.order_tax_percentage3))/100)}}
                    </td>
                </tr>
                <tr>
                    <td style="font-size:7px;">{{y.order_taxname}}</td>
                    <td>Rs {{(y.orderproduct_package_price*(+y.order_taxvalue + +y.order_tax_percentage2 +
                        +y.order_tax_percentage3))/100}}
                    </td>
                </tr>
                <tr>
                    <td>TOT &nbsp;AMOUNT</td>
                    <td>Rs {{y.orderproduct_package_price}}</td>
                </tr>
                <tr>
                    <td>TRANS NO.</td>
                    <td>{{y.orderproduct_prid}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </td>
    </tr>
    <tr>
        <td class="par_t">

            <table>
                <tr>
                    <td colspan="2">{{y.comp_name}}</td>
                </tr>
                <tr>
                    <td>GST NO.</td> <td >{{y.comp_gstname}}</td>
                </tr>
                <tr>
                    <td>&nbsp;DATE</td><td>{{y.orderproduct_date}}</td>
                </tr>

                <tr>
                    <td>SESSION&nbsp;TIME</td>
                    <td>{{y.orderproduct_inventory_from}}:{{y.orderproduct_inventory_minfrom}} Hrs</td>
                </tr>
                <tr>
                    <td>SESSION&nbsp;EXIT</td>
                    <td>{{y.orderproduct_inventory_to}}:{{y.orderproduct_inventory_minto}} Hrs</td>
                </tr>
                <tr>
                    <td>ENTRY FEES</td>
                    <td>Rs {{y.orderproduct_package_price-((y.orderproduct_package_price*(+y.order_taxvalue +
                        +y.order_tax_percentage2 + +y.order_tax_percentage3))/100)}}
                    </td>
                </tr>
                <tr>
                    <td style="font-size:7px;">{{y.order_taxname}}</td>
                    <td>Rs {{(y.orderproduct_package_price*(+y.order_taxvalue + +y.order_tax_percentage2 +
                        +y.order_tax_percentage3))/100}}
                    </td>
                </tr>
                <tr>
                    <td>TOT &nbsp;AMOUNT</td>
                    <td>Rs {{y.orderproduct_package_price}}</td>
                </tr>
                <tr>
                    <td>TRANS NO.</td>
                    <td>{{y.orderproduct_prid}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </td>
    </tr>


    </table>

    </div>


<div id="printableAreaaddons" style="display: none">

    <table id="print_table" width="284">

        <tr>
            <td style="text-align:center;">Invoice Cum Callan</td>
        </tr>

        <tr>
            <td>
                <table style="font-size:11px;line-height: 20px;" width="100%" ng-repeat="(y,z) in compamasterlist ">
                    <tr>
                        <td style="text-align:center;">{{z.invoiceaddress}}</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">GST NO: {{z.comp_gstname}}</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">Date :{{dateTime}}</td>
                    </tr>


                </table>
            </td>
        </tr>


        <tr>
            <td>
                <table style="font-size:14px;line-height: 20px;margin-top:15px;" width="100%" cellspacing="1px" cellpadding="1px" border="0px">
                    <tr>
                        <td>Item</td>
                        <td>Qty</td>
                        <td>Unit Rate</td>
                        <td>Total</td>
                    </tr>
                    <tr ng-repeat="(x,y) in printaddonslist">
                        <td>{{y.orderproduct_package_name}}</td>
                        <td>{{y.orderquant}}</td>
                        <td>{{y.orderproduct_package_price}}</td>
                        <td>{{+y.orderquant * +y.orderproduct_package_price}}</td>
                    </tr>


                </table>
            </td>
        </tr>
      <tr><td>
      	<table style="font-size:14px;line-height: 20px;margin-top:15px;" width="100%">
        <tr>
            <td>Cash: {{getaddonprintTotal()}}.00</td>
        </tr>
        <tr>
            <td>Change :0.00</td>
        </tr>


        <tr>
            <td ng-if="getaddonprintcgst()!=0" >CGST {{getaddonprintcgstpercentage()}} % : {{getaddonprintcgst()}}.00</td>
        </tr>
        <tr>
            <td ng-if="getaddonprintsgst()!=0">SGST {{getaddonprintsgstpercentage()}} % :{{getaddonprintsgst()}}.00</td>
        </tr>
        <tr>
            <td ng-if="getaddonprintigst()!=0">IGST {{getaddonprintigstpercentage()}} %:{{getaddonprintigst()}}.00444</td>
        </tr>
        <tr>
            <td>Total Amount :{{getaddonprintTotal()}}.00</td>
        </tr>
        <tr>
            <td>The Above Amount Is Inclusive Of GST as applicable</td>
        </tr>
        <tr>
            <td>T/N :{{orderidlist}}</td>
        </tr>
        <tr>
            <td ng-repeat="(x,y) in userlog_list">OPR: {{y.user_firstname}}</td>
        </tr>
    </table>
     </td>
    </tr>
    </table>

</div>
<div class="left_div2">
    <div class="page_left_3">
        <div class="div_box height_3">


            <div class="note_btn" ng-repeat="(x,y) in cashview">

                <img src="assets/admin/images/{{y.cash_image}}" width="100%" height="80%" ng-init="counter = 0"
                     ng-if(y.order_statusdetail='Cash' ) {
                     ng-click="Addcashses(y,counter = counter + 1,orderid,y.order_statusdetail)" }/></li>

            </div>



        </div>
        <div class="option_btn">
            <div class="div_box complete_btn_height">
                <div class="complete_btn">
                    <div class="complete_btn_img">
                        <img src="assets/frontend/images/complete.jpg"  /><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_left_4">
        <div class="div_box height_4">

            <table class="tbl_design_th">
                <tr>
                    <th>Description</th>
                    <th>Value</th>
                </tr>
            </table>

            <div class="tbl_overflow">
                <table class="tbl_design_td">

                    <tr ng-repeat="(key, value) in list track by $index"  id="red-col" ng-class="{sel: $index == selected}">
                        <td ng-click="removebyid($index,value,value.inventory_id,value.packageId,value.package_name,value.package_price,value.inventory_seats,value.inventory_from,value.inventory_to,value.inventory_minfrom,value.inventory_minto)">
                            {{value.orderproduct_package_name}}<br/> <span
                                    ng-if="value.orderproduct_inventory_from!=''"> {{value.orderproduct_inventory_from}}:{{value.orderproduct_inventory_to}} Hrs</span>
                        </td>
                        <td  ng-click="removebyid($index,value,value.inventory_id,value.packageId,value.package_name,value.package_price,value.inventory_seats,value.inventory_from,value.inventory_to,value.inventory_minfrom,value.inventory_minto)">Rs.{{value.orderproduct_package_price}}</td>
                    </tr>






                </table>
            </div>
            <div class="total_tbl">
                <table class="tbl_design_td2">
                    <tr>
                        <td>Total - {{list.length}}</td>
                        <td>Rs. {{ getTotal() }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="option_btn">
            <div class="div_box option_btn_height">
                <ul class="option_ul2">
                    <li>
                        <img src="assets/frontend/images/down_arrow.png" ng-click="downitem()">
                    </li>
                    <li>
                      <img src="assets/frontend/images/up_arrow.png" ng-click="upitem()">
                    </li>
                    <li>
                      Repeat Order
                    </li>
                    <li ng-if="s_cashamountlist.order_statusdetail=='Cash'" ng-click="deleteitem(orderid)">Delete</li>

                </ul>
            </div>
        </div>
        <div class="option_btn">
            <div class="div_box complete_btn_height">
                <ul class="complete_ul">
                    <li>&nbsp;</li>
                    <li>
                        <div class="complete_btn">
                            <div class="complete_btn_img">
                                <img src="assets/frontend/images/complete.jpg" ng-click="completeorder()" />
                            <br>Complete</div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div ng-include="'assets/frontend/templates/bottom.php'"></div>


<script language="javascript">

    $(document).ready(function () {
        $(".click-fun").click(function () {
            $(".click-fun-none").css({ "display": "block" });
        });
        $(".close_more").click(function () {
            $(".click-fun-none").css({ "display": "none" });
        });

    });

    angular.module('myApp', ['ngAnimate', 'ui.bootstrap']);
    angular.module('myApp').controller('myCntrl', function ($scope) {
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.dateformat = "MM/dd/yyyy";
        $scope.today();
        $scope.showcalendar = function ($event) {
            $scope.showdp = true;
        };
        $scope.showdp = false;
    });
</script>

<div class="modal fade bs-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <form method="post" name="myForm">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel">Debit Card Payment</h4> </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" id="fname" data-ng-model="dataa.fName"  data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="name" required aria-required="true">
                        <input type="hidden" name="entityId" ng-model="dataa.entityId" ng-init="dataa.entityId = 'Credit Card'" />
                    </div>

                    <div class="form-group">
                        <label>Mobile No</label>
                        <input type="text" id="mobileno" data-ng-model="dataa.mobileno"  data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="mobileno" required aria-required="true">
                    </div>

                    <div class="form-group">
                        <label>Debit Cart No</label>
                        <input type="text" id="desc" data-ng-model="dataa.desc"   data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="desc" required aria-required="true" name="desc" >
                    </div>

                </div>
                <div class="modal-footer">

                    <button class="cash_close btn btn-default pull-right" style="margin-right: 15px;" name="submit" ng-click="addFunccash(dataa)" ng-disabled="myForm.$invalid">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>

</body>
</html>

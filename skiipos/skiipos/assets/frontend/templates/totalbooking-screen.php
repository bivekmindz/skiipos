<script type="text/javascript">

    function printDiv(divName) {

        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;

        window.location.reload();

    }

    var refresh = $window.localStorage.getItem('refresh');

    //console.log(refresh);
    if (refresh === null) {
        window.location.reload();
        $window.localStorage.setItem('refresh', "1");
    }
</script>
<style>
    .tab_box {
        float: left;
        width: 100% !important;
        padding: 4px 4px;
    }
</style>

<div class="left_section">

    <div class="page_section">
        <div class="page_left_div page_top">
            <div class="page_tab">

                <a href="pos">
                    <div class="page_tab_d">Tickets <span class="pull-right"><i class="fa fa-calendar"
                                                                                aria-hidden="true"></i> {{dateval1_list}}</span>
                    </div>
                </a>
                <a href="pos#!addons">
                    <div class="page_tab_d top-left-he">Addons <span class="pull-right"><i class="fa fa-calendar"
                                                                                           aria-hidden="true"></i> {{dateval1_list}}</span>
                    </div>

                </a>
            </div>
            <div class="page_tab_content">


                <div class="second-scrol">

                    <div class="cont_left scol-top">

                        <div class="nodatatxt" ng-if="list.length===0">No Records Found</div>

                        <div class="page_row">


                            <div class="">

                                <div class="tab_box">
                                    <table border="1">
                                        <tr style="background-color: blue; color:white">
                                            <td>Booking</td>
                                            <td>Offline</td>
                                            <td>Price</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr ng-repeat="(key, value) in total_list">
                                            <td> {{value.package_name}}</td>
                                            <td>{{value.prnum}}
                                            </td>

                                            <td> {{value.package_price}} /-</td>

                                            <td>

                                                {{value.dailyinventory_from}}:{{value.dailyinventory_minfrom}}-{{value.dailyinventory_to}}:{{value.dailyinventory_minto}}
                                            </td>
                                        </tr>


                                    </table>
                                </div>


                            </div>

                        </div>


                        <!--new More Menu-->
                        <!--NEw More Menu End-->

                    </div>


                </div>


                <div ng-include="'assets/frontend/templates/rightmenu.php'"></div>


            </div>
        </div>


        <div ng-include="'assets/frontend/templates/footer.php'"></div>

    </div>

</div>
<div ng-include="'assets/frontend/templates/right.php'"></div>
<div ng-include="'assets/frontend/templates/bottom.php'"></div>


</body>
</html>
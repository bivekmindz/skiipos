
app.controller("AddonsCntrl", function($scope, $http,$q){

    $scope.list = [];
    $scope.nowdate = '';
    $scope.packages = [];
    $scope.orderDetails = [];
    $scope.orderprintDetails = [];
    $scope.orderonlineuserDetails = [];
    $scope.orderbooking = [];
    $scope.compamastDetails = [];
    var storagePackage = localStorage.getItem('packages');
    if (localStorage.getItem('orderonlineuserDetails') != null) {

        $scope.orderonlineuserDetails = JSON.parse(localStorage.getItem('orderonlineuserDetails'));

    }
    //var storagePackage = localStorage.getItem('packages');

    if (localStorage.getItem('packages') != null) {

        $scope.packages = JSON.parse(storagePackage);
    }
    $http({
        type: 'GET',
        url: 'addonData',
    }).then(function (response) {
        if(response.status==200){

            $scope.list = response.data.s_viewaddons;
            $scope.cntrl_list = 'AddonsCntrl';
            $scope.complval = response.data.complementval1;
            $scope.compamasterlist = response.data.s_viewcompamast;

            console.log('fffff:', $scope.complval);
        }
    }, function (response) {

    });

    //$scope.packages = [];
    $scope.AddPackage = function (noofpersons_list, key, addon_id, addon_name, addon_price, addon_createdon, addon_description, package_complementory, complval) {

        if (complval != 1) {
            if (package_complementory == 1) {
                alert("Sorry, Complement Product Not Allowed");
                return false;
            }
        }

        for($i=1;$i<=noofpersons_list;$i++) {
            var dataaajax = angular.copy({
                packageId: addon_id,
                inventory_id: '',
                package_name: addon_name,
                package_price: addon_price,
                inventory_seats: '',
                inventory_from: '',
                inventory_to: '',
                inventory_minfrom: '',
                inventory_minto: '',
                inventory_packagetype: 'Addons',


            });
            $scope.packages.push(dataaajax);
        }
        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));

        console.log("packages", $scope.packages);
    };

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.packages.length; i++){
            var product = $scope.packages[i];
            total += (product.package_price * 1);
        }
        return total;
    };
    $scope.adduseraction = function (ticketid) {

        var deferred = $q.defer();

        $transaction = (ticketid);

        $.ajax({
            url: 'transactiononlinelockvaldata',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderprintDetails = (response));

                console.log("6Success", (response));
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };
    $scope.delid='';  $scope.delid1='';  $scope.delid2='';$scope.delid3='';$scope.delid4='';
    $scope.delid5='';$scope.delid6='';$scope.delid7='';$scope.delid8='';$scope.delid9='';$scope.delid10='';
    $scope.removebyid=function(index,key,inventory_id,package_id,package_name,package_price,inventory_seats,inventory_from,inventory_to,inventory_minfrom,inventory_minto)
    {

        //alert(index);
        //$scope.multipleLocation.splice(x, 1);
        // console.log('index',index);
        // console.log("packagesssssssss", key.length);

        $scope.delid=index;
        $scope.delid1=inventory_id;
        $scope.delid2=package_id;
        $scope.delid3=package_name;
        $scope.delid4=package_price;
        $scope.delid5=inventory_seats;
        $scope.delid6=inventory_from;
        $scope.delid7=inventory_to;
        $scope.delid8=inventory_minfrom;
        $scope.delid9=inventory_minto;
        $scope.selected = index;

        $scope.select= function(index) {
            $scope.selected = index;
        };
    };

    $scope.deleteitem=function()
    {
        // console.log('index',index);
        $scope.packages.splice($scope.delid, 1);
        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));

    };
    $scope.repeatitem=function()
    {
        for ( property in $scope.packages ) {

            $scope.packages.push($scope.packages[property]);
        }

       /* var dataaajax = angular.copy({
            packageId: $scope.delid2,
            inventory_id: $scope.delid1,
            package_name: $scope.delid3,
            package_price: $scope.delid4,
            inventory_seats: $scope.delid5,
            inventory_from: $scope.delid6,
            inventory_to: $scope.delid7,
            inventory_minfrom: $scope.delid8,
            inventory_minto: $scope.delid9,



        });
        $scope.packages.push(dataaajax);*/
        //$scope.packages.splice($scope.delid, 1);
        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));

    };
    $scope.downitem=function()
    {
        if( $scope.delid>=0) {
            $scope.delid = $scope.delid + 1;
        }
        $scope.selected = $scope.delid;


    };


    $scope.upitem=function()
    {
        if( $scope.delid>0) {
            $scope.delid = $scope.delid - 1;
        }
        $scope.selected = $scope.delid;


    };
    $scope.additem=function()
    {
        console.log("packageds","ddddddddd");
        $addons = JSON.stringify($scope.packages);


        $.ajax({
            url: 'addonesadd',
            type: 'POST',
            dataType: 'json',
            data: {'packages': $addons},

            success: function(data){
                location.replace('print');
                console.log("dahhhhhhhhhta", data);
            }
        });

       // location.url('print');
       location.replace('print');
      console.log("packageds", $addons);

    };

    $scope.logoutitem=function()
    {

        localStorage.clear();
        $packages = JSON.stringify($scope.packages);
        $.ajax({
            url: 'logout',
            type: 'POST',
            dataType: 'json',
            data: {'packages': $packages},

            success: function(data){

                console.log("data", data);
            }
        });
        location.replace('index');
        // console.log("packasssges", $scope.delid);
    };

    $scope.addFuncgroupbooking=function(dataa)
    {
        $groupbooking = JSON.stringify(dataa);
        $.ajax({
            url: 'groupbooking',
            type: 'POST',
            dataType: 'json',
            data: {'groupbooking': $groupbooking},

            success: function(data){

                console.log("data", dataa);
            }
        });
        location.reload();
    };

//.........Refresh..................
    $scope.refreshitem=function()
    {
        $scope.packages = [];
        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));
        $.ajax({
            url: 'unsetsessions',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function(data){

                console.log("data", dataa);
            }
        });
       // location.replace('pos');
        //console.log("refreshdata", $scope.delid);
    };

    $scope.addprinting=function(dataa) {

        var deferred = $q.defer();

        $printing = JSON.stringify(dataa);

        $.ajax({
            url: 'printinglockval',
            type: 'POST',
            dataType: 'json',
            data: {'printing': $printing},



            success: function(response){
                deferred.resolve($scope.orderDetails = (response));
                console.log("Success",response);
            },error: function (error) {
                console.log("Error::",error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };
    $scope.printValue = [];
    $scope.addprintingvalue=function(orderproduct_id) {

        $scope.printValue.push(orderproduct_id);



        $scope.cancelFn=function(dataa) {

            location.reload();
        }


    };
    $scope.addrefundvalue = function (orderDetails, typeofpayment) {
        $grouporderDetails = JSON.stringify(orderDetails);

        $.ajax({
            url: 'cancelorderbooking',
            type: 'POST',
            dataType: 'json',
            data: {'grouporderDetails': $grouporderDetails, 'typeofpayment': typeofpayment},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };

    $scope.addrefundvalueaddons = function (orderDetails, typeofpayment) {
        $grouporderDetails = JSON.stringify(orderDetails);

        $.ajax({
            url: 'cancelorderbookingaddons',
            type: 'POST',
            dataType: 'json',
            data: {'grouporderDetails': $grouporderDetails, 'typeofpayment': typeofpayment},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };


    $scope.addrefundvaluetransactions = function (orderDetails, typeofpayment) {
        $grouporderDetails = JSON.stringify(orderDetails);

        $.ajax({
            url: 'cancelorderbookingtransactions',
            type: 'POST',
            dataType: 'json',
            data: {'grouporderDetails': $grouporderDetails, 'typeofpayment': typeofpayment},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };


    $scope.repeate_more = function () {

        $(".click-fun-none").css({"display": "none"});

    };
    $scope.click_funprint1 = function () {

        $(".click-fun1-noneprint").css({"display": "block"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funprintaddons1 = function () {
        $(".click-fun1-noneaddons").css({"display": "block"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };


    $scope.click_funbookingprocess = function () {

        $(".click-fun1-nonebookingprocess").css({"display": "block"});
        $(".click-fun1-noneprint").css({"display": "none"});

        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefund1 = function () {

        $(".click-fun1-nonerefund").css({"display": "block"});

        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundaddons1 = function () {
        $(".click-fun1-nonerefundaddons").css({"display": "block"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundtransactions1 = function () {
        $(".click-fun1-nonetransactions").css({"display": "block"});
        $(".click-fun1-nonerefundaddons").css({"display": "none"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundworkstation1 = function () {
        $(".click-fun1-noneworkstation").css({"display": "block"});

        $(".click-fun1-nonetransactions").css({"display": "none"});
        $(".click-fun1-nonerefundaddons").css({"display": "none"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundstep1 = function () {
        $(".click-fun1-nonerefundstep1").css({"display": "block"});
        $(".click-fun1-nonerefund").css({"display": "none"});

        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };


    $scope.click_funsetting1 = function () {
        $(".click-fun1-nonesetting").css({"display": "block"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };


    $scope.click_funprintstep1 = function () {
        $(".click-fun1-noneprintstep1").css({"display": "block"});

        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };
    $scope.click_funprinttransaction = function () {
        $(".click-fun1-noneprinttransaction").css({"display": "block"});


    };


    $scope.click_funreprintuser = function () {
        var deferred = $q.defer();

        // $transaction = JSON.stringify(dataa);

        $.ajax({
            url: 'onlineuserval',
            type: 'POST',
            dataType: 'json',
            data: {},


            success: function (response) {

                localStorage.setItem('orderonlineuserDetails', JSON.stringify(response));
                deferred.resolve($scope.orderonlineuserDetails = (response));
                console.log("userSuccess", $scope.orderonlineuserDetails);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

        $(".click-fun1-noneprintuser").css({"display": "block"});


    };


    $scope.addtransaction = function (dataa) {

        var deferred = $q.defer();

        $transaction = JSON.stringify(dataa);

        $.ajax({
            url: 'transactiononlinelockval',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderprintDetails = (response));
                console.log("5Success", response);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };


    $scope.addconfirmpassword = function (dataa, id) {
        var newpassword = document.getElementById("newpassword");
        var cpassword = document.getElementById("cpassword");

        if (newpassword.value != cpassword.value) {
            $(".err00").css('display', 'block').fadeOut(3000);

            return false;
        }
        if (newpassword.value == '') {
            $(".err00").css('display', 'block').fadeOut(3000);

            return false;
        }
        else {
            $transaction = JSON.stringify(dataa);

            $.ajax({
                url: 'updatepassword',
                type: 'POST',
                dataType: 'json',
                data: {'transaction': $transaction, 'uid': id},

                success: function (data) {

                    console.log("data", dataa);
                }
            });

            localStorage.clear('packages');

            $.ajax({
                url: 'logout',
                type: 'POST',
                dataType: 'json',
                data: {},

                success: function (data) {

                    console.log("data", data);
                }
            });
            location.replace('index');


            // cpassword.setCustomValidity('');
        }


    };


    $scope.adduseraction = function (ticketid) {

        var deferred = $q.defer();

        $transaction = (ticketid);

        $.ajax({
            url: 'transactiononlinelockvaldata',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderprintDetails = (response));

                console.log("6Success", (response));
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };
    $scope.getTotalprintonline = function () {
        var totalprintonline = 0;
        for (var i = 0; i < $scope.orderprintDetails.length; i++) {
            var product1 = $scope.orderprintDetails[i];
            totalprintonline += (product1.price * 1);
        }
        return totalprintonline;
    };

    $scope.addrefundtransactions = function (dataa) {

        var deferred = $q.defer();

        $transaction = JSON.stringify(dataa);

        $.ajax({
            url: 'transactiononlinerefundlockval',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderDetails = (response));
                console.log("1Success", (response));
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };

    $scope.addbooking = function (dataa) {
        $transactionbooking = JSON.stringify(dataa);

        var deferred = $q.defer();
        $.ajax({
            url: 'addbookinggtransactions',
            type: 'POST',
            dataType: 'json',
            data: {'transactionbooking': $transactionbooking},

            success: function (response) {

                deferred.resolve($scope.orderbooking = ((response)));

                console.log("33sSuccess", $scope.orderbooking);

                for (x = 0; x < $scope.orderbooking.length; x++) {
                    for (n = 1; n < $scope.orderbooking[x].qty; n++) {
                        if ($scope.orderbooking[x].txttod = 'PM') {
                            var a = Number($scope.orderbooking[x].tohrs || 0);
                            var b = Number(12 || 0);
                            $scope.orderbooking[x].tohrs1 = a + b;
                            // $scope.orderbooking[x].tohrs=$scope.orderbooking[x].tohrs+12;
                        }
                        else {
                            $scope.orderbooking[x].tohrs1 = $scope.orderbooking[x].tohrs;
                        }

                        var dataaajax = angular.copy({
                            packageId: '',
                            inventory_id: $scope.orderbooking[x].txttod,
                            package_name: $scope.orderbooking[x].name,
                            package_price: $scope.orderbooking[x].price,
                            inventory_seats: $scope.orderbooking[x].qty,
                            inventory_from: $scope.orderbooking[x].fromses,
                            inventory_to: $scope.orderbooking[x].tohrs1,
                            inventory_minfrom: $scope.orderbooking[x].frommin,
                            inventory_minto: $scope.orderbooking[x].tomin,
                            inventory_packagetype: 'Online',
                        });
                        $scope.packages.push(dataaajax);


                    }
                    localStorage.setItem('packages', JSON.stringify($scope.packages));
                    localStorage.setItem('lastorder', JSON.stringify($scope.packages));
                    //  location.reload();
                    console.log("sSuccess", $scope.orderbooking[x].qty);

                }


            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        //location.reload();


    };
    $scope.qty = [];
    $scope.getItemCollection = function (qty) {
        var collection = [];

        for (i = 0; i < qty; i++) {
            collection.push(qty);
        }
        return collection;
    };

    $scope.click_nocomp = function () {
        $.ajax({
            url: 'nocomplement',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function (data) {

                console.log("data", data);
            }
        });
        location.reload();
    };


    $scope.click_nocompval = function () {
        $.ajax({
            url: 'complementval',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function (data) {

                console.log("data", data);
            }
        });
        location.reload();
    };


    $scope.getaddonprintTotal = function () {

        var addonprinttotal = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprinttotal += (product.orderproduct_package_price * product.orderquant);
        }
        return addonprinttotal;
    };

    $scope.getaddonprintcgst = function () {

        var addonprintcgst = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprintcgst += (product.order_taxvalue * product.orderquant);
        }
        return addonprintcgst;
    };
    $scope.getaddonprintsgst = function () {

        var addonprintsgst = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprintsgst += (product.order_tax_percentage2 * product.orderquant);
        }
        return addonprintsgst;
    };


    $scope.getaddonprintigst = function () {

        var addonprintigst = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprintigst += (product.order_tax_percentage3 * product.orderquant);
        }
        return addonprintigst;
    };


    $scope.cancelprintingdata = function () {

        $(".click-fun1-nonetransactions").css({"display": "none"});
        $(".click-fun1-nonesetting").css({"display": "none"});
        $(".click-fun1-nonerefundaddons").css({"display": "none"});
        $(".click-fun1-noneprintuser").css({"display": "none"});
        $(".click-fun1-noneprinttransaction").css({"display": "none"});
        $(".click-fun1-nonebookingprocess").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});
        $(".click-fun1-noneprint2").css({"display": "none"});
        $(".click-fun1-noneprintstep1").css({"display": "none"});
        $(".click-fun1-noneaddons").css({"display": "none"});
        $(".click-fun1-nonesetting").css({"display": "none"});
    }

});

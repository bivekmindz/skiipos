app.controller("TotalCntrl", function ($scope, $http, $q) {

    $scope.list = [];
    $scope.txtDepartDate = '';
    $scope.nowdate = '';
    $scope.packages = [];
    $scope.orderDetails = [];
    $scope.orderprintDetails = [];
    $scope.orderonlineuserDetails = [];
    $scope.orderbooking = [];
    $scope.compamastDetails = [];


    // Check local storage

    var storagePackage = localStorage.getItem('packages');
    if (localStorage.getItem('orderonlineuserDetails') != null) {

        $scope.orderonlineuserDetails = JSON.parse(localStorage.getItem('orderonlineuserDetails'));

    }


    if (localStorage.getItem('packages') != null) {

        $scope.packages = JSON.parse(storagePackage);

    }
    console.log('storagePackage', $scope.packages);
    //Date Picker
    $scope.getFirstName = function () {

        $("#datepicker12").datepicker({dateFormat: "yy-mm-dd"});
        $("#datepicker12").on("change", function () {
            $selected = $(this).val();
            //alert($selected);


            $.ajax({
                url: 'dateval',
                type: 'POST',
                dataType: 'json',
                data: {'datevalue': $selected},

                success: function (data) {
                    location.reload();

                    console.log("date", data);
                }
            });
            location.replace('pos');


            // console.log("Date",selected);
        });


    };
    //.........Today Date Picker..................

    $scope.nowdate = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = mm + '/' + dd + '/' + yyyy;


        $selected = today;
        $.ajax({
            url: 'dateval',
            type: 'POST',
            dataType: 'json',
            data: {'datevalue': $selected},

            success: function (data) {
                // location.reload();

                console.log("date", data);
            }
        });
        location.reload();
    };
    //.........tommorow Date Picker..................
    $scope.tommorowdate = function (dateval1_list) {
        days = 1;
        newDate = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
        days;


        today = new Date();
        dd = newDate.getDate();
        mm = newDate.getMonth() + 1; //January is 0!

        yyyy = newDate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        today = mm + '/' + dd + '/' + yyyy;


        $selected = today;

        $.ajax({
            url: 'dateval',
            type: 'POST',
            dataType: 'json',
            data: {'datevalue': $selected},

            success: function (data) {
                // location.reload();

                console.log("date", data);
            }
        });
        location.reload();
    };


    //.........Complete ..................
    //.........Complete Dataa Display..................
    $http({
        type: 'GET',
        url: 'posData',
        dataType: 'json',
    }).then(function (response) {


        if (response.status == 200) {
            $scope.list = response.data.s_viewpackages;
            $scope.Advanced_list = response.data.advanceboking;
            $scope.compamasterlist = response.data.s_viewcompamast;
            $scope.total_list = response.data.totalboking;
            $scope.dateval1_list = response.data.dateval1;
            $scope.packqty_list = response.data.s_viewpackagesqty;
            $scope.userlog_list = response.data.select_userlog;
            $scope.orderlog_list = response.data.select_orderlog;
            $scope.viewtimeslotadv_list = response.data.s_viewtimeslotadv;
            $scope.vpackageadv_list = response.data.s_parameterpackageadv;
            $scope.nameadvance_list = response.data.nameadvance;
            $scope.noofpersons_list = response.data.noofpersons;
            $scope.cntrl_list = 'TotalCntrl';
            $scope.footerboking_list = response.data.footerboking;

            $scope.complval = response.data.complementval1;
            console.log('packqty_list LISSTTTTT:', $scope.orderlog_list);
        }
    }, function (response) {

    });
    $scope.packagesnum = 10;


    $scope.AddPackage = function (noofpersons_list, key, inventory_id, package_id, package_name, package_price, inventory_seats, inventory_from, inventory_to, inventory_minfrom, inventory_minto, dailyinventory_date, prnum) {

        txtDepartDate = dailyinventory_date;
        date = new Date();
        today = new Date();

        dd = today.getDate();
        mm = today.getMonth() + 1; //January is 0!
        yyyy = today.getFullYear();
        hour1 = date.getHours();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '/' + mm + '/' + yyyy;
        today = today.trim();

        start = inventory_from;

        hour1 = date.getHours();
        min1 = date.getMinutes();

        end = hour1 + ":" + min1;
        e = start.split(':');
        s = end.split(':');
        min = e[1] - s[1];
        hour_carry = 0;


        if (min < 0) {
            min += 60;
            hour_carry += 1;
        }
        hour = e[0] - s[0] - hour_carry;
        min = ((min / 60) * 100).toString();
        diff = hour;

        if (today >= txtDepartDate) {
            if (diff <= 0) {
                alert("Sorry, Online booking is not allowed  to the session time");
                return false;
            }
        }

        for ($i = 1; $i <= noofpersons_list; $i++) {
            var dataaajax = angular.copy({
                packageId: package_id,
                inventory_id: inventory_id,
                package_name: package_name,
                package_price: package_price,
                inventory_seats: inventory_seats,
                inventory_from: inventory_from,
                inventory_to: inventory_to,
                inventory_minfrom: inventory_minfrom,
                inventory_minto: inventory_minto,
                inventory_packagetype: 'Package',
            });
            $scope.packages.push(dataaajax);


        }

        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));

        console.log("packages", JSON.stringify($scope.packages));
    };

    $scope.getTotal = function () {
        var total = 0;
        for (var i = 0; i < $scope.packages.length; i++) {
            var product = $scope.packages[i];
            total += (product.package_price * 1);
        }
        return total;
    };

    $scope.getTotalprint = function () {
        var totalprint = 0;
        for (var i = 0; i < $scope.orderDetails.length; i++) {
            var product1 = $scope.orderDetails[i];
            totalprint += (product1.orderproduct_package_price * 1);
        }
        return totalprint;
    };


    $scope.delid = '';
    $scope.delid1 = '';
    $scope.delid2 = '';
    $scope.delid3 = '';
    $scope.delid4 = '';
    $scope.delid5 = '';
    $scope.delid6 = '';
    $scope.delid7 = '';
    $scope.delid8 = '';
    $scope.delid9 = '';
    $scope.delid10 = '';
    $scope.removebyid = function (index, key, inventory_id, package_id, package_name, package_price, inventory_seats, inventory_from, inventory_to, inventory_minfrom, inventory_minto) {

        //alert(index);
        //$scope.multipleLocation.splice(x, 1);
        // console.log('index',index);
        // console.log("packagesssssssss", key.length);

        $scope.delid = index;
        $scope.delid1 = inventory_id;
        $scope.delid2 = package_id;
        $scope.delid3 = package_name;
        $scope.delid4 = package_price;
        $scope.delid5 = inventory_seats;
        $scope.delid6 = inventory_from;
        $scope.delid7 = inventory_to;
        $scope.delid8 = inventory_minfrom;
        $scope.delid9 = inventory_minto;
        $scope.selected = index;

        $scope.select = function (index) {
            $scope.selected = index;
        };
    };
//.........delete Dataa ..................
    $scope.deleteitem = function () {

        // console.log('index',index);
        $scope.packages.splice($scope.delid, 1);
        //localStorage.splice('packages', $scope.delid, 1);
        localStorage.setItem('packages', JSON.stringify($scope.packages));

    };
    //.........Repeate Dataa ..................

    $scope.repeatitem = function () {

        for (property in $scope.packages) {

            $scope.packages.push($scope.packages[property]);

        }

        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));

        //$scope.packages.splice($scope.delid, 1);
        console.log('trdttt:', $scope.packages);
    };

    //.........Down Click ..................

    $scope.downitem = function () {
        if ($scope.delid >= 0) {
            $scope.delid = $scope.delid + 1;
        }
        $scope.selected = $scope.delid;


    };

//.........Up Click..................

    $scope.upitem = function () {

        if ($scope.delid > 0) {
            $scope.delid = $scope.delid - 1;
        }
        $scope.selected = $scope.delid;


    };

    //.........Packages Add In Database..................
    $scope.additem = function () {

        $packages = JSON.stringify($scope.packages);

        $.ajax({
            url: 'packagesadd',
            type: 'POST',
            dataType: 'json',
            data: {'packages': $packages},

            success: function (data) {

                console.log("data", data);
            }
        });
        location.replace('print');
        //console.log("packages", $packages);

    };
//.........Logout..................
    $scope.logoutitem = function () {

        localStorage.clear('packages');
        $packages = JSON.stringify($scope.packages);
        $.ajax({
            url: 'logout',
            type: 'POST',
            dataType: 'json',
            data: {'packages': $packages},

            success: function (data) {

                console.log("data", data);
            }
        });
        location.replace('index');
        // console.log("packasssges", $scope.delid);
    };

//.........Refresh..................
    $scope.refreshitem = function () {
        $scope.packages = [];
        localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', JSON.stringify($scope.packages));

        //localStorage.clear();
        $.ajax({
            url: 'unsetsessions',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        // location.replace('pos');
        //console.log("refreshdata", $scope.delid);
    };


    //.........Advance Booking..................
    $scope.addFuncadvancebooking = function (dataa) {
        $advancebooking = JSON.stringify(dataa);
        $.ajax({
            url: 'advancebookingadd',
            type: 'POST',
            dataType: 'json',
            data: {'advancebooking': $advancebooking},

            success: function (data) {

                console.log("data", dataa);
            }
        });
//alert(dataa);
        location.reload();
    };
    $scope.addvanceFuncbooking = function (package_id, dailyinventory_id, dailyinventory_seats, advancepayment_id, advancepayment_name, advancepayment_mobile, advancepayment_noofpersons, advancepayment_email, advancepayment_entrydate, dailyinventory_from, dailyinventory_minfrom, dailyinventory_to, dailyinventory_minto, package_name, package_price, advancepayment_payment, rembalancepaidby) {
        $advancepayment_name = advancepayment_name;

        $.ajax({
            url: 'advancebookingaddorder',
            type: 'POST',
            dataType: 'json',
            data: {
                'package_id': package_id,
                'dailyinventory_id': dailyinventory_id,
                'dailyinventory_seats': dailyinventory_seats,
                'advancepayment_id': advancepayment_id,
                'advancepayment_name': advancepayment_name,
                'advancepayment_mobile': advancepayment_mobile,
                'advancepayment_noofpersons': advancepayment_noofpersons,
                'advancepayment_email': advancepayment_email,
                'advancepayment_entrydate': advancepayment_entrydate,
                'dailyinventory_from': dailyinventory_from,
                'dailyinventory_minfrom': dailyinventory_minfrom,
                'dailyinventory_to': dailyinventory_to,
                'dailyinventory_minto': dailyinventory_minto,
                'package_name': package_name,
                'package_price': package_price,
                'advancepayment_payment': advancepayment_payment,
                'rembalancepaidby': rembalancepaidby
            },

            success: function (data) {

                console.log("data", dataa);
            }
        });

        location.replace('print');


    };

    $scope.addFuncgroupbooking = function (dataa) {
        $groupbooking = JSON.stringify(dataa);
        $.ajax({
            url: 'groupbooking',
            type: 'POST',
            dataType: 'json',
            data: {'groupbooking': $groupbooking},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };

    $scope.lockitem = function () {

        $.ajax({
            url: 'userlock',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };
    $scope.repeatorder = function () {

        for (property in (JSON.parse(localStorage.getItem('lastorderval')))) {


            $scope.packages.push((JSON.parse(localStorage.getItem('lastorderval')))[property]);

        }

        //   localStorage.setItem('packages', JSON.stringify($scope.packages));
        //   localStorage.setItem('lastorder', JSON.stringify($scope.packages));


//alert(((localStorage.getItem('lastorder')));
        //  $scope.packages.push(JSON.parse(localStorage.getItem('lastorder')));
        //   localStorage.setItem('packages', JSON.stringify($scope.packages));
        localStorage.setItem('lastorder', localStorage.getItem('lastorderval'));
        localStorage.setItem('packages', localStorage.getItem('lastorderval'));

        //$scope.packages.splice($scope.delid, 1);
        console.log('trdttt:', $scope.packages);

    };
    $scope.addprintingaddons = function (dataa) {

        var deferred = $q.defer();

        $printing = JSON.stringify(dataa);

        $.ajax({
            url: 'printingaddonslockval',
            type: 'POST',
            dataType: 'json',
            data: {'printing': $printing},


            success: function (response) {

                deferred.resolve($scope.orderDetails = (response.s_vieworder));
                deferred.resolve($scope.compamastDetails = (response.s_viewcompamast));
                deferred.resolve($scope.compamastDetails = (response.s_viewcompamast));

                deferred.resolve($scope.printaddonslist = (response.s_vieworderaddons));
                deferred.resolve($scope.orderidlist = (response.s_vieworderid));

                console.log("2Success", response.s_viewcompamast);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };

    $scope.addprinting = function (dataa) {

        var deferred = $q.defer();

        $printing = JSON.stringify(dataa);

        $.ajax({
            url: 'printinglockval',
            type: 'POST',
            dataType: 'json',
            data: {'printing': $printing},


            success: function (response) {
                deferred.resolve($scope.orderDetails = (response));
                console.log("2Success", response);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };
    $scope.addrefundticket = function (dataa) {

        var deferred = $q.defer();

        $printing = JSON.stringify(dataa);

        $.ajax({
            url: 'refundlockvalticket',
            type: 'POST',
            dataType: 'json',
            data: {'printing': $printing},


            success: function (response) {
                deferred.resolve($scope.orderDetails = (response));
                console.log("3Success", response);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };
    $scope.addrefundaddons = function (dataa) {

        var deferred = $q.defer();

        $printing = JSON.stringify(dataa);

        $.ajax({
            url: 'refundlockvaladdons',
            type: 'POST',
            dataType: 'json',
            data: {'printing': $printing},


            success: function (response) {
                deferred.resolve($scope.orderDetails = (response));
                console.log("4Success", response);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };


    $scope.printValue = [];
    $scope.addprintingvalue = function (orderproduct_id) {

        $scope.printValue.push(orderproduct_id);


        $scope.cancelFn = function (dataa) {

            location.reload();
        }


    };
    $scope.addrefundvalue = function (orderDetails, typeofpayment) {
        $grouporderDetails = JSON.stringify(orderDetails);

        $.ajax({
            url: 'cancelorderbooking',
            type: 'POST',
            dataType: 'json',
            data: {'grouporderDetails': $grouporderDetails, 'typeofpayment': typeofpayment},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };

    $scope.addrefundvalueaddons = function (orderDetails, typeofpayment) {
        $grouporderDetails = JSON.stringify(orderDetails);

        $.ajax({
            url: 'cancelorderbookingaddons',
            type: 'POST',
            dataType: 'json',
            data: {'grouporderDetails': $grouporderDetails, 'typeofpayment': typeofpayment},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };


    $scope.addrefundvaluetransactions = function (orderDetails, typeofpayment) {
        $grouporderDetails = JSON.stringify(orderDetails);

        $.ajax({
            url: 'cancelorderbookingtransactions',
            type: 'POST',
            dataType: 'json',
            data: {'grouporderDetails': $grouporderDetails, 'typeofpayment': typeofpayment},

            success: function (data) {

                console.log("data", dataa);
            }
        });
        location.reload();
    };


    $scope.repeate_more = function () {

        $(".click-fun-none").css({"display": "none"});

    };
    $scope.click_funprint1 = function () {

        $(".click-fun1-noneprint").css({"display": "block"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funprintaddons1 = function () {
        $(".click-fun1-noneaddons").css({"display": "block"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };


    $scope.click_funbookingprocess = function () {

        $(".click-fun1-nonebookingprocess").css({"display": "block"});
        $(".click-fun1-noneprint").css({"display": "none"});

        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefund1 = function () {

        $(".click-fun1-nonerefund").css({"display": "block"});

        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundaddons1 = function () {
        $(".click-fun1-nonerefundaddons").css({"display": "block"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundtransactions1 = function () {
        $(".click-fun1-nonetransactions").css({"display": "block"});
        $(".click-fun1-nonerefundaddons").css({"display": "none"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundworkstation1 = function () {
        $(".click-fun1-noneworkstation").css({"display": "block"});

        $(".click-fun1-nonetransactions").css({"display": "none"});
        $(".click-fun1-nonerefundaddons").css({"display": "none"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };

    $scope.click_funrefundstep1 = function () {
        $(".click-fun1-nonerefundstep1").css({"display": "block"});
        $(".click-fun1-nonerefund").css({"display": "none"});

        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };


    $scope.click_funsetting1 = function () {
        $(".click-fun1-nonesetting").css({"display": "block"});
        $(".click-fun1-nonerefund").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };


    $scope.click_funprintstep1 = function () {
        $(".click-fun1-noneprintstep1").css({"display": "block"});

        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});


    };
    $scope.click_funprinttransaction = function () {
        $(".click-fun1-noneprinttransaction").css({"display": "block"});


    };

    $scope.click_funreprintuser = function () {
        var deferred = $q.defer();

        // $transaction = JSON.stringify(dataa);

        $.ajax({
            url: 'onlineuserval',
            type: 'POST',
            dataType: 'json',
            data: {},


            success: function (response) {

                localStorage.setItem('orderonlineuserDetails', JSON.stringify(response));
                deferred.resolve($scope.click_funreprintuser = (response));
                console.log("userSuccess", $scope.orderonlineuserDetails);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

        $(".click-fun1-noneprintuser").css({"display": "block"});


    };


    $scope.addtransaction = function (dataa) {

        var deferred = $q.defer();

        $transaction = JSON.stringify(dataa);

        $.ajax({
            url: 'transactiononlinelockval',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderprintDetails = (response));
                console.log("5Success", response);
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };


    $scope.addconfirmpassword = function (dataa, id) {
        var newpassword = document.getElementById("newpassword");
        var cpassword = document.getElementById("cpassword");

        if (newpassword.value != cpassword.value) {
            $(".err00").css('display', 'block').fadeOut(3000);

            return false;
        }
        if (newpassword.value == '') {
            $(".err00").css('display', 'block').fadeOut(3000);

            return false;
        }
        else {
            $transaction = JSON.stringify(dataa);

            $.ajax({
                url: 'updatepassword',
                type: 'POST',
                dataType: 'json',
                data: {'transaction': $transaction, 'uid': id},

                success: function (data) {

                    console.log("data", dataa);
                }
            });

            localStorage.clear('packages');

            $.ajax({
                url: 'logout',
                type: 'POST',
                dataType: 'json',
                data: {},

                success: function (data) {

                    console.log("data", data);
                }
            });
            location.replace('index');


            // cpassword.setCustomValidity('');
        }


    };


    $scope.adduseraction = function (ticketid) {

        var deferred = $q.defer();

        $transaction = (ticketid);

        $.ajax({
            url: 'transactiononlinelockvaldata',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderprintDetails = (response));

                console.log("6Success", (response));
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };
    $scope.getTotalprintonline = function () {
        var totalprintonline = 0;
        for (var i = 0; i < $scope.orderprintDetails.length; i++) {
            var product1 = $scope.orderprintDetails[i];
            totalprintonline += (product1.price * 1);
        }
        return totalprintonline;
    };

    $scope.addrefundtransactions = function (dataa) {

        var deferred = $q.defer();

        $transaction = JSON.stringify(dataa);

        $.ajax({
            url: 'transactiononlinerefundlockval',
            type: 'POST',
            dataType: 'json',
            data: {'transaction': $transaction},


            success: function (response) {

                deferred.resolve($scope.orderDetails = (response));
                console.log("1Success", (response));
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        deferred.promise;

    };

    $scope.addbooking = function (dataa) {
        $transactionbooking = JSON.stringify(dataa);

        var deferred = $q.defer();
        $.ajax({
            url: 'addbookinggtransactions',
            type: 'POST',
            dataType: 'json',
            data: {'transactionbooking': $transactionbooking},

            success: function (response) {

                deferred.resolve($scope.orderbooking = ((response)));

                console.log("33sSuccess", $scope.orderbooking);

                for (x = 0; x < $scope.orderbooking.length; x++) {
                    for (n = 0; n < $scope.orderbooking[x].qty; n++) {
                        if ($scope.orderbooking[x].txttod = 'PM') {
                            var a = Number($scope.orderbooking[x].tohrs || 0);
                            var b = Number(12 || 0);
                            $scope.orderbooking[x].tohrs1 = a + b;
                            // $scope.orderbooking[x].tohrs=$scope.orderbooking[x].tohrs+12;
                        }
                        else {
                            $scope.orderbooking[x].tohrs1 = $scope.orderbooking[x].tohrs;
                        }

                        var dataaajax = angular.copy({
                            packageId: '',
                            inventory_id: $scope.orderbooking[x].txttod,
                            package_name: $scope.orderbooking[x].name,
                            package_price: ($scope.orderbooking[x].price / $scope.orderbooking[x].qty),
                            inventory_seats: $scope.orderbooking[x].qty,
                            inventory_from: $scope.orderbooking[x].fromses,
                            inventory_to: $scope.orderbooking[x].tohrs1,
                            inventory_minfrom: $scope.orderbooking[x].frommin,
                            inventory_minto: $scope.orderbooking[x].tomin,
                            inventory_packagetype: 'Online',
                        });
                        $scope.packages.push(dataaajax);


                    }
                    localStorage.setItem('packages', JSON.stringify($scope.packages));
                    localStorage.setItem('lastorder', JSON.stringify($scope.packages));
                    location.reload();
                    console.log("sSuccess", $scope.orderbooking[x].qty);

                }

                // console.log("1sssSuccess", ((response)));
                //alert(JSON.parse(JSON.stringify(response)));

                /* compname
                     :
                     ""
                 entrydate
                     :
                     "23/07/2017"
                 frommin
                     :
                     "00"
                 fromses
                     :
                     "18"
                 gstno
                     :
                     ""
                 name
                     :
                     "Regular"
                 orderid
                     :
                     "92"
                 price
                     :
                     "1150"
                 ticketid
                     :
                     "5971a8c4a4d24"
                 tohrs
                     :
                     "7"
                 tomin
                     :
                     "00"
                 txtfromd
                     :
                     "PM "
                 txttod
                     :
                     "PM "*/


                /* for ($i = 1; $i <= noofpersons_list; $i++) {
                     var dataaajax = angular.copy({
                         packageId: package_id,
                         inventory_id: inventory_id,
                         package_name: package_name,
                         package_price: package_price,
                         inventory_seats: inventory_seats,
                         inventory_from: inventory_from,
                         inventory_to: inventory_to,
                         inventory_minfrom: inventory_minfrom,
                         inventory_minto: inventory_minto,
                         inventory_packagetype: 'Package',
                     });
                     $scope.packages.push(dataaajax);


                 }



                 localStorage.setItem('packages', JSON.stringify(response));
                 localStorage.setItem('lastorder', JSON.stringify(response));
 */
            }, error: function (error) {
                console.log("Error::", error.responseText);
                deferred.reject();
            }
        });
        //location.reload();


    };
    $scope.qty = [];
    $scope.getItemCollection = function (qty) {
        var collection = [];

        for (i = 0; i < qty; i++) {
            collection.push(qty);
        }
        return collection;
    };


    $scope.click_nocomp = function () {
        $.ajax({
            url: 'nocomplement',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function (data) {

                console.log("data", data);
            }
        });
        location.reload();
    };


    $scope.click_nocompval = function () {
        $.ajax({
            url: 'complementval',
            type: 'POST',
            dataType: 'json',
            data: {},

            success: function (data) {

                console.log("data", data);
            }
        });
        location.reload();
    };

    $scope.getaddonprintTotal = function () {

        var addonprinttotal = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprinttotal += (product.orderproduct_package_price * product.orderquant);
        }
        return addonprinttotal;
    };

    $scope.getaddonprintcgst = function () {

        var addonprintcgst = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprintcgst += (product.order_taxvalue * product.orderquant);
        }
        return addonprintcgst;
    };
    $scope.getaddonprintsgst = function () {

        var addonprintsgst = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprintsgst += (product.order_tax_percentage2 * product.orderquant);
        }
        return addonprintsgst;
    };


    $scope.getaddonprintigst = function () {

        var addonprintigst = 0;
        for (var i = 0; i < $scope.printaddonslist.length; i++) {
            var product = $scope.printaddonslist[i];

            addonprintigst += (product.order_tax_percentage3 * product.orderquant);
        }
        return addonprintigst;
    };
    $scope.cancelprintingdata = function () {

        $(".click-fun1-nonetransactions").css({"display": "none"});
        $(".click-fun1-nonesetting").css({"display": "none"});
        $(".click-fun1-nonerefundaddons").css({"display": "none"});
        $(".click-fun1-noneprintuser").css({"display": "none"});
        $(".click-fun1-noneprinttransaction").css({"display": "none"});
        $(".click-fun1-nonebookingprocess").css({"display": "none"});
        $(".click-fun1-noneprint").css({"display": "none"});
        $(".click-fun-none").css({"display": "none"});
        $(".click-fun-noneprint1").css({"display": "none"});
        $(".click-fun1-noneprint2").css({"display": "none"});
        $(".click-fun1-noneprintstep1").css({"display": "none"});
        $(".click-fun1-noneaddons").css({"display": "none"});
        $(".click-fun1-nonesetting").css({"display": "none"});
    }


});

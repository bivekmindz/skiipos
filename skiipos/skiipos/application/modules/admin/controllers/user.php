<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
    }


    public  function userviewstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 's_viewmail_status',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
           );
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
        redirect("admin/user/reguser");

    }


    public function reguserDelete1()
    {
        $parameter = array('act_mode' => 'delete_user',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->uri->segment('4'),);

        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter);
        redirect("admin/user/reguser");

    }


    public function editreguser()
    {

        if($this->input->post('submit')=='Submit')
        {

            $parameter1 = array('act_mode' => 's_viewfrontuseredit',
                'Param1' => $this->input->post('s_username'),
                'Param2' => $this->input->post('s_loginemail'),
                'Param3' => base64_encode($this->input->post('s_loginpassword')),
                'Param4' => $this->input->post('contact_no'),
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->input->post('uuid'),);
            //pend($parameter);
            $response['edit_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
            redirect("admin/user/reguser");
        }


        $parameter1 = array('act_mode' => 's_viewfrontuserdisplay',
            'Param1' =>'',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' =>  base64_decode($this->uri->segment('4')));
       
        $response['vieww_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);






        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/useredit', $response);
    }

    public function addreguser()
    {

        if($this->input->post('submit')=='Submit')
        {
           
            $parameter1 = array('act_mode' => 's_viewfrontuseradd',
                'Param1' => $this->input->post('s_username'),
                'Param2' => $this->input->post('s_loginemail'),
                'Param3' => base64_encode($this->input->post('s_loginpassword')),
                'Param4' => $this->input->post('contact_no'),
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            //pend($parameter);
          $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
redirect("admin/user/reguser");
        }
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/useradd', $response);
    }
    /*User from regestration time listing*/
    public function reguser()
    {

        $parameter1 = array('act_mode' => 's_viewfrontuser',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);


        //pend($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/frontuser', $response);

    }

}// end class
?>
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Ordercontrol extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
    }

    /*listing filter */
    public function order_sess(){

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }
  

$a = explode(":",$this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));
//p($a);
//p($b);

if($b['1'] == 'PM'){
   $session_time_param = $a['0'] + 12;
}
else{
   $session_time_param = $a['0'];
}
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

         $branchids = implode(',',$a);
         $filterstatus = implode(',',$b);
         $filtertime = implode(',',$c);
         $filtersta = implode(',',$d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
            );
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/ordercontrol/order');


    }


        /*view all orders*/
    public function order()
    {

     //p($_POST);
    
    //$this->session->unset_userdata('order_filter');

        if($this->session->userdata('order_filter'))
        {
          //p($this->session->userdata('order_filter'));
                 $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                 'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => '');
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
             //  echo $parameter1[$key];
            }

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);
          //p($response['vieww_order']);
          $this->session->unset_userdata('order_filter');



        }
        else {


            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
           
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
//pend($response['vieww_order']);

        }



       //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/orderlist',$response);
    }


    public function orderview()
    {

        $parameter = array('act_mode' => 'vieworderdet',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_orderdetail'] = $this->supper_admin->call_procedure('proc_order_s',$parameter);
        $parameter = array('act_mode' => 'vieworderPackages',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_orderdetail'] = $this->supper_admin->call_procedure('proc_order_s',$parameter);

        $parameter = array('act_mode' => 'updatetotal',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_ordertotal'] = $this->supper_admin->call_procedure('proc_order_s',$parameter);
        $parameter = array('act_mode' => 'updateorderprice',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' =>$response['vieww_ordertotal'][0]->price,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_ordertotal'] = $this->supper_admin->call_procedure('proc_order_s',$parameter);



        //p(  $response['vieww_orderdetail'] );
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/orderview',$response);
    }


    

}// end class
?>
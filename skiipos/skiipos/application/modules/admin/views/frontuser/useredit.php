<!DOCTYPE html>
<html><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<style>
    .error{
        color: red;
    }
    .tbl_input input[type="url"], .tbl_input input[type="number"], .tbl_input input[type="text"], .tbl_input input[type="email"], .tbl_input input[type="password"], .tbl_input select, .tbl_input textarea {
        padding: 9px 6px;
        border: solid 1px #E6E6E6;
        width: 100%;
    }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-init="init()">
<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant pad_ding">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Edit Employee</h2>
                    </div>
                    <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section, you can Edit Employee!
                                <!-- <a href="admin/retailer/viewmanagers"><button type="button" style="float:right;">CANCEL</button></a> --></p>
                            <p><div class='flashmsg'>
                                <?php echo validation_errors(); ?>
                                <?php
                                if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                            </div></p>
                        </div>
                    </div>
                    <form action="" id="mgrForm" name="mgrForm" method="post" enctype="multipart/form-data" >
                        <div class="page_box">
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Name <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" title="Please fill valid details." name="s_username" id="s_username" required  value="<?php echo $vieww_frontuser->user_firstname; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">USER ID <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input title="Please fill valid details."  id="s_loginemail" type="text" name="s_loginemail" required value="<?php echo $vieww_frontuser->user_username; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input" style="position: relative;">
                                                <p id="msg"></p>
                                                <input type="text" title="Please fill valid details." name="s_loginpassword" id="s_loginpassword" autocomplete="off" required value="<?php echo base64_decode($vieww_frontuser->user_password); ?>"/>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Contact No <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">

                                                <input type="number" title="Please fill valid details."  name="contact_no" id="contact_no" onkeyup="checknumber();" required value="<?php echo $vieww_frontuser->user_mobileno; ?>"/>

                                                <input type="hidden" title="Please fill valid details."  name="uuid"  value="<?php echo $vieww_frontuser->user_id; ?>"/>
                                                <!-- <input type="text" name="contact_no" id="contact_no" onkeyup="checknumber();" /> -->
                                            </div>
                                            <div class="tbl_input" id="errorsss"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input class="submitbtn"  type="submit" name="submit" value="Submit" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>





                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</body>

</html>


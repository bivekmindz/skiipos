<?php //pend($s_viewtimeslot); ?><!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    .to {
        margin-right: 10px;
    }

    .to_input {
        width: 67% !important;
    }

    .fonrr {
        font-size: 26px;
        margin-left: -14px;
        color: rgba(0, 0, 0, 0.43);
        margin-top: 7px;
    }

    .tbl_input {
        margin-bottom: 15px;
    }

    .table_add {
        width: 100%;
        float: left;
    }

    .timebox{ width:100%; float:left; font-size:14px; color:#777676;}

    .timebox button, input, select, textarea {padding: 3px 5px 3px 7px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        margin: 3px 9px 13px 9px;
    }

    .branche_time{ width:100%; float:left;}

    .branche_time li{padding: 3px 0px 12px 0px;}

    .branche_time li button{    padding: 8px 20px 8px 20px; margin-left: 15px; font-size:20px;}

</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD TIMESLOT</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div id="content">
                                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                        <li class="active"><a href="<?php  echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a></li>
                                        <li ><a href="<?php  echo base_url('admin/Taxes/s_tax') ?>" >Taxes</a></li>
                                        <li><a href="<?php echo base_url('admin/packages/addPackage') ?>" >Package</a></li>
                                        <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                        </li>
                                        <li ><a href="<?php  echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily Timeslot</a></li>
                                        <li ><a href="<?php  echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                        <li ><a href="<?php  echo base_url('admin/companys/s_addCompany') ?>">Add Company</a></li>
                                        <li ><a
                                                    href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                Payment</a></li>
                                        <li ><a
                                                    href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer Data
                                                Payment</a></li>
                                    </ul>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <form action="<?= base_url('admin/timeslots/addTimeslot') ?>" name="add_com" id="add_com"
                              method="post" enctype="multipart/form-data">


                            <div class="sep_box">

                                <div class="sep_box">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="tbl_text">Timeslot<span
                                                            style="color:red;font-weight: bold;">*</span></div>
                                            </div>


                                            <div class="col-md-9">

                                                <div class="row field_wrapper">

                                                    <div class="timebox">FROM:
                                                        <select name="field_name1[]" id="field_name1" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select>
                                                        <select name="minute_name1[]" id="minute_name1" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select>
                                                        TO:
                                                        <select name="field_name2[]" id="field_name2" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select>
                                                        <select name="minute_name2[]" id="minute_name2" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select>
                                                        <input id="no_of_seats" name="no_of_seats[]" placeholder="Seats available" type="number" min="0" max="1000"><a href="javascript:void(0);" class="add_button" title="Add field">ADD</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        var maxField = 20; //Input fields increment limitation
                                        var addButton = $('.add_button'); //Add button selector
                                        var wrapper = $('.field_wrapper'); //Input field wrapper
                                        var fieldHTML = '<div class="timebox">FROM<select name="field_name1[]" id="field_name1" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select><select name="minute_name1[]" id="minute_name1" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select>TO:<select name="field_name2[]" id="field_name2" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select> <select name="minute_name2[]" id="minute_name2" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select><input id="no_of_seats" name="no_of_seats[]" placeholder="Seats available" type="number" min="0" max="1000"><a href="javascript:void(0);" class="remove_button" title="Remove field">REMOVE</a></div>'; //New input field html
                                        var x = 1; //Initial field counter is 1
                                        $(addButton).click(function(){ //Once add button is clicked
                                            if(x < maxField){ //Check maximum number of input fields
                                                x++; //Increment field counter
                                                $(wrapper).append(fieldHTML); // Add field html
                                            }
                                        });
                                        $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
                                            e.preventDefault();
                                            $(this).parent('div').remove(); //Remove field html
                                            x--; //Decrement field counter
                                        });
                                    });
                                </script>

                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" onclick='
                                                    $("#branchids").attr("disabled",false)' name="submit" value="Submit"
                                                           class="btn_button sub_btn"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                        </form>


                        <div class="col-md-12 refreshtime" >

                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>

                                        <th>Branch TIME SLOT</th>
                                        <th>Branch TIME SEAT</th>



                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                $i=1;  foreach ($s_viewtimeslot as $v) {
  ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?php echo $v->timeslot_from;  ?> : <?php echo $v->timeslot_minfrom;  ?> - <?php echo $v->timeslot_to;  ?> : <?php echo $v->timeslot_minto;  ?> Hrs  </td> <td><?php echo $v->timeslot_seats;  ?></td>
   <td><button onclick="return deletesingletime(<?php echo $v->timeslot_id; ?>)">-</button></td>


                                    </tr>
                                        <?php
                                        $i++;  } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function deletesingletime(id,from,to) {

        var cou = confirm("ARE YOU WANT TO DELETE COMPLETELY?");
        if(cou == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admin/timeslots/deletesingletime",
                data: {
                    'id': id,
                    'from': from,
                    'to': to
                },

                success: function (data) {
                    $(".refreshtime").load(location.href + " .refreshtime");
                    return false;
                },
                error: function (data) {
                    // alert(data);
                }
            });
            return false;
        }

    }
</script>
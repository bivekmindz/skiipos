<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_addon").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                addon_name: "required",
                addon_desc: "required",
                branchids: "required",
                act_ids: "required",
                addon_price: {
                    "required": true,
                    "number": true,
                },
                addon_image : "required",

            },
            // Specify validation error messages
            messages: {
                addon_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                var ext = $('#addon_image').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('Please Select Valid Image!');
                    return false;
                }
                else {
                    $("#branchids").attr('disabled',false);
                    form.submit();
                }
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add addon</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li ><a href="<?php  echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a></li>

                                            <li ><a href="<?php  echo base_url('admin/Taxes/s_tax') ?>" >Taxes</a></li>
                                            <li ><a href="<?php echo base_url('admin/packages/addPackage') ?>" >Package</a></li>
                                            <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                            </li>
                                            <li ><a href="<?php  echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily Timeslot</a></li>
                                            <li class="active"><a href="<?php  echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                            <li ><a href="<?php  echo base_url('admin/companys/s_addCompany') ?>">Add Company</a></li>

                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                    Payment</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer Data
                                                    Payment</a></li>

                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form  action="<?= base_url('admin/cash/addcash') ?>" id="update_com" method="post" enctype="multipart/form-data" >


                            <form action="<?= base_url('admin/cash/addcash') ?>" name="add_addon" id="add_addon" method="post" enctype="multipart/form-data" >


                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text"> Name<span style="color:red;font-weight: bold;">*</span></div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <input type="text" id="addon_name" name="addon_name" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>







                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text"> Image<span style="color:red;font-weight: bold;">*</span></div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <input type="file" id="addon_image" name="addon_image" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>


                            <div class="col-md-12">
                                <div class="add_comp">
                                    <h2>Table</h2>
                                    <div class="table-responsive" style="width: 100%;">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <th>NAME</th>

                                                <th>IMAGE</th>
                                                <th>ACTION</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php  $i=1; foreach($vieww_cash as $v) {



                                                if($this->session->userdata('snowworld')->EmployeeId) {
                                                    if($this->session->userdata('snowworld')->branchid != $v->addon_branchid)
                                                    {
                                                        continue;
                                                    }
                                                }




                                                ?>
                                                <tr>
                                                    <td><?= $i;?></td>
                                                    <td id="t_act_name"><?php echo $v->cash_name; ?></td>




                                                    <td id="t_act_image">

                                                        <img height="100px;" src="<?= base_url('assets/admin/images')."/".$v->cash_image;?> "
                                                        <?php echo $v->addon_image; ?></td>
                                                    <td>


                                                        <a href="<?= base_url('admin/cash/addonstatus/'.base64_encode($v->cash_id) .'/'.base64_encode($v->cash_status).'') ?>"   class="btn btn-primary"><?php echo $v->cash_status == 0 ? "Inactive": "Active"; ?></a>

                                                        <a href="<?= base_url('admin/cash/cashviewupdate/'.base64_encode($v->cash_id) .'') ?>"   class="btn btn-primary">
                                                            <span class="fa fa-pencil" aria-hidden="true">Edit</span>
                                                        </a>

                                                        <a href="<?= base_url('admin/cash/cashdelete/'.$v->cash_id .'') ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">


                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </a>







                                                    </td>

                                                </tr>

                                                <?php $i++;} ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update addon</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('admin/addons/updateaddon') ?>" name="update_addon" id="update_addon" method="post" enctype="multipart/form-data" >


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update addon Name<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_addon_name" name="update_addon_name" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update addon Description<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <textarea  id="update_addon_desc" name="update_addon_desc" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="update_branchids[]" multiple id="update_branchids">
                                                <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                    <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="update_act_ids[]" multiple id="update_act_ids">
                                                <?php foreach ($vieww_act as $key => $value) { ?>
                                                    <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update addon Price<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_addon_price" name="update_addon_price" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>

</div>

<script>
    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data",
            allowClear: true
        }).select2('val', $('.select2 option:eq(0)').val());

        $("#updateaddonButton").on('click',function () {

            $("#update_act_ids,#update_branchids").select2({
                placeholder: "Select the Data",
                allowClear: true
            });



            /*var defaultData = [{id:1, text:'Item1'},{id:2,text:'Item2'},{id:3,text:'Item3'}];
             $('#update_act_ids').data().select2.updateSelection(defaultData);
             var a = $(this).parent('tr').find('ul [id=branch_map]').html('');
             console.log(a);*/

        });


        $("#act_ids").on('change',function () {

            var act =$(this).val().toString();
            var finalPrice = 0;
            var act_split = act.split(',');
            for(var count = 0 ; count < act_split.length ; count++)
            {
                $.ajax({
                    url: '<?php echo base_url()?>admin/addons/activityPrice',
                    type: 'POST',
                    dataType : "json",
                    data: {'act_id': act_split[count]},
                    success: function(data){
                        if(data.length > 0) {
                            finalPrice = finalPrice +  parseInt(data[0].activity_price);
                        }
                        $("#addon_price").val(finalPrice);
                    }
                });
            }

            //console.log(act_split.length);
            return false;

        })
    })







</script>

<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                com_name: "required",
            },
            // Specify validation error messages
            messages: {
                com_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });



        $("#update_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                com_name_update: "required",
            },
            // Specify validation error messages
            messages: {
                com_name_update: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD Company</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li ><a href="<?php  echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a></li>
                                            <li ><a href="<?php  echo base_url('admin/Taxes/s_tax') ?>" >Taxes</a></li>
                                            <li><a href="<?php echo base_url('admin/packages/addPackage') ?>" >Package</a></li>
                                            <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                            </li>
                                            <li ><a href="<?php  echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily Timeslot</a></li>
                                            <li ><a href="<?php  echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                            <li class="active" ><a href="<?php  echo base_url('admin/companys/s_addCompany') ?>" >Add Company</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                    Payment</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer Data
                                                    Payment</a></li>


                                        </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <form style="display: none" action="<?= base_url('admin/companys/s_addcompany') ?>" id="update_com" method="post" enctype="multipart/form-data" >
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Company Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="com_name_update" name="com_name_update" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update GST No<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="com_gst_update" name="com_gst_update" >
                                                <input type="hidden" name="com_id" id="com_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>





                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn_update" type="submit" name="submit_update" value="Update" class="btn_button sub_btn" />

                                                <input id="submitBtn_update" type="button" onclick="return UpdateCancel()" name="submit_update" value="Cancel" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <form  action="<?= base_url('admin/companys/s_addcompany') ?>" name="add_com" id="add_com"  method="post" enctype="multipart/form-data" >



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Company Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="com_name" name="com_name" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">GST No<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="com_gstname" name="com_gstname" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Company NAME</th>
                                        <th>GST No</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww as $v) {?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_com_name"><?php echo $v->comp_name; ?></td>
                                            <td id="t_com_gstname"><?php echo $v->comp_gstname; ?></td>
                                            <td>

                                                <a class="btn btn-primary" onclick="return update_com('<?php echo $v->comp_name; ?>','<?php echo $v->comp_gstname; ?>','<?php echo $v->comp_id; ?>')">
                                                 Edit   <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/companys/company_delete/'.$v->comp_id .'') ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function update_com(a,b,c){
        //alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(c);
        $("#com_gst_update").val(b);

        $("#update_com").css('display','block');
        $("#add_com").css('display','none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display','block');
        $("#update_com").css('display','none');
        return false;
    }





</script>
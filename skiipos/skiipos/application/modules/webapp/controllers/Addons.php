<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Addons extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }









    public function addons()
    {
        if($this->session->userdata('skiindiauserid')==''){
            redirect(base_url());
        }
        else
        {
            //        $dateval=date("d/m/Y");
//        $parameter2 = array( 'act_mode'=>'front_viewpackage',
//            'Param1'=>$dateval,
//            'Param2'=>'',
//            'Param3'=>'',
//            'Param4'=>'',
//            'Param5'=>'',
//            'Param6'=>'',
//            'Param7'=>'',
//            'Param8'=>'',
//            'Param9'=>'');
//        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
//       print_r(json_encode( $response['s_viewpackages']));

//        $this->load->view("pos", $response);
            $this->load->view("addons");
        }





    }

    public function printvalue()
    {
        p($this->session->userdata);
    }
    public function logout()
    {

        $this->session->unset_userdata('skiindiauserid');
        $this->session->unset_userdata('orderid');
    }

    public function addonData()
    {

        if ($this->session->userdata('complement') != '') {
            $response['complementval1'] = $this->session->userdata('complement');
        } else {
            $response['complementval1'] = 0;
        }

        $parameter2 = array( 'act_mode'=>'front_viewaddons',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewaddons'] = $this->supper_admin->call_procedure('proc_addon_s',$parameter2);


//p(response['complementval1']);


        print_r(json_encode($response));

    }




    public function packages()
    {


        $dateval=date("d/m/Y");
        $parameter2 = array( 'act_mode'=>'front_viewpackage',
            'Param1'=>$dateval,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $this->load->view("packages", $response);
        $this->load->view("helper/left", $response);
    }

    public function addonesadd()
    {
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
        }
        else

        {
            $dateval1=date("d/m/Y");
        }

        $selected_data =json_decode($_REQUEST['packages'],true);

//        p($selected_data);
        /*        [packageId] => 1
                    [inventory_id] => 27
                    [package_name] => Normal
                    [package_price] => 1000
                    [inventory_seats] => 200
                    [inventory_from] => 15
                    [inventory_to] => 16
                    [inventory_minfrom] => 00
                    [inventory_minto] => 30
        */


        $parameter1 = array( 'act_mode'=>'s_addorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
            'Param3'=> 'Addons',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        $data = array(
            'skiindiauseruniqid' => uniqid(),
            'orderid' => $response['s_vieworder'][0]->order_id,

        );

        $this->session->set_userdata($data);
        foreach($selected_data as $k => $v):

            $parameter2 = array( 'act_mode'=>'s_addpackage_master',
                'Param1'=>$v['packageId'],
                'Param2'=> $v['inventory_id'],
                'Param3'=> $v['package_name'],
                'Param4'=> $v['package_price'],
                'Param5'=> $v['inventory_seats'],
                'Param6'=>$v['inventory_from'],
                'Param7'=>$v['inventory_to'],
                'Param8'=>$v['inventory_minfrom'],
                'Param9'=>$v['inventory_minto'],
                'Param10'=>$response['s_vieworder'][0]->order_id,
                'Param11' => $v['inventory_packagetype'],
                'Param12'=>$dateval1,
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',

            );
      //  pend($parameter2 );
            $response['s_viewaddons'] = $this->supper_admin->call_procedure('proc_order',$parameter2);

        endforeach;

        return true;
    }

    public function printinglockval($data)
    {
        $selected_data =json_decode($_REQUEST['printing'],true);

        // $selected_data['transid']

        $parameter1 = array( 'act_mode'=>'s_printinglockval',
            'Param1'=>$selected_data['transid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }
}//end of class
?>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Printv extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }






    public function printvalue()
    {
        $this->session->unset_userdata('noofpersons');


        // tax add .client request last time if we start initial it take more time in  i hrs it is best option
        $parametertax = array( 'act_mode'=>'s_selectorder_tax',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_viewordertax'] = $this->supper_admin->call_procedure('proc_order',$parametertax);


        foreach($response['s_viewordertax'] as $v)
        {


            $parametertaxinsert = array( 'act_mode'=>'s_inserttorder_tax',
                'Param1' => $v->orderproduct_id,
                'Param2'=>$v->tax_name,
                'Param3'=> $v->tax_value,
                'Param4' => $v->tax_name1,
                'Param5' => $v->tax_name2,
                'Param6' => $v->tax_name3,
                'Param7' => $v->tax_percentage2,
                'Param8' => $v->tax_percentage3,
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15' => $this->session->userdata('orderid'),

            );


            $response['s_viewordertaxinsert'] = $this->supper_admin->call_procedure('proc_order',$parametertaxinsert);
        }




        $parameter1 = array( 'act_mode'=>'s_selectorder_total',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedurerow('proc_order',$parameter1);


        $parameter2 = array( 'act_mode'=>'s_updateorder_total',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$response['s_vieworder']->pacprice,
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['updateorder'] = $this->supper_admin->call_procedurerow('proc_order',$parameter2);

        $this->load->view("printpage");
    }




    public function printData()
    {
        //  pend($this->session->userdata('orderid'));

        //Company Master
        $response['s_vieworderid'] = $this->session->userdata('orderid');

        $parametercompamast = array('act_mode' => 's_selectorder_compamast',
            'Param1' => $this->session->userdata('orderid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_viewcompamast'] = $this->supper_admin->call_procedure('proc_order', $parametercompamast);


        // parameter paymentcashdata
        $parameterpaymentcashdata = array('act_mode' => 's_paymentcashdata',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_paymentcashamount'] = $this->supper_admin->call_procedure('proc_order', $parameterpaymentcashdata);

        $parametercashamount = array('act_mode' => 's_orderdata_totalpayment',
            'Param1' => $this->session->userdata('orderid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_cashamount'] = $this->supper_admin->call_procedurerow('proc_order', $parametercashamount);


        $parametercashamountdisplay = array('act_mode' => 's_orderdata_totalcashdisplay',
            'Param1' => $this->session->userdata('orderid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_cashamountdisplaydata'] = $this->supper_admin->call_procedure('proc_order', $parametercashamountdisplay);


        $parameterorder = array( 'act_mode'=>'s_orderdata_printdisplay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_orderdatarder'] = $this->supper_admin->call_procedure('proc_order',$parameterorder);
       // p( $parameterorder );
//p( $response['s_orderdatarder'] );




        $parameter1 = array( 'act_mode'=>'s_addorder_printdisplayvalprintpage',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);


        $parameter1 = array('act_mode' => 's_addorder_printdisplayvalpackages',
            'Param1' => $this->session->userdata('orderid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworderpackages'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        $parameter1 = array('act_mode' => 's_addorder_printdisplayvaladdons',
            'Param1' => $this->session->userdata('orderid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworderaddons'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        // display user name
        $parameterlog = array('act_mode' => 'memusersesiid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );

        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameterlog);
// display last order total
        $parameterorder = array('act_mode' => 'memuserorderid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_orderlog'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameterorder);
        $parameter4 = array('act_mode' => 'cashviewfrontend',
            'Param1' => $this->session->userdata('orderid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['cashview'] = $this->supper_admin->call_procedure('proc_cash_s', $parameter4);


        $response['orderid'] =$this->session->userdata('orderid');


        $parameter5 = array( 'act_mode'=>'s_addorder_cashdisplay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['scashdisplay'] = $this->supper_admin->call_procedure('proc_order',$parameter5);

        print_r(json_encode( $response));
    }

    public function logout()
    {

        $this->session->unset_userdata('skiindiauserid');
        $this->session->unset_userdata('orderid');
    }

    public function cancelitem()
    {

        $parameter1 = array( 'act_mode'=>'s_cancelorddisplay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);


        return true;
        //  $this->session->unset_userdata('skiindiauserid');
        // $this->session->unset_userdata('orderid');
    }


    public function addcash()
    {
        $selected_data =json_decode($_REQUEST['datacash'],true);

        $parameter1 = array( 'act_mode'=>'s_addorder_amountval',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$this->session->userdata('skiindiauserid'),
            'Param3'=>'Cash',
            'Param4'=>$selected_data['fName'],
            'Param5'=>$selected_data['mobileno'],
            'Param6'=>$selected_data['desc'],
            'Param7'=>0,
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

 $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

       return true;
    }



    public function addcredit()
    {
        $selected_data =json_decode($_REQUEST['datacash'],true);

        $parameter1 = array( 'act_mode'=>'s_addorder_amountvalprintpay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$this->session->userdata('skiindiauserid'),
            'Param3'=>'Credit',
            'Param4'=>$selected_data['fName'],
            'Param5'=>$selected_data['mobileno'],
            'Param6'=>$selected_data['desc'],
            'Param7'=>0,
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        return true;
    }

    public function addpaymentdynamic()
    {
   /*     [/addpaymentdynamic] =>
    [orderid] => 8
    [printpayment_name] => DLF
    [printpayment_value] => 100
    [getTotal] => 5300*/
     // pend($_REQUEST['printpayment_name']);


        //$selected_data =json_decode($_REQUEST['datacash'],true);

        $parameter1 = array( 'act_mode'=>'s_addorder_amountvalprintpay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$this->session->userdata('skiindiauserid'),
            'Param3'=>$_REQUEST['printpayment_name'],
            'Param4'=>$selected_data['fName'],
            'Param5'=>$selected_data['mobileno'],
            'Param6'=>$selected_data['desc'],
            'Param7'=>$_REQUEST['printpayment_value'],
            'Param8'=>($_REQUEST['getTotal']-($_REQUEST['getTotal']*$_REQUEST['printpayment_value']/100)),
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        return true;
    }



    public function addFunccancel()
    {
        $selected_data =json_decode($_REQUEST['datacash'],true);

        $parameter1 = array( 'act_mode'=>'s_addorder_amountval',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$this->session->userdata('skiindiauserid'),
            'Param3'=>'Cancel',
            'Param4'=>$selected_data['fName'],
            'Param5'=>$selected_data['mobileno'],
            'Param6'=>$selected_data['desc'],
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        return true;
    }
    public function adddebdit()
    {
        $selected_data =json_decode($_REQUEST['datacash'],true);

        $parameter1 = array( 'act_mode'=>'s_addorder_amountvalprintpay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$this->session->userdata('skiindiauserid'),
            'Param3'=>'Debit',
            'Param4'=>$selected_data['fName'],
            'Param5'=>$selected_data['mobileno'],
            'Param6'=>$selected_data['desc'],
            'Param7'=>0,
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        return true;
    }


    public function casesadd()
    {
     /*   [cash_id] => 5
            [cash_name] => 1000
            [cash_status] => 1
            [cash_image] => 58af37287f9a3c4649545567ceaf8820.jpg
    [cash_total] => 1000
            [cash_counter] => 1
            [cash_orderid] => 10*/
        $selected_data = json_decode($_REQUEST['cashones'], true);

        foreach ($selected_data as $k => $v):

            $parameter1 = array('act_mode' => 's_viewcash',
                'Param1' => $v['cash_id'],
                'Param2' =>  $v['cash_name'],
                'Param3' => $v['cash_total'],
                'Param4' =>  $v['cash_counter'],
                'Param5' => $v['cash_orderid'],
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10' => '',
                'Param11' => '',
                'Param12' => '',
                'Param13' => '',
                'Param14' => '',
                'Param15' => '',

            );


            $response['s_viewcash'] = $this->supper_admin->call_procedurerow('proc_order', $parameter1);

if($response['s_viewcash']->casesid!=$v['cash_id'])
{
    $parameter1 = array('act_mode' => 's_addcash',
        'Param1' => $v['cash_id'],
        'Param2' =>  $v['cash_name'],
        'Param3' => $v['cash_total'],
        'Param4' =>  $v['cash_counter'],
        'Param5' => $v['cash_orderid'],
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',
        'Param10' => '',
        'Param11' => '',
        'Param12' => '',
        'Param13' => '',
        'Param14' => '',
        'Param15' => '',

    );


    $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
}
else
{
    $parameter1 = array('act_mode' => 's_updatecash',
        'Param1' => $v['cash_id'],
        'Param2' =>  $v['cash_name'],
        'Param3' => $v['cash_total'],
        'Param4' =>  $v['cash_counter'],
        'Param5' => $v['cash_orderid'],
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',
        'Param10' => '',
        'Param11' => '',
        'Param12' => '',
        'Param13' => '',
        'Param14' => '',
        'Param15' => '',

    );


    $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
}

        endforeach;
    }
    public function casesupdate()
    {

        $selected_data =json_decode($_REQUEST['cashones'],true);

        foreach ($selected_data as $k => $v):
        $parameter1 = array('act_mode' => 's_updatecash',
            'Param1' => $v['cash_id'],
            'Param2' =>  $v['cash_name'],
            'Param3' => $v['cash_total'],
            'Param4' =>  $v['cash_counter'],
            'Param5' => $v['cash_orderid'],
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);

        endforeach;

    }

    public function deletecashtot()
    {
        $selected_data = json_decode($_REQUEST['orderid'], true);


        $parameter1 = array('act_mode' => 's_deletecashorderid',
            'Param1' => $selected_data,
            'Param2' =>  '',
            'Param3' => '',
            'Param4' =>  '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
    }
//Delete Total Cash
    public function deletecashval()
    {

        $parameter1 = array('act_mode' => 's_deletecashvalid',
            'Param1' => $_REQUEST['cash_id'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        return true;

    }


}//end of class
?>
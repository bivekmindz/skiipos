<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Advancebooking extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }




    public function advancebookingpos()
    {


        if($this->session->userdata('skiindiauserid')==''){
            redirect(base_url());
        }
        else
        {
            if( $this->session->userdata('datedataval')!='')
            {
                $response['dateval1']=  $this->session->userdata('datedataval');
            }
            else

            {
                $response['dateval1']=date("d/m/Y");
            }




            $this->load->view("advancebookingpos", $response);
        }





    }


    public function dateval()
    {
        $selected_data =$_REQUEST['datevalue'];

      //[datevalue] => 08/23/2017
     $selecteddate=explode("/",$selected_data);


$datedata=$selecteddate[1]."/".$selecteddate[0]."/".$selecteddate[2];

        $data = array(
            'datedataval' => $datedata,


        );

        $this->session->set_userdata($data);
      //  p($this->session->userdata('datedataval'));
        return true;
    }

    public function logout()
    {

        $this->session->unset_userdata('skiindiauserid');
        $this->session->unset_userdata('orderid');
        $this->session->unset_userdata('datedataval');
    }

    public function posData()
    {
//Date
if( $this->session->userdata('datedataval')!='')
{
    $dateval1=  $this->session->userdata('datedataval');
    $response['dateval1']=  $this->session->userdata('datedataval');
}
else

{
    $dateval1=date("d/m/Y");
    $response['dateval1']=date("d/m/Y");
}
//Packages

        $parameter2 = array( 'act_mode'=>'front_viewpackage',
            'Param1'=>$dateval1,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);




        foreach($response['s_viewpackages'] as $key=>$value)

        {


            $parameter4 = array( 'act_mode'=>'front_viewpackageqty',
                'Param1'=>$dateval1,
                'Param2'=>$value->dailyinventory_from,
                'Param3'=>'',
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'');
            $response['s_viewpackagesqty'][$key] = $this->supper_admin->call_procedurerow('proc_packages_s',$parameter4);



            $data = explode(',',$response['s_viewpackages'][$key]->package_id);

            $response['s_viewpackages'][$key]->package_id = ((object)
            $data);

            $packaiteamcount=explode(",",$response['s_viewpackagesqty'][$key]->iteamcount);
            $packahename=explode(",",$response['s_viewpackages'][$key]->package_name);
            $packaheprice=explode(",",$response['s_viewpackages'][$key]->package_price);
            $packahetax=explode(",",$response['s_viewpackages'][$key]->package_tax);
            $packcolor=explode(",",$response['s_viewpackages'][$key]->package_color);
            $packtextcolor=explode(",",$response['s_viewpackages'][$key]->package_fontcolor);


            $response['s_viewpackages'][$key]->package_name = ((object)
            $packahename);
            $response['s_viewpackages'][$key]->package_price = ((object)
            $packaheprice);
            $response['s_viewpackages'][$key]->package_tax = ((object)
            $packahetax);
            $response['s_viewpackages'][$key]->package_color = ((object)
            $packcolor);
            $response['s_viewpackages'][$key]->package_fontcolor = ((object)
            $packtextcolor);
            $response['s_viewpackages'][$key]->package_iteamcount= ((object)
            $packaiteamcount);

        }
        $parameter1 = array('act_mode' => 's_advanceboking',
            'Param1' => $dateval1,
            'Param2' => '',
        );

        $response['advanceboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter1);



        // display user name
        $parameterlog = array('act_mode' => 'memusersesiid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );

        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameterlog);
// display last order total
        $parameterorder = array('act_mode' => 'memuserorderid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_orderlog'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameterorder);


//p( $response['advanceboking']);
   echo json_encode( $response);

     //  print_r(json_encode( $response['s_viewpackages']));

    }




    public function packages()
    {


      $dateval=date("d/m/Y");
        $parameter2 = array( 'act_mode'=>'front_viewpackage',
            'Param1'=>$dateval,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $this->load->view("packages", $response);
        $this->load->view("helper/left", $response);
    }

    public function packagesadvadd()
    {
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
        }
        else

        {
            $dateval1=date("d/m/Y");
        }
        $selected_data =json_decode($_REQUEST['packages'],true);

//        p($selected_data);
/*        [packageId] => 1
            [inventory_id] => 27
            [package_name] => Normal
            [package_price] => 1000
            [inventory_seats] => 200
            [inventory_from] => 15
            [inventory_to] => 16
            [inventory_minfrom] => 00
            [inventory_minto] => 30
*/


        $parameter1 = array( 'act_mode'=>'s_addorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
            'Param3'=> 'Packages',
            'Param4'=> $dateval1,
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


      $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        $parameter10 = array( 'act_mode'=>'s_advaddorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
            'Param3'=> 'Packages',
            'Param4'=> $dateval1,
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworderadv'] = $this->supper_admin->call_procedure('proc_order',$parameter10);
        $data = array(
            'skiindiauseruniqid' => uniqid(),
            'orderid' => $response['s_vieworder'][0]->order_id,

                );

        $this->session->set_userdata($data);

        foreach($selected_data as $k => $v):

            $parameter2 = array( 'act_mode'=>'s_addpackage_master',
                'Param1'=>$v['packageId'],
                'Param2'=> $v['inventory_id'],
                'Param3'=> $v['package_name'],
                'Param4'=> $v['package_price'],
                'Param5'=> $v['inventory_seats'],
                'Param6'=>$v['inventory_from'],
                'Param7'=>$v['inventory_to'],
                'Param8'=>$v['inventory_minfrom'],
                'Param9'=>$v['inventory_minto'],
                'Param10'=>$response['s_vieworder'][0]->order_id,
                'Param11'=>'Packages',
                'Param12'=>$dateval1,
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',


            );
 $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_order',$parameter2);


            $parameter12 = array( 'act_mode'=>'s_addpackageadv_master',
                'Param1'=>$v['packageId'],
                'Param2'=> $v['inventory_id'],
                'Param3'=> $v['package_name'],
                'Param4'=> $v['package_price'],
                'Param5'=> $v['inventory_seats'],
                'Param6'=>$v['inventory_from'],
                'Param7'=>$v['inventory_to'],
                'Param8'=>$v['inventory_minfrom'],
                'Param9'=>$v['inventory_minto'],
                'Param10'=>$response['s_vieworderadv'][0]->order_id,
                'Param11'=>'Packages',
                'Param12'=>$dateval1,
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',


            );
            $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_order',$parameter12);


        endforeach;

        return true;
    }


}//end of class
?>
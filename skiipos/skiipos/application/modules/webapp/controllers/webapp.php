<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Webapp extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }





    //Index page
    public function landing()
    {


        if(( $this->session->userdata('skiindiauserid'))=='')
        {
        if($this->input->post('submit')) {

            $parameter1 = array('act_mode' => 's_frontuserlogin',
                'Param1' => $this->input->post('emailmob'),
                'Param2' => base64_encode($this->input->post('pwd')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',);
          //  pend($parameter1);
            $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);


            if(empty($response['select_frontuser']) )
            {
                $response['msg']="No Record Found";

            }
            else {

                $data = array(
                    'skiindiauserid' => $response['select_frontuser']->user_id,
                    'skiindiaemailid' => $response['select_frontuser']->emailmob,

                );

               $this->session->set_userdata($data);

// client-ip.php : Demo script by nixCraft <www.cyberciti.biz>
// get an IP address
                $ip = $_SERVER['REMOTE_ADDR'];


                $parameter2 = array('act_mode' => 'memuserlog',
                    'user_id' => $response['select_frontuser']->user_id,
                    'remoteaddress' => gethostbyaddr($ip),
                    );
              //  pend($parameter2);
                $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);


header("Location:pos");
            }
        }
            $this->load->view("landingpage", $response);

        }
        else
{
    header("Location:pos");
}}




    public function pos()
    {
        $hrs = date('H');
        //echo($t . "<br>");

      //  $this->session->unset_userdata('noofpersons');
        if($this->session->userdata('skiindiauserid')==''){
            redirect(base_url());
        }
        else
        {
            if( $this->session->userdata('datedataval')!='')
            {
                $response['dateval1']=  $this->session->userdata('datedataval');
            }
            else

            {
                $response['dateval1']=date("d/m/Y");
            }

            if ($this->session->userdata('complement') != '') {
                $response['complementval1'] = $this->session->userdata('complement');
            } else {
                $response['complementval1'] = 0;
            }




            if($this->input->post('submit')) {

                $parameter1 = array('act_mode' => 's_frontuserlogin',
                    'Param1' => $this->input->post('emailmob'),
                    'Param2' => base64_encode($this->input->post('pwd')),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '',);
                //  pend($parameter1);
                $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);


                if(empty($response['select_frontuser']) )
                {
                    $response['msg']="No Record Found";

                }
                else {

                    $data = array(
                        'skiindiauserid' => $response['select_frontuser']->user_id,
                        'skiindiaemailid' => $response['select_frontuser']->emailmob,

                    );

                    $this->session->set_userdata($data);
                    $parameter2 = array('act_mode' => 'memuserlog',
                        'user_id' => $response['select_frontuser']->user_id,
                        'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
                    );
                    //  pend($parameter2);
                    $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);


                    header("Location:pos");
                }
            }

            $parameter2 = array('act_mode' => 'selectmemuser',
                'user_id' =>  $this->session->userdata('skiindiauserid'),
                'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
            );
            //  pend($parameter2);
            $response['select_user'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameter2);


            //        $dateval=date("d/m/Y");
//        $parameter2 = array( 'act_mode'=>'front_viewpackage',
//            'Param1'=>$dateval,
//            'Param2'=>'',
//            'Param3'=>'',
//            'Param4'=>'',
//            'Param5'=>'',
//            'Param6'=>'',
//            'Param7'=>'',
//            'Param8'=>'',
//            'Param9'=>'');
//        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
//       print_r(json_encode( $response['s_viewpackages']));

//        $this->load->view("pos", $response);
            $this->load->view("pos", $response);
        }





    }


    public function dateval()
    {
        $selected_data =$_REQUEST['datevalue'];

      //[datevalue] => 08/25/2017
     $selecteddate=explode("/",$selected_data);

//p( $selected_data);
$datedata=$selecteddate[1]."/".$selecteddate[0]."/".$selecteddate[2];

        $data = array(
            'datedataval' => $datedata,


        );

        $this->session->set_userdata($data);
      //  p($this->session->userdata('datedataval'));
        return true;
    }

    public function logout()
    {

        $this->session->unset_userdata('skiindiauserid');
        $this->session->unset_userdata('orderid');
        $this->session->unset_userdata('datedataval');
    }

    public function posData()
    {
        if($this->session->userdata('nameadv')!='')
        {
            $response['nameadvance'] =$this->session->userdata('nameadv');
        }
        else
        {
            $response['nameadvance']='';
        }
//Date Select
if( $this->session->userdata('datedataval')!='')
{
    $dateval1=  $this->session->userdata('datedataval');
    $response['dateval1']=  $this->session->userdata('datedataval');
}
else

{
    $dateval1=date("d/m/Y");
    $response['dateval1']=date("d/m/Y");
}

        if ($this->session->userdata('complement') != '') {
            $response['complementval1'] = $this->session->userdata('complement');
        } else {
            $response['complementval1'] = 0;
        }

//No Of Persons Select
        if( $this->session->userdata('noofpersons')!='')
        {
                $response['noofpersons']=  $this->session->userdata('noofpersons');
        }
        else

        {
              $response['noofpersons']=1;
        }

        $hrs = date('H');

//Display Advance booking timing
        $parametertimeslotadv = array( 'act_mode'=>'s_viewdailytimeslotadvance',
            'Param1'=>$dateval1,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslotadv'] = $this->supper_admin->call_procedure('proc_timeslot_v',$parametertimeslotadv);

        //Display Advance booking Packages

        $parameterpackageadv = array( 'act_mode'=>'s_viewdailypackageadv',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_parameterpackageadv'] = $this->supper_admin->call_procedure('proc_packages_s',$parameterpackageadv);


// Display Inventory and packages
        if ($dateval1 == date("d/m/Y")) {
            $parameter2 = array('act_mode' => 'front_viewpackage',
                'Param1' => $dateval1,
                'Param2' => $hrs,
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
        } else {
            $parameter2 = array('act_mode' => 'front_viewpackagewithouthrs',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
        }

        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);


        //single packages

        $parameterpackagecomplete = array('act_mode' => 'front_viewpackagecomplete',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['s_packagename'] = $this->supper_admin->call_procedure('proc_packages_s', $parameterpackagecomplete);




        // display user name
        $parameterlog = array('act_mode' => 'memusersesiid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );

        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameterlog);
// display last order total
       // pend($response['select_userlog'] );


        $parameterorder = array('act_mode' => 'memuserorderid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_orderlog'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameterorder);

// display packages

        foreach($response['s_viewpackages'] as $key=>$value)

        {


            $parameter4 = array( 'act_mode'=>'front_viewpackageqty',
                'Param1'=>$dateval1,
                'Param2'=>$value->dailyinventory_from,
                'Param3'=>'',
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'');
            $response['s_viewpackagesqty'][$key] = $this->supper_admin->call_procedurerow('proc_packages_s',$parameter4);



            $data = explode(',',$response['s_viewpackages'][$key]->package_id);

            $response['s_viewpackages'][$key]->package_id = ((object)
            $data);

            $packaiteamcount=explode(",",$response['s_viewpackagesqty'][$key]->iteamcount);
            $packahename=explode(",",$response['s_viewpackages'][$key]->package_name);
            $packaheprice=explode(",",$response['s_viewpackages'][$key]->package_price);
            $packahetax=explode(",",$response['s_viewpackages'][$key]->package_tax);
            $packcolor=explode(",",$response['s_viewpackages'][$key]->package_color);
            $packtextcolor=explode(",",$response['s_viewpackages'][$key]->package_fontcolor);


            $response['s_viewpackages'][$key]->package_name = ((object)
            $packahename);
            $response['s_viewpackages'][$key]->package_price = ((object)
            $packaheprice);
            $response['s_viewpackages'][$key]->package_tax = ((object)
            $packahetax);
            $response['s_viewpackages'][$key]->package_color = ((object)
            $packcolor);
            $response['s_viewpackages'][$key]->package_fontcolor = ((object)
            $packtextcolor);
            $response['s_viewpackages'][$key]->package_iteamcount= ((object)
            $packaiteamcount);

        }
        $datedata=explode("/",$dateval1);

        $dateval2=$datedata[1]."/".$datedata[0]."/".$datedata[2];
        $parameter1 = array('act_mode' => 's_advanceboking',
            'Param1' => $dateval2,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',

        );

        $response['advanceboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter1);

        $parameter10 = array('act_mode' => 's_boking',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
        );

        $response['totalboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter10);

//footer data

        $parameter11 = array('act_mode' => 's_footerdata',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['footerboking'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter11);


//p( $response['advanceboking']);
   echo json_encode( $response);

     //  print_r(json_encode( $response['s_viewpackages']));

    }




    public function packages()
    {


      $dateval=date("d/m/Y");
        $parameter2 = array( 'act_mode'=>'front_viewpackage',
            'Param1'=>$dateval,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $this->load->view("packages", $response);
        $this->load->view("helper/left", $response);
    }

    public function packagesadd()
    {
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
        }
        else

        {
            $dateval1=date("d/m/Y");
        }
        $selected_data =json_decode($_REQUEST['packages'],true);

//        p($selected_data);
/*        [packageId] => 1
            [inventory_id] => 27
            [package_name] => Normal
            [package_price] => 1000
            [inventory_seats] => 200
            [inventory_from] => 15
            [inventory_to] => 16
            [inventory_minfrom] => 00
            [inventory_minto] => 30
*/

        $ip = $_SERVER['REMOTE_ADDR'];


        $parameter1 = array( 'act_mode'=>'s_addorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
            'Param3'=> 'Packages',
            'Param4'=> $dateval1,
            'Param5' => gethostbyaddr($ip),
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        $data = array(
            'skiindiauseruniqid' => uniqid(),
            'orderid' => $response['s_vieworder'][0]->order_id,

                );

        $this->session->set_userdata($data);

        foreach($selected_data as $k => $v):

            $parameter2 = array( 'act_mode'=>'s_addpackage_master',
                'Param1'=>$v['packageId'],
                'Param2'=> $v['inventory_id'],
                'Param3'=> $v['package_name'],
                'Param4'=> $v['package_price'],
                'Param5'=> $v['inventory_seats'],
                'Param6'=>$v['inventory_from'],
                'Param7'=>$v['inventory_to'],
                'Param8'=>$v['inventory_minfrom'],
                'Param9'=>$v['inventory_minto'],
                'Param10'=>$response['s_vieworder'][0]->order_id,
                'Param11' => $v['inventory_packagetype'],
                'Param12'=>$dateval1,
                'Param13' => gethostbyaddr($ip),
                'Param14'=>'',
                'Param15'=>'',


            );
 $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_order',$parameter2);

        endforeach;

        return true;
    }

    //Total Booking
    public function totalbooking()
    {
        $hrs = date('H');

        if ($this->session->userdata('skiindiauserid') == '') {
            redirect(base_url());
        } else {
            if ($this->session->userdata('datedataval') != '') {
                $response['dateval1'] = $this->session->userdata('datedataval');
            } else {
                $response['dateval1'] = date("d/m/Y");
            }

            if ($this->session->userdata('complement') != '') {
                $response['complementval1'] = $this->session->userdata('complement');
            } else {
                $response['complementval1'] = 0;
            }


            if ($this->input->post('submit')) {

                $parameter1 = array('act_mode' => 's_frontuserlogin',
                    'Param1' => $this->input->post('emailmob'),
                    'Param2' => base64_encode($this->input->post('pwd')),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '',);
                //  pend($parameter1);
                $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);


                if (empty($response['select_frontuser'])) {
                    $response['msg'] = "No Record Found";

                } else {

                    $data = array(
                        'skiindiauserid' => $response['select_frontuser']->user_id,
                        'skiindiaemailid' => $response['select_frontuser']->emailmob,

                    );

                    $this->session->set_userdata($data);
                    $parameter2 = array('act_mode' => 'memuserlog',
                        'user_id' => $response['select_frontuser']->user_id,
                        'remoteaddress' => $_SERVER['REMOTE_ADDR'],
                    );
                    //  pend($parameter2);
                    $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);


                    header("Location:totalbooking");
                }
            }

        }
        $parameter2 = array('act_mode' => 'selectmemuser',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' => $_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_user'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameter2);

        $this->load->view("totalbooking", $response);

    }

    public function advancebookingadd()
    {
        $selected_data =json_decode($_REQUEST['advancebooking'],true);
/*
        [name] => ytryrty
    [mobile] => 4564564
    [noofpersons] => 5
    [email] => snowworld@gmail.com
    [date] => 08/30/2017
    [packagetime] => 206
    [packagename] => 3
    [payment] => 5*/
        $parameter2 = array( 'act_mode'=>'s_addadv',
            'Param1'=>$selected_data['name'],
            'Param2'=> $selected_data['mobile'],
            'Param3'=> $selected_data['noofpersons'],
            'Param4'=> $selected_data['email'],
            'Param5'=> $selected_data['packagetime'],
            'Param6'=>$selected_data['packagename'],
            'Param7'=>$selected_data['payment'],
            'Param8'=>$selected_data['date'],
            'Param9'=>'',
            'Param10'=>'',



        );
        $response['advancebooking'] = $this->supper_admin->call_procedure('proc_advanceboking',$parameter2);


        return $selected_data;
    }


 public function advancebookingaddorder()
    {

  if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
        }
        else

        {
            $dateval1=date("d/m/Y");
        }

         $parameter1 = array( 'act_mode'=>'s_addorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
            'Param3'=> 'Packages',
            'Param4'=> $dateval1,
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        $data = array(
            'skiindiauseruniqid' => uniqid(),
            'orderid' => $response['s_vieworder'][0]->order_id,

                );

        $this->session->set_userdata($data);
for($i=0; $i<$_REQUEST['advancepayment_noofpersons'];$i++) {
 $parameter2 = array( 'act_mode'=>'s_addpackage_master',
                'Param1'=>$_REQUEST['package_id'],
                'Param2'=> $_REQUEST['dailyinventory_id'],
                'Param3'=> $_REQUEST['package_name'],
                'Param4'=> $_REQUEST['package_price'],
                'Param5'=> $_REQUEST['dailyinventory_seats'],
                'Param6'=>$_REQUEST['dailyinventory_from'],
                'Param7'=>$_REQUEST['dailyinventory_to'],
                'Param8'=>$_REQUEST['dailyinventory_minfrom'],
                'Param9'=>$_REQUEST['dailyinventory_minto'],
                'Param10'=>$response['s_vieworder'][0]->order_id,
                'Param11'=>'Packages',
                'Param12'=>$dateval1,
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',


            );
 $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_order',$parameter2);
}


  //  [advancepayment_id] => 3

  //   [advancepayment_name] => yty
  //   [advancepayment_mobile] => 6575675656
  //   [advancepayment_noofpersons] => 4
  //   [advancepayment_email] => durgesh@chiliadprocons.in
   //  [advancepayment_entrydate] => 08/28/2017
   //  [dailyinventory_from] => 13
   //  [dailyinventory_minfrom] => 00
   //  [dailyinventory_to] => 14
   //  [dailyinventory_minto] => 30
   //  [package_name] => School
  //   [package_price] => 700
  //   [advancepayment_payment] => 44
  //   [rembalancepaidby] => y
//[package_id] => 3
  //  [dailyinventory_id] => 194
  //  [dailyinventory_seats] => 200

    }
    public function groupbooking()
    {
        $groupbooking_data =json_decode($_REQUEST['groupbooking'],true);

        $data = array(
       'noofpersons' =>$groupbooking_data['noofpersons'],

     );

       $this->session->set_userdata($data);
        return true;
    }
    public function unsetsessions()
    {
        $this->session->unset_userdata('noofpersons');
    }
    public function userlock()
    {

        $parameter2 = array('act_mode' => 'memuserlock',
            'user_id' =>  $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);
return true;
           }


    public function refundlockvaladdons($data)
    {
        $selected_data = json_decode($_REQUEST['printing'], true);

        // $selected_data['transid']

        $parameter1 = array('act_mode' => 's_refundaddonslockval',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }

    public function refundlockvalticket($data)
    {
        $selected_data = json_decode($_REQUEST['printing'], true);

        // $selected_data['transid']

        $parameter1 = array('act_mode' => 's_refundticketlockval',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }

    public function printingaddonslockval($data)
    {
        $selected_data = json_decode($_REQUEST['printing'], true);

        // $selected_data['transid']

        $parameter1 = array('act_mode' => 's_printingaddonslockval',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        $parametercompamast = array('act_mode' => 's_selectorder_compamast',
            'Param1' => $response['s_vieworder'][0]->order_id,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_viewcompamast'] = $this->supper_admin->call_procedure('proc_order', $parametercompamast);

        $parameteraddons = array('act_mode' => 's_addorder_printdisplayvaladdons',
            'Param1' => $response['s_vieworder'][0]->order_id,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworderaddons'] = $this->supper_admin->call_procedure('proc_order', $parameteraddons);

        $response['s_vieworderid'] = $response['s_vieworder'][0]->order_id;

        echo json_encode($response);
//p(($response['s_vieworder']));
    }


    public function printinglockval($data)
    {
        $selected_data =json_decode($_REQUEST['printing'],true);

       // $selected_data['transid']

        $parameter1 = array( 'act_mode'=>'s_printinglockval',
            'Param1'=>$selected_data['transid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }
    public function printinglockvaldata($data)
    {
     //   $selected_data =json_decode($_REQUEST['orderproduct_id'],true);

        // $selected_data['transid']
      //  pend($_REQUEST['orderproduct_id']);
for($i=0;$i<=count($_REQUEST['orderproduct_id']);$i++) {


    $parameter1 = array('act_mode' => 's_addorder_printdisplayval',
        'Param1' => $_REQUEST['orderproduct_id'][$i],
        'Param2' => '',
        'Param3' => '',
        'Param4' => '',
        'Param5' => '',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',
        'Param10' => '',
        'Param11' => '',
        'Param12' => '',
        'Param13' => '',
        'Param14' => '',
        'Param15' => '',

    );

   $response['printdisplayval'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
  echo json_encode($response['printdisplayval']);
//p($response['printdisplayval']);
}   }

//cancelorderbooking
    public function cancelorderbooking($orderDetails,$typeofpayment)
    {

        $selected_data =json_decode($_REQUEST['grouporderDetails'],true);
        $ip = $_SERVER['REMOTE_ADDR'];
        $parameter1 = array( 'act_mode'=>'s_cancelorderbooking',
            'Param1'=>$selected_data[0]['order_id'],
            'Param2'=>$_REQUEST['typeofpayment'],
            'Param3'=>'Refund',
            'Param4' => gethostbyaddr($ip),
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_cancelorderbooking'] = $this->supper_admin->call_procedure('proc_order',$parameter1);


        return true;


    }


    public function cancelorderbookingaddons($orderDetails, $typeofpayment)
    {

        $selected_data = json_decode($_REQUEST['grouporderDetails'], true);
        $ip = $_SERVER['REMOTE_ADDR'];
        $parameter1 = array('act_mode' => 's_cancelorderbookingaddons',
            'Param1' => $selected_data[0]['order_id'],
            'Param2' => $_REQUEST['typeofpayment'],
            'Param3' => 'Refund',
            'Param4' => gethostbyaddr($ip),
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_cancelorderbooking'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        return true;


    }


    public function cancelorderbookingtransactions($orderDetails, $typeofpayment)
    {

        $selected_data = json_decode($_REQUEST['grouporderDetails'], true);

        $parameter1 = array('act_mode' => 's_cancelorderbookingtransactions',
            'Param1' => $selected_data[0]['orderid'],
            'Param2' => $_REQUEST['typeofpayment'],
            'Param3' => 'Refund',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_cancelorderbooking'] = $this->supper_admin->call_procedure('proc_order', $parameter1);

        return true;


    }

//Transaction Online print


    public function transactiononlinelockvaldata()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactionofflineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        echo json_encode($response['s_transactiononlineprint']);

    }




    public function transactiononlinelockval()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactiononlineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_order', $parameter1);


        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);


        $parameter3 = array('act_mode' => 's_selecttransactiononlineprint',
            'Param1' => $selected_data['transid'],
            'Param2' => $response['s_transactiononlineprint']->pacorderid,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintdata'] = $this->supper_admin->call_procedure('proc_order', $parameter3);


        echo json_encode($response['s_transactiononlineprintdata']);

    }

//Online user value
    public function onlineuserval()
    {

        if ($this->session->userdata('datedataval') != '') {
            $dateval1 = $this->session->userdata('datedataval');
        } else {
            $dateval1 = date("d/m/Y");
        }


//31/08/2017
        // $selected_date = '13/08/2017';
//Select

        $parameter1 = array('act_mode' => 's_selectnofflineuserselectdata',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['onlineuser'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        echo json_encode($response['onlineuser']);


    }

    public function updatepassword()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        p($selected_data);
        /*     [/updatepassword] =>
         [transaction] => {"newpassword":"ssssss","cpassword":"ssssss"}
         [uid] => 1

        [newpassword] => 111111
         [cpassword] => 111111

        */

        $parameter1 = array(
            'UserId' => $_REQUEST['uid'],
            'newpassword' => base64_encode($selected_data['newpassword']),


        );

        $response['changepass'] = $this->supper_admin->call_procedure('proc_changePassword', $parameter1);

        return true;
    }

//Transaction Online print
    public function transactiononlinerefundlockval()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactiononlineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_order', $parameter1);


        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);


        $parameter3 = array('act_mode' => 's_selecttransactiononlineprint',
            'Param1' => $selected_data['transid'],
            'Param2' => $response['s_transactiononlineprint']->pacorderid,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintdata'] = $this->supper_admin->call_procedure('proc_order', $parameter3);


        echo json_encode($response['s_transactiononlineprintdata']);


    }

//bookinggtransactions
    public function addbookinggtransactions($transactionbooking)
    {
        $selected_data = json_decode($_REQUEST['transactionbooking'], true);


//p( $response['s_bookinggtransactions']);

//Select

        $parameter1 = array('act_mode' => 's_selecttransactiononlineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => $selected_data['name'],
            'Param3' => $selected_data['bookingno'],
            'Param4' => $selected_data['phoneno'],
            'Param5' => $selected_data['emailid'],
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_orderbooking', $parameter1);


        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);


        $parameter3 = array('act_mode' => 's_orderbooking_master',
            'Param1' => $selected_data['transid'],
            'Param2' => $selected_data['name'],
            'Param3' => $selected_data['bookingno'],
            'Param4' => $selected_data['phoneno'],
            'Param5' => $selected_data['emailid'],
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_bookinggtransactions'] = $this->supper_admin->call_procedure('proc_orderbooking', $parameter3);

        echo json_encode($response['s_bookinggtransactions']);


    }

    public function nocomplement()
    {
        $data = array(
            'complement' => 1,


        );

        $this->session->set_userdata($data);
        return true;

    }

    public function complementval()
    {
        $data = array(
            'complement' => 0,


        );

        $this->session->set_userdata($data);
        return true;

    }


}//end of class
?>
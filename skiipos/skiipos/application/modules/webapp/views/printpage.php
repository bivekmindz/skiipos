<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/bootstrap.min.css">

    <script type="text/javascript" src="assets/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/frontend/js/jquery-3.2.1.min.js"></script>
    <script src="assets/frontend/js/angular.min.js"></script>
    <script src="assets/frontend/js/angular-route.js"></script>
    <script language="javascript" src="assets/frontend/js/angular-animate.js"></script>
    <script language="javascript" src="assets/frontend/js/ui-bootstrap-tpls-0.14.3.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">

</head>

<body class="inner_body" ng-app="myApp" ng-controller="PrintpageCntrl">


<div ng-view>
</div>

<script src="assets/frontend/js/app/app.js"></script>

<script src="assets/frontend/js/app/printval-router.js"></script>
<script src="assets/frontend/js/app/controllers/PrintpageCntrl.js"></script>

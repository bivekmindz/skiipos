</!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/bootstrap.min.css">
    <script type="text/javascript" src="assets/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/frontend/js/jquery-3.2.1.min.js"></script>

    <script language="javascript" src="assets/frontend/js/angular.js"></script>
    <script language="javascript" src="assets/frontend/js/angular-animate.js"></script>
    <script language="javascript" src="assets/frontend/js/ui-bootstrap-tpls-0.14.3.js"></script>


    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">



</head>
<body class="inner_body">
<div class="left_section">
    <div class="page_section">
        <div class="page_left_div page_top">
            <div class="page_tab">
                <div class="page_tab_d">Tickets</div>
            </div>
            <div class="page_tab_content">
                <div class="cont_left">

                    <?php  foreach($s_viewpackages as $viewpackages) { ?>
                    <div class="page_row">
                        <div class="tab_con_box_1">
                            <div class="tab_box_1">

                                <button class="button-all">
                                    <div class="button_box_left">
                                        <div class="back-per" style="width:60%;"></div>
                                        <div class="app-package">
                                            <div class="session_name">Packages</div>
                                            <div class="session_time"><span class="left">500/0/0</span><span class="right">
                                                    <?php

                                                    $desttype=$viewpackages->dailyinventory_from;
 $desttypeto=$viewpackages->dailyinventory_to;
                                                    if($desttype>12)
                                                    { $desttype= ($desttype-12);
                                                        $apmvar1="PM";}elseif($desttype==12) {
                                                        $apmvar1="PM";
                                                    }else {

                                                        $apmvar1="AM";
                                                    }

                                                    if($desttypeto>12)
                                                    { $desttypeto= ($desttypeto-12);
                                                        $apmvar1="PM";}elseif($desttypeto==12) {
                                                        $apmvar1="PM";
                                                    }else {

                                                        $apmvar1="AM";
                                                    }



                                                    echo $desttype; ?>:<?php  echo $viewpackages->dailyinventory_minfrom; ?> <?php echo $apmvar1; ?> - <?php  echo  $desttypeto; ?>:<?php  echo $viewpackages->dailyinventory_minto; ?> <?php echo $apmvar; ?>
                                                </span></div>
                                        </div>
                                    </div>

                                </button>
                            </div>
                        </div>
                        <div class="tab_con_box_2">

                            <?php $packaheval=explode(",",$viewpackages->package_id);
                            $packahename=explode(",",$viewpackages->package_name);
                            $packaheprice=explode(",",$viewpackages->package_price);
                            $packahetax=explode(",",$viewpackages->package_tax);

                            for ($x = 0; $x <= (count($packaheval)-1); $x++) {

      ?>



                            <div class="tab_box">
                                <div class="button_box ">
                                    <span class="type_text"><?php     echo ($packahename[$x]);  ?></span>
                                    <span class="price_text">Rs. <?php     echo ($packaheprice[$x]);  ?>.00</span>
                                </div>
                            </div>

                            <?php } ?>


                        </div>

                    </div>


                <?php } ?>








                    <!--new More Menu-->

                    <div class="more click-fun-none">

                        <ul class="more_menu">
                            <li><a href="javascript:void(0);"><img src="assets/frontend/images/clock.png" alt="clock" height="20px" width="32px"></a></li>
                            <li><a href="javascript:void(0);">Refresh</a></li>
                            <li><a href="javascript:void(0);">Swap</a></li>
                            <li><a href="javascript:void(0);">Find Booking</a></li>
                            <li><a href="javascript:void(0);">Member Card</a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);">Printing</a></li>
                            <li><a href="javascript:void(0);">Group</a></li>
                            <li><a href="javascript:void(0);">Open</a></li>
                            <li><a href="javascript:void(0);">Refund</a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);">Settings</a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);">Alt Lang</a></li>
                            <li><a href="javascript:void(0);">Compl</a></li>
                            <li><a href="javascript:void(0);">Lock</a></li>
                            <li><a href="javascript:void(0);">Void</a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);">Receipt</a></li>
                            <li><a href="javascript:void(0);">Hold Date</a></li>
                            <li><a href="javascript:void(0);"></a></li>
                            <li><a href="javascript:void(0);">Close</a></li>
                            <li><a href="javascript:void(0);">Log Off</a></li>
                            <li><a href="javascript:void(0);">Multi</a></li>
                            <li><a href="javascript:void(0);">Sched</a></li>
                            <li><a href="javascript:void(0);">Book</a></li>
                            <li><a href="javascript:void(0);">Abort</a></li>
                            <li><a href="javascript:void(0);">Discount</a></li>
                            <li><a href="javascript:void(0);">Seats</a></li>
                        </ul>


                    </div>


                    <!--NEw More Menu End-->

                </div>
                <div class="cont_right">
                    <div class="cont_right_tab">
                        <ul>
                            <li><div class="right_box clock_1"></div></li>
                            <li><div class="right_box clock_2"></div></li>
                            <li><div class="right_box calender_1"></div></li>
                            <li><div class="right_box r_box">Now</div></li>

                            <form>
                                <div ng-app="myApp" ng-controller="myCntrl">
                                    <input  style="visibility:hidden;" type="text" uib-datepicker-popup="{{dateformat}}" ng-model="dt" is-open="showdp" />
                                    <span>
             <button type="button" class="btn-cal btn-default-box" ng-click="showcalendar($event)">
                <i class="glyphicon glyphicon-calendar"></i>
        </span>
                                </div>

                                </button>
                            </form>	<!--<li><div class="right_box calender"></div></li>-->
                            <li><div class="right_box calender_2"></div></li>
                            <li><div class="right_box clock_3"></div></li>
                            <li><div class="right_box clock_4"></div></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="full_width">
            <div class="more_nav click-fun"><a href="#">More</a></div>
            <div class="nav_div">
                <div class="page_left_div">
                    <ul class="main_nav">
                        <li><a href="javascript:void(0);">Log Off</a></li>

                        <li><a href="javascript:void(0);">Add-Ons</a></li>
                        <li><a href="javascript:void(0);">Advance Booking</a></li>

                        <li><a href="javascript:void(0);">Invoice Facility</a></li>
                        <li><a href="javascript:void(0);">Total Booking</a></li>
                        <li><a href="javascript:void(0);">Seats</a></li>




                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class userfunction {
	public function paramiter($parameter = array()) {
		$qtag = null;
		$size = sizeof($parameter);
		if ($size > 0) {
			for ($i = 1; $i <= $size; $i++) {
				$qtag[] = '?';
			}
			return implode(',', $qtag);
		} else {
			return false;
		}
	}
}
?>
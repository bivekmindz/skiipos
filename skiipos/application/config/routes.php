<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


/* B2c */
$route['default_controller'] = 'webapp/webapp/landing';
$route['packages'] = 'webapp/webapp/packages';
$route['pos'] = 'webapp/webapp/pos';
$route['dateval'] = 'webapp/webapp/dateval';
$route['logout'] = 'webapp/webapp/logout';
$route['index'] = 'webapp/webapp/landing';
$route['advancebookingadd'] = 'webapp/webapp/advancebookingadd';
$route['advancebookingaddorder'] = 'webapp/webapp/advancebookingaddorder';
$route['groupbooking'] = 'webapp/webapp/groupbooking';
$route['unsetsessions'] = 'webapp/webapp/unsetsessions';
$route['cancelorderbooking'] = 'webapp/webapp/cancelorderbooking';
$route['refundlockvalticket'] = 'webapp/webapp/refundlockvalticket';
$route['refundlockvaladdons'] = 'webapp/webapp/refundlockvaladdons';
$route['cancelorderbookingaddons'] = 'webapp/webapp/cancelorderbookingaddons';
$route['transactiononlinerefundlockval'] = 'webapp/webapp/transactiononlinerefundlockval';
$route['cancelorderbookingtransactions'] = 'webapp/webapp/cancelorderbookingtransactions';
$route['addbookinggtransactions'] = 'webapp/webapp/addbookinggtransactions';
$route['addbookinggtransactions'] = 'webapp/webapp/addbookinggtransactions';
$route['printingaddonslockval'] = 'webapp/webapp/printingaddonslockval';
$route['transactiononlinelockuservaldata'] = 'webapp/webapp/transactiononlinelockuservaldata';
$route['groupbookingvalue'] = 'webapp/webapp/groupbookingvalue';
$route['onlinebookingstatusupdate'] = 'webapp/webapp/onlinebookingstatusupdate';

$route['offlinebookingstatusupdate'] = 'webapp/webapp/offlinebookingstatusupdate';


$route['releasepbooking'] = 'webapp/webapp/releasepbooking';


$route['holdpbooking'] = 'webapp/webapp/holdpbooking';
$route['transactiononlinelockvaldata'] = 'webapp/webapp/transactiononlinelockvaldata';
$route['totalbookingposData'] = 'webapp/webapp/totalbookingposData';


$route['addons'] = 'webapp/Addons/addons';
$route['addonData'] = 'webapp/Addons/addonData';
$route['addonesadd'] = 'webapp/Addons/addonesadd';

$route['posData'] = 'webapp/webapp/posData';
$route['packagesadd'] = 'webapp/webapp/packagesadd';
$route['userlock'] = 'webapp/webapp/userlock';
$route['printinglockval'] = 'webapp/webapp/printinglockval';
$route['printinglockvaldata'] = 'webapp/webapp/printinglockvaldata';
$route['transactiononlinelockval'] = 'webapp/webapp/transactiononlinelockval';
$route['onlineuserval'] = 'webapp/webapp/onlineuserval';
$route['updatepassword'] = 'webapp/webapp/updatepassword';
$route['nocomplement'] = 'webapp/webapp/nocomplement';
$route['complementval'] = 'webapp/webapp/complementval';

$route['addbookinggofflinetransactions'] = 'webapp/webapp/addbookinggofflinetransactions';



$route['timeslotadd'] = 'webapp/Cronejob/timeslotadd';
$route['print'] = 'webapp/Printv/printvalue';
$route['printData'] = 'webapp/Printv/printData';
$route['printcmdtoposprinter'] = 'webapp/Printv/printCmdToPosPrinter';
$route['cancelitem'] = 'webapp/Printv/cancelitem';
$route['addcash'] = 'webapp/Printv/addcash';
$route['casesadd'] = 'webapp/Printv/casesadd';
$route['casesupdate'] = 'webapp/Printv/casesupdate';
$route['deletecash'] = 'webapp/Printv/deletecash';
$route['addcredit'] = 'webapp/Printv/addcredit';
$route['adddebdit'] = 'webapp/Printv/adddebdit';
$route['addpaymentdynamic'] = 'webapp/Printv/addpaymentdynamic';
$route['deletecashval'] = 'webapp/Printv/deletecashval';
$route['updatechangevalue'] = 'webapp/Printv/updatechangevalue';

$route['printvel'] = 'webapp/Printv/printvel';


$route['addFunccancel'] = 'webapp/Printv/addFunccancel';
$route['deletecashtot'] = 'webapp/Printv/deletecashtot';
$route['totalbooking'] = 'webapp/webapp/totalbooking';
$route['advancebookingpos'] = 'webapp/advancebooking/advancebookingpos';
$route['packagesadvadd'] = 'webapp/advancebooking/packagesadvadd';


$route['admin']		 		 = 'admin/login';


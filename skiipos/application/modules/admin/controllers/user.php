<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
    }

    public  function userpermissionviewstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 's_viewmailpermission_status',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
        );
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
        redirect("admin/user/regpermissions");

    }


    public  function userviewstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 's_viewmail_status',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
           );
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
        redirect("admin/user/reguser");

    }

        public  function uservieloginwstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 's_viewmail_loginstatus',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
           );
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
        redirect("admin/user/reguser");

    }


    public function reguserDelete1()
    {
        $parameter = array('act_mode' => 'delete_user',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->uri->segment('4'),);

        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter);
        redirect("admin/user/reguser");

    }
    public function reguserpermissionDelete1()
    {
        $parameter = array('act_mode' => 'delete_userpermissions',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->uri->segment('4'),);

        $response['vieww'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter);
        redirect("admin/user/regpermissions");

    }
    public function editreguserpermission()
    {
        $parameter = array('act_mode' => 'viewmenu',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);


        $parameter = array('act_mode' => 'get_menu_submenu',
            'Param1' => '100',
            'Param2' => base64_decode($this->uri->segment('4')),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //p($parameter);
        $response['menusubmenu'] = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);


        if($this->input->post('submit')=='Submit')
        {

            $parameter1 = array('act_mode' => 's_viewfrontuserpermissionedit',
                'Param1' => $this->input->post('s_username'),
                'Param2' => implode(",",$this->input->post('mainmenu')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->input->post('empoid'),);
            //pend($this->input->post('empoid'));


            $response['edit_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);



            redirect("admin/user/regpermissions");
        }


        $parameter1 = array('act_mode' => 's_viewfrontuserpermissiondisplay',
            'Param1' =>'',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' =>  base64_decode($this->uri->segment('4')));

        $response['vieww_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);






        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/userpermissionedit', $response);
    }
    public function editreguser()
    {
        $parameter = array('act_mode' => 'viewmenuadmin',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);

        $parameter = array('act_mode' => 's_viewfrontuseradminpermissiondisplay',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_permission'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter);



        $parameter = array('act_mode' => 'get_menu_submenu',
            'Param1' => '100',
            'Param2' => base64_decode($this->uri->segment('4')),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //p($parameter);
        $response['menusubmenu'] = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);


        if($this->input->post('submit')=='Submit')
        {


            $parameter1 = array('act_mode' => 's_viewfrontuseredit',
                'Param1' => $this->input->post('s_username'),
                'Param2' => $this->input->post('s_loginemail'),
                'Param3' => base64_encode($this->input->post('s_loginpassword')),
                'Param4' => $this->input->post('contact_no'),
                'Param5' => $this->input->post('logintype'),
                'Param6' => $this->input->post('mainmenu'),
                'Param7' =>  $this->input->post('s_tenentid'),
                'Param8' => '',
                'Param9' => $this->input->post('uuid'));
            //pend($this->input->post('empoid'));
            $response['edit_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);

            $parameter = array('act_mode' => 'addasignrole_DELETE',
                'Param1' => 100,
                'Param2' => $this->input->post('empoid'),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response['deleteitems'] = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);
            //p($response['vieww_menu']);


            foreach ($response['vieww_menu'] as $a) {
                //$valuefooter=explode("-",$a);

              $parameter = array('act_mode' => 'updateemp_role',
                    'Param1' => $a->footer_id,
                    'Param2' => $this->input->post('empoid'),
                    'Param3' => '0',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => $this->input->post('uuid'));
                $response['roleee'] = (array)$this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);

//p($response['roleee']);

            }

            $parameter = array('act_mode' => 'addasignrole_edit',
                'Param1' => 100,
                'Param2' => $this->input->post('mainmenu'),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->input->post('empoid'));
            $response['addasignrole'] = $this->supper_admin->call_procedurerow('proc_userrolemanage_s', $parameter);

//p($response['addasignrole']->userpermission_data);
$valueassignrole=explode(",",$response['addasignrole']->userpermission_data);
            foreach ($valueassignrole as $a) {
                $parameter = array('act_mode' => 'updateemp_roleupdate',
                    'Param1' => $a,
                    'Param2' => $this->input->post('empoid'),
                    'Param3' => '1',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => $this->input->post('uuid'));
              $response['roleee'] = (array)$this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);

            }



           redirect("admin/user/reguser");
        }


        $parameter1 = array('act_mode' => 's_viewfrontuserdisplay',
            'Param1' =>'',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' =>  base64_decode($this->uri->segment('4')));
       
        $response['vieww_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);






        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/useredit', $response);
    }

    public function addreguser()
    {
        $parameter1 = array('act_mode' => 's_viewfrontuseredit',
            'Param1' => $this->input->post('s_username'),
            'Param2' => $this->input->post('s_loginemail'),
            'Param3' => base64_encode($this->input->post('s_loginpassword')),
            'Param4' => $this->input->post('contact_no'),
            'Param5' => $this->input->post('logintype'),
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->input->post('uuid'),);
        //pend($this->input->post('empoid'));
        $response['edit_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
        $parameter = array('act_mode' => 's_viewfrontuseradminpermissiondisplay',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_permission'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter);

        if($this->input->post('submit')=='Submit')
        {
            $parameter = array('act_mode' => 'viewmenuadmin',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);
            $parameter1 = array('act_mode' => 's_viewfrontuseradd',
                'Param1' => $this->input->post('s_username'),
                'Param2' => $this->input->post('s_loginemail'),
                'Param3' => base64_encode($this->input->post('s_loginpassword')),
                'Param4' => $this->input->post('contact_no'),
                'Param5' => $this->input->post('logintype'),
                'Param6' => $this->input->post('mainmenu'),
                'Param7' => $this->input->post('s_tenentid'),
                'Param8' => '',
                'Param9' => '');
            //pend($parameter);
            $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);



            foreach ($response['vieww_menu'] as $a) {


               // $valuefooter=explode("-",$a);

                $parameter = array('act_mode' => 'addemp_role',
                    'Param1' => $a->footer_id,
                    'Param2' => $response['vieww_frontuser']['0']->dd,
                    'Param3' => 0,
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                $response['roleee'] = (array)$this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);

  //p($parameter);
//p($response['roleee']);
            }

            $parameter = array('act_mode' => 'addasignrole_edit',
                'Param1' => 100,
                'Param2' => $this->input->post('mainmenu'),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' =>  $response['vieww_frontuser']['0']->dd);
            $response['addasignrole'] = $this->supper_admin->call_procedurerow('proc_userrolemanage_s', $parameter);
            $valueassignrole=explode(",",$response['addasignrole']->userpermission_data);

            foreach ($valueassignrole as $a) {
                $parameter = array('act_mode' => 'updateemp_roleupdate',
                    'Param1' => $a,
                    'Param2' => $response['vieww_frontuser']['0']->dd,
                    'Param3' => '1',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => $response['vieww_frontuser']['0']->dd);
                $response['roleee'] = (array)$this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);

            }
//p($response['addasignrole']->userpermission_data);
            $valueassignrole=explode(",",$response['addasignrole']->userpermission_data);
            redirect("admin/user/reguser");
        }

        $parameter = array('act_mode' => 'viewmenu',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_frontmenu_s', $parameter);



        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/useradd', $response);
    }

    public function addreguserpermissions()
    {

        if($this->input->post('submit')=='Submit')
        {

            $parameter1 = array('act_mode' => 's_viewfrontuserpermissionadd',
                'Param1' => $this->input->post('s_username'),
                'Param2' => implode(",",$this->input->post('mainmenu')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
               //pend($parameter);
            $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);




            redirect("admin/user/regpermissions");
        }

        $parameter = array('act_mode' => 'viewmenu',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_frontmenu_s', $parameter);



        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/useraddpermissions', $response);
    }




    /*User from regestration time listing*/
    public function reguser()
    {

        $parameter1 = array('act_mode' => 's_viewfrontuser',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);


        //pend($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/frontuser', $response);

    }



    public function regpermissions()
    {

        $parameter1 = array('act_mode' => 's_viewfrontuseradminpermission',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);


        //pend($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('frontuser/frontuserpermissions', $response);

    }




    public function get_menu_submenu()
    {
        $parameter = array('act_mode' => 'get_menu_submenu',
            'Param1' => $this->input->post('roleid'),
            'Param2' => $this->input->post('empid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //p($parameter);
        $response = $this->supper_admin->call_procedure('proc_userrolemanage_s', $parameter);
        print_r(json_encode($response));
    }
}// end class
?>
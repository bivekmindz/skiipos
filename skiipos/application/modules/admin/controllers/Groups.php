<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Groups extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
 $this->load->library('session');

        //$this->userfunction->loginAdminvalidation();
    }


    public  function Groupstatus($id,$status)
    {
        $status==1 ? $status=0:$status=1;

        $parameter = array('act_mode' => 'update_status_groups',
            'Param1' => $id,
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_groupbooking', $parameter);
        redirect(base_url() . 'admin/Groups/s_group');


    }



    public function Groupsupdate()
    {
        if ($this->input->post('submit'))
        {

            $this->form_validation->set_rules('group_name', 'group_name', 'required');
            $this->form_validation->set_rules('group_noofpersonfrom', 'group_noofpersonfrom', 'required');
            $this->form_validation->set_rules('group_noofpersonto','group_noofpersonto','required');

            if ($this->form_validation->run() != FALSE)
            {
                $from=$this->input->post('group_noofpersonfrom');
                $to=$this->input->post('group_noofpersonto');
                $gid=$this->input->post('group_id');

                if($from >= $to)
                {
                   // pend('hggdjlfg');exit;
                    $this->session->set_flashdata('message', 'Number Of Person From Greater Than Number Of Person To');
                    redirect(base_url() . 'admin/Groups/Groupsupdate/'.$gid);
                }
                else
                {
                    //pend('gdfgdufhgd');
                    $parameter10 = array(
                        'act_mode' => 's_editgroup_from_range',
                        'Param1' => $gid,
                        'Param2' => $from,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                   // pend($parameter10);
                    $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter10);
                   // pend($response);

                    if($response->c > 0)//to check from alredy exits
                    {
                        $this->session->set_flashdata('message', 'Number Of Person From Already Exits');
                        redirect(base_url() . 'admin/Groups/Groupsupdate/'.$gid);
                    }


                    $parameter11 = array('act_mode' => 's_editgroup_to_range',
                        'Param1' => $gid,
                        'Param2' => $to,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter11);

                    $response2 = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter11);
                    //pend($response2);


                    if ($response2->c > 0)  //to check to alredy exits
                    {
                        $this->session->set_flashdata('message', 'Number Of Person To Already Exits');
                        redirect(base_url() . 'admin/Groups/Groupsupdate/'.$gid);

                    }


//
                    //inserted
                    $parameter12 = array(
                        'act_mode' => 'sedit_update',
                        'Param1' => $this->input->post('group_name'),
                        'Param2' => $from,
                        'Param3' => $to,
                        'Param4' => $this->input->post('packages'),
                        'Param5' => $gid,
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                   //pend($parameter12);
                    $response3 = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter12);




                    $this->session->set_flashdata('message', 'Updated Sucessfully');

                    redirect(base_url() . 'admin/Groups/s_group');

                }

            }

        }





        $parameter2 = array('act_mode' => 's_viewgroupsupdateval',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->uri->segment('4'));
        $response['s_viewgroups'] = $this->supper_admin->call_procedurerow('proc_groupbooking', $parameter2);

       // pend($response['s_viewgroups']);
        $parameter4 = array('act_mode' => 's_viewpackageadmin',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('groups/editgroups', $response);

    }



    /*Add branch by zzz*/
    public function s_group()
    {
        if ($this->input->post('submit'))
        {

            $this->form_validation->set_rules('group_name', 'group_name', 'required');
            $this->form_validation->set_rules('group_noofpersonfrom', 'group_noofpersonfrom', 'required');
            $this->form_validation->set_rules('group_noofpersonto', 'group_noofpersonto', 'required');

            if ($this->form_validation->run() != FALSE)
            {

                $from=$this->input->post('group_noofpersonfrom');
                $to=$this->input->post('group_noofpersonto');

                if($from >= $to)//condition from >to
                {
                    $this->session->set_flashdata('message', 'Number Of Person From Greater Than Number Of Person To');
                    redirect(base_url() . 'admin/Groups/s_group');
                }
                else
                {
                    $parameter1 = array('act_mode' => 's_addgroupadmin_from_check',
                        'Param1' => '',
                        'Param2' => $from,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter11);
                    $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter1);

                    if($response->c > 0)//to check from alredy exits
                    {
                        $this->session->set_flashdata('message', 'Number Of Person From Already Exits');
                        redirect(base_url() . 'admin/Groups/s_group');

                    }


                        $parameter11 = array('act_mode' => 's_addgroupadmin_to_check',
                            'Param1' => '',
                            'Param2' => $to,
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter11);
                        $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter11);


                        if ($response->c > 0)  //to check to alredy exits
                        {
                            $this->session->set_flashdata('message', 'Number Of Person To Already Exits');
                            redirect(base_url() . 'admin/Groups/s_group');

                        }


                       //inserted
                            $parameter12 = array('act_mode' => 's_addgroup',
                                'Param1' => $this->input->post('group_name'),
                                'Param2' => $from,
                                'Param3' => $to,
                                'Param4' => $this->input->post('packages'),
                                'Param5' => '',
                                'Param6' => '',
                                'Param7' => '',
                                'Param8' => '',
                                'Param9' => '');
                            //pend($parameter11);
                            $response = $this->supper_admin->call_procedure('proc_groupbooking', $parameter12);
                            $this->session->set_flashdata('message', 'Inserted Sucessfully');
                            redirect(base_url() . 'admin/Groups/s_group');

                }

            }

        }
        

        $parameter2 = array('act_mode' => 's_groupbookingbackendselect',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewgroups'] = $this->supper_admin->call_procedure('proc_groupbooking', $parameter2);
     //  pend($response['s_viewgroups']);
        $parameter4 = array('act_mode' => 's_viewpackageadmin',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('groups/addgroups', $response);

    }

    public function checkgroupfromedit()
    {

        $parameter10 = array(
            'act_mode' => 's_editgroup_from_range',
            'Param1' => $this->input->post('group_id'),
            'Param2' => $this->input->post('from'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
       //pend($parameter10);
        $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter10);

        if($response->c >0)
        {
            echo 1;exit;
        }
        else
        {

            echo 0;exit;

        }

    }

    public function checkgrouptoedit()
    {
        $parameter10 = array(
            'act_mode' => 's_editgroup_to_range',
            'Param1' => $this->input->post('group_id'),
            'Param2' => $this->input->post('to'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter10);

        if ($response->c > 0)
        {
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }


    }




    public function checkgroupfrom()
    {
        $parameter10 = array(
            'act_mode' => 's_addgroupadmin_from_check',
            'Param1' => '',
            'Param2' => $this->input->post('from'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        // pend($parameter10);
        $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter10);

        if ($response->c > 0)
        {
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }


    }


    public function checkgroupto()
    {
        $parameter10 = array(
            'act_mode' => 's_addgroupadmin_to_check',
            'Param1' => '',
            'Param2' => $this->input->post('to'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        // pend($parameter10);
        $response = $this->supper_admin->call_procedureRow('proc_groupbooking', $parameter10);

        if ($response->c > 0)
        {
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }


    }




    /*Delete package By zzz*/
    public function GroupsDelete1()
    {
        $parameter = array('act_mode' => 'delete_groups',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_groupbooking', $parameter);
        redirect(base_url() . 'admin/Groups/s_group');

    }

    public function PrintpaymentDelete1()
    {
        $parameter = array('act_mode' => 'delete_printpage',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_printpayment_s', $parameter);
        redirect(base_url() . 'admin/Taxes/s_printpage');

    }



    /*DELETE BRANCH BY zzz*/
    public function branchdelete1()
    {
        $parameter = array('act_mode' => 'delete_branch',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter);
        redirect(base_url() . 'admin/branchs/addbranch');

    }

    /*UPDATE BRANCH BY zzz*/
    public function updatebranch()
    {
        echo $this->input->post('update_branch_id');
        echo $this->input->post('locationid_update');
        echo $this->input->post('companyid_update');
        echo $this->input->post('branch_name_update');

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('locationid_update', 'locationid', 'required');
            $this->form_validation->set_rules('companyid_update', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name_update', 'branch_name', 'required');
            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_updatebranch',
                    'Param1' => $this->input->post('locationid_update'),
                    'Param2' => $this->input->post('companyid_update'),
                    'Param3' => $this->input->post('branch_name_update'),
                    'Param4' => $this->input->post('update_branch_id'),
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                $this->session->set_flashdata('message', 'updated sucessfully');
                redirect(base_url() . 'admin/branchs/addbranch');

            }
            else {
                $this->session->set_flashdata('message', 'Not updated please try again');
                redirect(base_url() . 'admin/branchs/addbranch');
            }
        }
    }

     public  function branchstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 'branchstatus',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter1);
        redirect(base_url() . 'admin/branchs/addbranch');

    }

//footer data
    public function s_footerpage()
    {


        $parameter2 = array('act_mode' => 's_viewfooter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewfooter'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter2);
        //pend($response['s_viewbranch']);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('footer/addfooter', $response);

    }


    public function footerstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter4 = array('act_mode' => 's_statusaddon_admin',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter4);
        $response = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter4);
        redirect("admin/Taxes/s_footerpage");
    }

}// end class
?>
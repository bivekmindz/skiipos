<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reportingcontrolticketmonthlysalereport extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
    }

    /*listing filter */
    public function order_sess()
    {

        if ($_POST['Clear'] == 'Show All Records') {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ", $this->input->post('filter_date_session'));
//p($a);
//p($b);

        if ($b['1'] == 'PM') {
            $session_time_param = $a['0'] + 12;
        } else {
            $session_time_param = $a['0'];
        }
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);
        $array = array('branchids' => $branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' => $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        $this->session->set_userdata('order_filter', $array);
        redirect('admin/reportingcontrolticketmonthlysalereport/order');


    }


    /*view all orders*/
    public function order()
    {
//Date Select
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
            $response['dateval1']=  $this->session->userdata('datedataval');
            $daten=explode("-",$this->session->userdata('datedataval'));
            $datedata2=$daten[2].'-'.$daten[1].'-'.$daten[0];
            $dateval2= $datedata2;
        }
        else

        {
            $response['dateval1']=date("Y-m-d");
            $dateval1=date("Y-m-d");
            $dateval2=date("d-m-Y");
        }
        //p($_POST);

        //$this->session->unset_userdata('order_filter');


        if ($this->session->userdata('order_filter')) {
            //p($this->session->userdata('order_filter'));
            $parameter1 = array('act_mode' => 'S_vieworderticketmonthlysalereport',
                'Param1' => $dateval1,
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' =>'',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                'Param11' => '',
                'Param12' => '',
                'Param13' => '',
                'Param14' => '',
                'Param15' => '',
                'Param16' =>'',
                'Param17' => '',
                'Param18' => '',
                'Param19' => '');
            foreach ($parameter1 as $key => $val) {
                if ($parameter1[$key] == '') {
                    $parameter1[$key] = -1;
                }
                //  echo $parameter1[$key];
            }

            $response['vieww_ordermonthly'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);

            $response['vieww_dateval13']=$this->session->userdata('order_filter')['filter_date_session_from']. "-" .$this->session->userdata('order_filter')['filter_date_session_to'] ;


            $this->session->unset_userdata('order_filter');
            $response['vieww_dateval1']=$dateval1;

        } else {

            $dateval1data=explode("/",$dateval1);

            $datenew=$dateval1data[1]."/".$dateval1data[0]."/".$dateval1data[2];
            $parameter1 = array('act_mode' => 'S_viewordermonthlycounter',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_ordermonthly'] = $this->supper_admin->call_procedure('proc_reportingorder_s', $parameter1);


if($this->session->userdata('order_filter')['filter_date_ticket']!='')
{
    $response['vieww_dateval13']=$this->session->userdata('order_filter')['filter_date_ticket'];

}
else{
    $response['vieww_dateval13']=$dateval1;

}



        }


        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/ticketmonthlysalereport', $response);
    }


}// end class
?>
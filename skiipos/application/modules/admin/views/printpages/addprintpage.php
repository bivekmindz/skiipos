<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_taxes").validate({
            ignore: ".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                tax_name: "required",

                tax_percentage: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                $("#branchids").attr('disabled', false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    .btne_active {
        text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2> Print Pages </h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li>
                                                <a href="<?php echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a>
                                            </li>
                                            <li><a href="<?php echo base_url('admin/Taxes/s_tax') ?>">Taxes</a></li>
                                            <li>
                                                <a href="<?php echo base_url('admin/packages/addPackage') ?>">Package</a>
                                            </li>
                                            <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                            </li>
                                            <li><a href="<?php echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily
                                                    Timeslot</a></li>
                                            <li><a href="<?php echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                            <li><a href="<?php echo base_url('admin/companys/s_addCompany') ?>">Add
                                                    Company</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                    Payment</a></li>
                                               <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer Data
                                                    Payment</a></li>
                                            <li class="active"><a
                                                        href="<?php echo base_url('admin/Prints/s_printpage') ?>">Print Pages
                                                    </a></li>
                                        </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if ($this->session->flashdata('message')) {
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                              <div class="col-md-12">
                            <div class="add_comp">

                                <div class="table-responsive" style="width: 100%;">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th> Name</th>

                                             <th>EDIT/DELETE</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        $i = 1;
                                        foreach ($s_viewprintpayment as $v) {
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td id="u_pack_name"><?php echo $v->printsetting_name; ?></td>
                                                  <td>

                                                    <a class="btn btn-primary"
                                                       href="<?= base_url('admin/Prints/Printpageupdate/') . "/" . $v->printsetting_id ?>">
                                                        Edit <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>



                                                </td>

                                            </tr>

                                            <?php $i++;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    .input-group-addon {
        position: absolute;
        left: 0;
        width: 100%!important;
        padding: 12px 12px!important;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: transparent!important;
        border: none!important;
        border-radius: 4px;
        top: 0px;
    }
    .input-group-addon  .glyphicon-time{
        float:right;
    }
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    /*.tab_ble{ width:100%; float:left;}
    .tab_ble table{ width:100%; float:left;}
    .tab_ble table tr{ width:100%; float:left;}*/
    .tab_ble table tr th {
        /*padding: 5px 35px 15px 35px;*/
        text-align: center;
    }

    .tab_ble table tr td {
        /*border: 1px solid #CCC;*/
        text-align: center;

        /*min-width: 100px;*/
    }
    .lbl_box{
        float:left;
        margin-right:10px;
    }

    @media screen and (min-width: 320px) and (max-width: 680px){

        .lbl_box{ width: 100%;
            float:left;
            margin-right:10px;
        }

    }
    .checkbox_lbl{
        display: none;
        z-index: -9999;
    }
    .checkbox_lbl + label{
        position: relative;
        padding-left:25px;
        font-weight:normal;
        cursor: pointer;
    }
    .checkbox_lbl + label:before{
        position: absolute;
        content: "";
        width:19px;
        height:19px;
        border:solid 1px #abaaaa;
        left:0;
    }
    .checkbox_lbl + label:after{
        position: absolute;
        content: "";
        width: 10px;
        height: 6px;
        border-left: solid 2px #444;
        border-bottom: solid 2px #444;
        left: 5px;
        transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        transition: 0.3s;
        -o-transition: 0.3s;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        top: 5px;
        visibility: hidden;
        opacity: 0;
    }
    .checkbox_lbl:checked + label:after{
        visibility: visible;
        opacity: 1;
    }
    .page_contant{
        margin-top:0px;
    }
    .page_box{
        box-shadow: none;
    }
    /* .tbl_overflow{
         float:left;
         width:100%;
         overflow-x: auto;
         padding-bottom: 10px;
     }*/
    #advance_search_d{
        display: none;
        margin-bottom:15px;
        float:left;
        width:100%;
    }

    .advance_btn{
        float:left;
        background: #0f71c3;
        color:#fff;
        padding:10px 15px;
        cursor: pointer;
    }
    .table > thead > tr > th {
        vertical-align: middle!important;

    }

    #container{width:100%;}
    #left{float:left}
    #right{float:right}

    #center{margin:0 auto;width:100px;float: left;padding: 1px 17px;}
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">





                    <div class="page_name">

                        <h2>Order View</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!-- <div class='flashmsg'>
                    <?php echo validation_errors(); ?>
                    <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                </div> -->
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="tab_ble">

                                <table width="100%" id="table1" style="width: 100%"
                                       class="table table-bordered table-striped">
                                    <tr>
                                        <td>Ticket Id</td>
                                        <td><?php echo $vieww_orderdetailnew->order_uniqueid; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Total</td>
                                        <td><?php echo $vieww_orderdetailnew->order_total; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Payment Mode</td>
                                        <td><?php echo $vieww_orderdetailnew->order_statusdetail; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $vieww_orderdetailnew->order_tax_name1; ?></td>
                                        <td><?php echo $vieww_orderdetailnew->order_taxvalue; ?>%</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $vieww_orderdetailnew->order_tax_name2; ?></td>
                                        <td><?php echo $vieww_orderdetailnew->order_tax_percentage2; ?>%</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $vieww_orderdetailnew->order_tax_name3; ?></td>
                                        <td><?php echo $vieww_orderdetailnew->order_tax_percentage3; ?>%</td>
                                    </tr>


                                </table>

                                <div class="tbl_overflow">


                                    <table id="table1" style="width: 100%" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Package Name</th>
                                            <th>Package Price</th>
                                            <th>Package Seats</th>
                                            <th>Visit Date - Time</th>

                                              </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1;
                                        //p($vieww_order[0]);
                                        foreach ($vieww_orderdetail as $v) {



                                            if($this->session->userdata('snowworld')->EmployeeId) {
                                                if($this->session->userdata('snowworld')->branchid != $v->branch_id)
                                                {
                                                    continue;
                                                }
                                            }?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?php echo $v->orderproduct_package_name; ?></td>
                                                <td>Rs <?php echo $v->orderproduct_package_price; ?></td>
                                                <td><?php if ($v->orderproduct_inventory_seats != '') {
                                                        echo $v->orderproduct_inventory_seats;
                                                    } else {
                                                        echo "---";
                                                    } ?></td>



                                                <td><?php if ($v->orderproduct_inventory_from != '') {
                                                        echo $v->departuredate . "<br>" . $v->orderproduct_inventory_from . ":" . $v->orderproduct_inventory_minfrom . $v->txtfromd . " TO " . $v->orderproduct_inventory_to . ":" . $v->orderproduct_inventory_minto . $v->txttod;
                                                    } else {
                                                        echo "---";
                                                    } ?></td>



                                            </tr>

                                            <?php $i++;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function funct_change_status(a,b) {
        if(b == 2) {
            $(".ticket" + a).attr('disabled', true);
        }
        $.ajax({
            url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
            type: 'POST',
            data: {'pacorderid': a,'op_ticket_print_status':b},
            dataType: "JSON",
            success: function(data){


//console.log(data.s[0].ticket_status);
                if(data.s[0].ticket_status == 2)
                {
                    a.attr('disabled',true);

                }
//console.log(data1);
            }
        });
//var b = $(this).val();
// alert(b);return false;

    }

    /*    $(function () {
    $("#op_ticket_print_status").change(function () {
    alert("hello"); return false;
    var a = $(this);
    if($("#op_ticket_print_status").val() == 2)
    {
    a.attr('disabled',true);

    }
    //return false;
    $.ajax({
    url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
    type: 'POST',
    data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
    dataType: "JSON",
    success: function(data){


    //console.log(data.s[0].ticket_status);
        if(data.s[0].ticket_status == 2)
        {
            a.attr('disabled',true);

        };
       //console.log(data1);
    }
    });
    });
    });*/

    function ticket_print(a) {
//alert(a);
        window.print();

    }

    function update_com(a, b) {
//alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display', 'block');
        $("#add_com").css('display', 'none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display', 'block');
        $("#update_com").css('display', 'none');
        return false;
    }


</script>



<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        $(".advance_btn").click(function(){
            $("#advance_search_d").slideToggle();
        });

    })
</script>

<script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet"

      href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript"

src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
                                                                      <script type="text/javascript"

src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

                                                                            <script>
                                                                            $(document).ready(function(){


    var table = $('#table1').DataTable({"scrollY": 400,"scrollX": true });
    $.noConflict();
    var date_input=$('input[name="filter_date_booking_from"],input[name="filter_date_booking_to"],input[name="filter_date_printed_from"],input[name="filter_date_printed_to"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
    format: 'yyyy-mm-dd',
    container: container,
    todayHighlight: true,
    autoclose: true,
});


var date_input=$('input[name="filter_date_session_from"],input[name="filter_date_session_to"],input[name="filter_date_ticket"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
    format: 'dd/mm/yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
});
/*$('#filter_date1').datepicker('setDate', 'today');
$('#filter_date2').datepicker('setDate', 'today');
$('#filter_date3').datepicker('setDate', 'today');
$('#filter_date4').datepicker('setDate', 'today');*/
});
//$('#table1').dataTable();
</script>






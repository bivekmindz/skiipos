<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_taxes").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                tax_name: "required",

                tax_percentage: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                $("#branchids").attr('disabled',false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .btne_active{    text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;}
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Edit Groups</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li class="active"><a href="<?php  echo base_url('admin/Groups/s_group') ?>" >Groups</a></li>

                                        </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form action="" name="add_taxes" id="add_taxes" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">

                                            <div class="tbl_input">
                                                <input type="text" id="group_name" name="group_name"  value="<?php echo $s_viewgroups->groupname; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box" >
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> No Of Person From </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="group_noofpersonfrom" name="group_noofpersonfrom" onkeyup="checkfrom(<?php echo $s_viewgroups->group_id; ?>);"   value="<?php echo $s_viewgroups->groupfrom; ?>"  >
                                                <label for="" class="from_error"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box" >
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> No Of Person To</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="group_noofpersonto" name="group_noofpersonto" onkeyup="checkto(<?php echo $s_viewgroups->group_id;  ?>);" onkeypress="return isNumberKey(event)" value="<?php echo $s_viewgroups->groupto; ?>">
                                                <label for="" class="to_error"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Packages</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">

                                                <select name="packages">
                                                    <?php
                                                    foreach($vieww_pack as $v) {
                                                        ?>

                                                        <option value="<?php echo $v->package_id ?>" <?php if($v->package_id==$s_viewgroups->grouppackageid) { ?> selected <?php } ?>><?php echo $v->package_name ?></option>
                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>








<input type="hidden" name="group_id" value="<?php echo $s_viewgroups->group_id; ?>">

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                                <a href="<?php echo base_url(); ?>admin/Groups/s_group" class="btn_button sub_btn backbtn" style="margin-left: 10px">Back</a>


<!--                                                <input id="submitBtn" type="submit" name="submit" value="Back" class="btn_button sub_btn" onclick="window.location=" />-->
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>



                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function checkfrom(value)
    {

        var f=$('#group_noofpersonfrom').val();
        $.ajax({
            url : '<?php echo base_url();  ?>admin/Groups/checkgroupfromedit',
            type :'POST',
            data : { 'from' : f,'group_id':value},
            success : function(data)
            {
               //alert(data);
                if(data==1)
                {
                    $('#group_noofpersonfrom').focus();
                    $('#group_noofpersonfrom').innerHTML='';
                    $(".from_error").html("Number Of Person From Already Exits.").css({ 'color': 'red' });
                    return false;
                }
                else
                {
                    $(".from_error").html("");
                }

            }

        });

    }


    function checkto(value)
    {
        var t=$('#group_noofpersonto').val();

        $.ajax({
            url : '<?php echo base_url();  ?>admin/Groups/checkgrouptoedit',
            type :'POST',
            data : { 'to':t,'group_id':value },
            success : function(data)
            {
                //alert(data);
                if(data==1)
                {
                    $('#group_noofpersonto').focus();
                    $('#group_noofpersonto').html='';
                    $(".to_error").html("Number Of Person To Already Exits.").css({ 'color': 'red' });
                    return false;
                }
                else
                {
                    $(".to_error").html(" ");
                }



            }

        });

    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


</script>




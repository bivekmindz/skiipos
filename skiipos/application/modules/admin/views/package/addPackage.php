<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_package").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                package_name: "required",
                package_desc: "required",
                branchids: "required",
                act_ids: "required",
                package_price: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                $("#branchids").attr('disabled',false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .btne_active{    text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;}
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add Package</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li ><a href="<?php  echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a></li>

                                            <li ><a href="<?php  echo base_url('admin/Taxes/s_tax') ?>" >Taxes</a></li>
                                            <li class="active"><a href="<?php echo base_url('admin/packages/addPackage') ?>" >Package</a></li>
                                            <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                            </li>

                                            <li ><a href="<?php  echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily Timeslot</a></li>
                                            <li ><a href="<?php  echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                            <li ><a href="<?php  echo base_url('admin/companys/s_addCompany') ?>">Add Company</a></li>

                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                    Payment</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer Data
                                                    Payment</a></li>
                                            <li><a href="<?php echo base_url('admin/Prints/s_printpage') ?>">Printpage
                                                </a></li>
                                            <li ><a  href="<?php echo base_url('admin/timeslotspackages/addTimeslotPackage') ?>">Manual Time slot</a></li>


                                        </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form action="<?= base_url('admin/packages/addPackage') ?>" name="add_package" id="add_package" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="package_name" name="package_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="package_desc" name="package_desc" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Tax <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">





                                                <select name="branchids" required id="branchids">

                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->tax_id; ?>"><?php echo $value->tax_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="package_price" name="package_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Color<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="color" id="package_color" name="package_color" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Text Color<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="color" id="package_textcolor" name="package_textcolor" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Shadow Color<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="color" id="package_shadowcolor" name="package_shadowcolor" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Order Number<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="package_orderno" name="package_orderno" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box" style="display:none">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Type<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                               <select name="type" >
                                                   <option value="packages" selected>Packages</option>
                                                   <option value="addons">Addons</option>

                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th> Name</th>
                                        <th> Description</th>

                                        <th> Price</th>
                                        <th> Type</th>
                                        <th> Status</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww_pack as $v) {?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="u_pack_name"><?php echo $v->package_name; ?></td>
                                            <td id="u_pack_desc"><?php echo $v->package_description; ?></td>

                                            <td id="t_act_price"><?php echo $v->package_price; ?></td>
                                            <td id="t_act_type"><?php echo $v->package_type; ?></td>
                                            <td id="t_act_type"><a
                                                        href="<?= base_url('admin/packages/packagesstatus/' . base64_encode($v->package_id) . '/' . base64_encode($v->package_status) . '') ?>"
                                                        class="btn btn-primary"><?php echo $v->package_status == 0 ? "Inactive" : "Active"; ?></a>
                                            </td>
                                            <td>

                                                <a class="btn btn-primary" href="<?= base_url('admin/packages/packageupdate/')."/".$v->package_id ?>" >Edit
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/packages/packageDelete1/')."/".$v->package_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Package</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('admin/packages/updatePackage') ?>" name="update_package" id="update_package" method="post" enctype="multipart/form-data" >


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Name<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_package_name" name="update_package_name" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Description<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <textarea  id="update_package_desc" name="update_package_desc" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <?php
                                        //echo "<pre>";
                                        //print_r($s_viewbranch);

                                        ?>
                                        <div class="tbl_input">
                                            <select name="update_branchids[]" multiple id="update_branchids">
                                                <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                    <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box" >
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Package For<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">

                                            <select name="update_package_for" id="update_package_for">
                                                <option value="0">Users/Guests</option>
                                                <option value="1">Partners</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Price<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_package_price" name="update_package_price" >
                                            <input type="hidden" id="pack_update_hidden_id" name="pack_update_hidden_id">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>

</div>

<script>

    function func_update(dataid) {
        $('#update_branchids option').attr('selected',false);
        //alert(dataid);
        $.ajax({
            url: '<?php echo base_url()?>admin/packages/update_package_data',
            type: 'POST',
            dataType : "json",
            data: {'pack_id':dataid},
            success: function(data){
                // alert(data['0'].package_name + data['0'].package_id);
                $("#update_package_name").val(data['0'].package_name);
                $("#update_package_desc").val(data['0'].package_description);
                $("#update_package_price").val(data['0'].package_price);
                $("#pack_update_hidden_id").val(data['0'].package_id);
                $('#update_package_for option["value="'+ data['0'].package_for +"']').attr('selected',true);
                $('.selDiv [value="SEL1"]');
                var mystr = data['0'].bp_id;
                var myarr = mystr.split(",");
                for(var i = 0 ; i < myarr.length; i++)
                {
                    //alert(myarr[i]);

                    $('#update_branchids option[value="'+myarr[i]+'"]').attr('selected',true);
                }
                $("#update_branchids").select2({
                    placeholder: "Select the Data"
                });
                //$("#update_package_name").val(data['0'].package_name);
                return false;
            }
        });


        //console.log(act_split.length);
        return false;

    }

    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data"
        });
    });



</script>
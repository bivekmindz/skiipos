<style> 
/*input.error {
     border: 1px solid red;
}
 label.error {
     border: 0px solid red;
     color: red;
     font-weight: normal;
     display: inline;
}
 .btne_active {
     text-decoration: none;
     background-color: #c7921b;
     padding: 6px 5px 9px 6px;
     color: #fff;
     border-radius: 5px;
}
*/
 .sep_box-listing {
     float: left;
     width: 100%;
     padding: 20px 0px;
}
 .sep_box-listing ul {
     padding: 0px;
     list-style-type: none;
}
 .sep_box-listing ul li{
     display: inline-block;
     width: 20%;
     margin: 7px;
     box-shadow: 3px 5px 14px #8c8c8c;
}
 .form-group lable{
    margin-bottom: 10px;
     display: block;
     font-weight:600;
}
 .form-group input[type=text]{
     width: 100%;
     height:33px;
     padding: 5px;
     font-size: 14px;
     color: #393939;
}
 .form-group input[type=text]:focus{
     outline: none;
}
 .form-group input[type=submit]{
    background: #2196F3;
    border: none;
    padding:7px 23px;
    color: #fff;
    font-size: 16px;
     text-transform: capitalize;
    margin:27px 0 0 0;
}
 .sep_box-listing ul li .file-data {
    border:2px solid #a9a9a9;
    padding: 15px 0;
    box-sizing: border-box;
    letter-spacing: 1px;
}
 .sep_box-listing ul li .file-data a{
    font-weight: 800;
    font-size:22px;
     color: #8c8c8c;
     text-decoration: none;
}
 .sep_box-listing ul li .file-data a .fa{
    display: block;
    font-size: 50px;
    margin: 0 0 20px 0;
}
 .sep_box-listing ul li .file-data.active, .sep_box-listing ul li .file-data:hover{
    border:2px solid #78bdff;
     background: #499bea;
    /* Old browsers */
     background: -moz-linear-gradient(top, #499bea 0%, #207ce5 100%);
    /* FF3.6-15 */
     background: -webkit-linear-gradient(top, #499bea 0%,#207ce5 100%);
    /* Chrome10-25,Safari5.1-6 */
     background: linear-gradient(to bottom, #499bea 0%,#207ce5 100%);
    color: #fff;
}
 .sep_box-listing ul li .file-data.active a, .sep_box-listing ul li .file-data:hover a{
     color: #fff;
}
 </style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="wrapper">
   <div class="col-lg-10 col-lg-push-2">
      <div class="row">
         <div class="page_contant">
            <div class="col-lg-12">
               <div class="page_name">
                  <h2> Dlf Files Download </h2>
               </div>
               <div class="page_box">
               <form method="post" action="<?php echo base_url().'admin/reportingusermi/miuserdata' ?>">
                      <div class="sep_box">
                     <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                           <lable>Start Date</lable>
                           <input type="text" autocomplete="off"  placeholder="Start Date" required name="startdate" id="startdate">
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                           <lable>End Date</lable>
                           <input type="text" autocomplete="off" placeholder="End Date" required name="enddate" id="enddate">
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-5 col-xs-12">
                        <div class="form-group">
                           <input type="submit" name="submit" value="Search" id="get-result">
                        </div>
                     </div>
                  </div> 
                 </form>
                  <div class="sep_box-listing">
                     <ul>
                       <?php 
                       $count = count($getfiledetail);
                       if($count > 0){
                       foreach($getfiledetail as $value){  ?>
                      <li>
                      <center style="background-color: #8c8c8c"><a style="text-decoration:none;background-color: #8c8c8c" href ="<?php echo base_url()?>admin/reportingusermi/downloadFilename?q=<?php echo $value->filename ?>"><span>Download</span> </a></center><span>Receipt Number : </span><span><?php echo $value->receipt_number ?></span> | <span>Login Time : </span><span><?php echo $value->login_time ?></span>
                        <div class="file-data text-center">
                          <a href= "javascript:void(0)"><span><i class="fa fa-file-text-o" aria-hidden="true"></i><?php echo $value->user_firstname.' '.$value->remote_address.' '.date('jS F Y',strtotime($value->created_on)) ?></span></a>
                        </div>
                       <center style="background-color: #8c8c8c"> <a href="javascript:void(0)" onclick="deleteFile('<?php echo $value->t_id ?>')" style="text-decoration:none;background-color: #8c8c8c"> <span>Delete</span> </a></center>
                      </li>
                         
                      <?php } }else{
                       echo '<b style="padding-left:285px;">No Result Found</b>';
                        } ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#startdate,#enddate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );


  function deleteFile(id){
   var ids = id;
   var x = confirm("Are you sure you want to delete?");
   if (x){
   $.ajax({
   url: '<?php echo base_url()?>admin/reportingusermi/deleteFile',
   type: "POST",
    data: {'id':ids},
   success: function(data){
    location.reload();
   }
   });
   }else{  
     return false;
   
  }
 }
  

  </script>


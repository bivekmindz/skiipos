<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Ski India</title>


    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?= base_url('assets/login') ?>/css/style.css">

<!-- Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          HREF="<?php echo base_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" HREF="<?php echo base_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" HREF="<?php echo base_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" HREF="<?php echo base_url(); ?>assets/favicon/favicon-16x16.png">
</head>

<body>
<div class="wrapper">


       <!-- <div class="banner_pic"><img src="<?= base_url() ?>/assets/admin/images/background.jpg"></div> -->




    <form method="post" action="" class="form-signin">
        <div class="ad_login" style="margin-top:30%;">
        <h2 class="form-signin-heading"><strong>Admin</strong></h2>
        <div><?php echo validation_errors();  if(isset($result) && $result!=''){echo ($result);}?></div>
            <div class="form-group">
                <label>Email Address</label>
                <input type="text" class="form-control" name="email" placeholder="Email Address" required autofocus /></div>

            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required/></div>

        <label style="margin-left:30px;" class="checkbox">
            <input type="checkbox" name="checkbox" checked="">Remember Me
        </label>
        <input class="btn btn-lg btn-primary btn-block" name="submit" value="Login" type="submit">
        </div>
    </form>
</div>


</body>
</html>

<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo admin_url(); ?>js/moment.js"></script>

<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
      rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $(function () {
    });
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    .input-group-addon {
        position: absolute;
        left: 0;
        width: 100% !important;
        padding: 12px 12px !important;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: transparent !important;
        border: none !important;
        border-radius: 4px;
        top: 0px;
    }

    .input-group-addon .glyphicon-time {
        float: right;
    }

    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    /*.tab_ble{ width:100%; float:left;}
    .tab_ble table{ width:100%; float:left;}
    .tab_ble table tr{ width:100%; float:left;}*/
    .tab_ble table tr th {
        /*padding: 5px 35px 15px 35px;*/
        text-align: center;
    }

    .tab_ble table tr td {
        /*border: 1px solid #CCC;*/
        text-align: center;

        /*min-width: 100px;*/
    }

    .lbl_box {
        float: left;
        margin-right: 10px;
    }

    @media screen and (min-width: 320px) and (max-width: 680px) {

        .lbl_box {
            width: 100%;
            float: left;
            margin-right: 10px;
        }

    }

    .checkbox_lbl {
        display: none;
        z-index: -9999;
    }

    .checkbox_lbl + label {
        position: relative;
        padding-left: 25px;
        font-weight: normal;
        cursor: pointer;
    }

    .checkbox_lbl + label:before {
        position: absolute;
        content: "";
        width: 19px;
        height: 19px;
        border: solid 1px #abaaaa;
        left: 0;
    }

    .checkbox_lbl + label:after {
        position: absolute;
        content: "";
        width: 10px;
        height: 6px;
        border-left: solid 2px #444;
        border-bottom: solid 2px #444;
        left: 5px;
        transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        transition: 0.3s;
        -o-transition: 0.3s;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        top: 5px;
        visibility: hidden;
        opacity: 0;
    }

    .checkbox_lbl:checked + label:after {
        visibility: visible;
        opacity: 1;
    }

    .page_contant {
        margin-top: 0px;
    }

    .page_box {
        box-shadow: none;
    }

    /* .tbl_overflow{
         float:left;
         width:100%;
         overflow-x: auto;
         padding-bottom: 10px;
     }*/
    #advance_search_d {
        display: none;
        margin-bottom: 15px;
        float: left;
        width: 100%;
    }

    .advance_btn {
        float: left;
        background: #0f71c3;
        color: #fff;
        padding: 10px 15px;
        cursor: pointer;
    }

    .table > thead > tr > th {
        vertical-align: middle !important;

    }

    #container {
        width: 100%;
    }

    #left {
        float: left
    }

    #right {
        float: right
    }

    #center {
        margin: 0 auto;
        width: 100px;
        float: left;
        padding: 1px 17px;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">


                    <div class="page_name">

                        <h2>Ticket Sales Monthly Report</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">


                                <div id="container">
                                    <div id="left">
                                        <div class="advance_btn">Advance Search</div>
                                    </div>

                                    <div id="center">
                                        <form method="post"
                                              action="<?= base_url('admin/reportingcontrolticketmonthlysalereport/order') ?>">


                                            <div class="submit_tbl">
                                                <input type="submit" value="Show All Records" class="btn_button sub_btn"
                                                       name="Clear"></div>
                                        </form>

                                    </div>

                                    <div id="right">


                                        <input type="button" id="create_pdf" value="Generate PDF" class="btn btn-primary">


                                        <button id="btnExport" class="btn btn-primary">Download EXCEL</button>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!-- <div class='flashmsg'>
                    <?php echo validation_errors(); ?>
                    <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                </div> -->
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="tab_ble">


                                <div id="advance_search_d">
                                    <form method="post"
                                          action="<?= base_url('admin/reportingcontrolticketmonthlysalereport/order_sess') ?>">




                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date3"  placeholder="SESSION DATE FROM" name="filter_date_session_from" required>
                                                </div></div>
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date4"  placeholder="SESSION DATE TO" name="filter_date_session_to" required>
                                                </div></div>
                                        </div>


                                        <div class="sep_box">
                                            <div class="col-md-12">
                                                <div class="submit_tbl">
                                                    <input type="submit" value="Search" class="btn_button sub_btn">
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <style>
                                    .tab_down {
                                        width: 100%;
                                        float: left;
                                        padding: 40px 0px;
                                    }

                                    .tab_down table {
                                        width: 100%;
                                        float: left;
                                        line-height: 37px;
                                    }

                                    .tab_down table tr { /*border: 1px solid #d2d1d1;*/
                                    }

                                    .tab_down table td { /*border: 1px solid #d2d1d1;*/
                                    }

                                    .tab_down table td span {
                                        font-size: 25px;
                                    }
                                    .tav-one {
                                        width: 100%;
                                        float: left;
                                        margin-bottom: 50px;
                                    }

                                    .tav-one table {
                                        width: 100%;
                                        float: left;
                                        padding: 30px 0px;
                                    }

                                    .tav-one table tr {
                                        border: 1px solid #d2d1d1;
                                    }

                                    .tav-one table th {
                                        border: 1px solid #d2d1d1;
                                        font-size: 15px;
                                        color: #000;
                                        line-height: 40px;
                                    }

                                    .tav-one table td {
                                        line-height: 25px;
                                        font-size: 12px;
                                        border: 1px solid #d2d1d1;
                                        color: #757575;
                                    }

                                    .tav-one p {
                                        padding: 15px 13px;
                                        background-color: #D8D8D8;
                                    }

                                    .tav-one .hedi {
                                        font-size: 15px;
                                        text-align: left;
                                        color: #000;
                                    }


                                </style>

                                <form class="form" >
                                    <div id="table_Excel">
                                        <div >

                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="content">
                                                <tbody><tr>
                                                    <td height="63" align="center" style="font-size:24px;">SKI INDIA - Ticket Sales Report</td>
                                                </tr>
                                                <tr>
                                                    <td height="43" align="center" style="font-size:18px;">Brand of Chiliad Procons</td>
                                                </tr>
                                                <tr>
                                                    <td height="43" style="font-size:18px;">&nbsp;</td>
                                                </tr>


                                                <tr>

                                                </tr>

                                                <tr>

                                                    <td>
                                                        <table width="100%" border="0" style="border: 2px solid #D8D8D8;" cellspacing="0" cellpadding="5">
                                                            <tr>
                                                                <td height="40" bgcolor="#d2d1d1" colspan="6"  style=" margin-left:5px; text-align:left !important; font-weight:600;" scope="col">&nbsp;&nbsp;&nbsp;Monthly Sales Report: <?php echo $vieww_dateval13; ?></td>

                                                            </tr>
                                                            <tr>
                                                                <th height="30" style="border: 1px solid #d2d1d1; text-align: center; font-weight: 600;">Booking Date</th>
                                                                <th  height="30" style="border: 1px solid #d2d1d1; text-align:center; font-weight: 600;">No. of Tickets</th>
                                                                <th  height="30" style="border: 1px solid #d2d1d1; text-align:center; font-weight: 600;">Gross Sale</th>
                                                                <th  height="30" style="border: 1px solid #d2d1d1; text-align:center; font-weight:600;">CGST</th>
                                                                <th  height="30" style="border: 1px solid #d2d1d1; text-align:center; font-weight: 600;">SGST</th>
                                                                <th  height="30" style="border: 1px solid #d2d1d1; text-align:center; font-weight: 600;">Entry Net Sale</th>

                                                            </tr>

                                                            <?php    $counterno=0;
                                                            $countergross=0;
                                                            $countercgst=0;
                                                            $countersgst=0;
                                                            $counternetsell=0;
                                                            foreach ($vieww_ordermonthly as $v) {
                                                                ?>

                                                                <tr>
                                                                    <td height="30" style="border: 2px solid #d2d1d1; font-weight: 100;" align="center"><?php echo $v->orderproduct_date; ?></td>
                                                                    <td height="30" style="border: 2px solid #d2d1d1; font-weight: 100;" align="center"><?php $counterno +=$v->prnum; echo $v->prnum; ?></td>
                                                                    <td height="30" style="border: 2px solid #d2d1d1; font-weight: 100;" align="center"><?php $countergross +=($v->offlineprice-$v->offlinepriceCGST-$v->offlinepriceSGST);
                                                                        echo number_format(($v->offlineprice-$v->offlinepriceCGST-$v->offlinepriceSGST),2); ?></td>
                                                                    <td height="30" style="border: 2px solid #d2d1d1; font-weight: 100;" align="center"><?php
                                                                        $countercgst+=$v->offlinepriceCGST;
                                                                        echo number_format($v->offlinepriceCGST,2); ?></td>
                                                                    <td height="30" style="border: 2px solid #d2d1d1; font-weight: 100;" align="center"><?php
                                                                        $countersgst +=$v->offlinepriceSGST;
                                                                        echo number_format($v->offlinepriceSGST,2); ?></td>
                                                                    <td height="30" style="border: 2px solid #d2d1d1; font-weight: 100;" align="center"><?php
                                                                        $counternetsell +=$v->offlineprice;
                                                                        echo number_format($v->offlineprice,2); ?></td>

                                                                </tr>
                                                            <?php } ?>
                                                            <tr>
                                                                <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: 600;">Total Booking</th>
                                                                <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: 600;"><?php echo $counterno; ?></th>
                                                                <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: 600;"><?php echo number_format($countergross,2); ?></th>
                                                                <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: 600;"><?php echo number_format($countercgst,2); ?></th>
                                                                <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: 600;"><?php echo number_format($countersgst,2); ?></th>
                                                                <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: 600;"><?php echo number_format($counternetsell,2); ?></th>
                                                            </tr>
                                                        </table>


                                                    </td>

                                                </tr>


                                                </tbody></table>









                                        </div>
                                    </div>  </div>
                            </form>  </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script>

    function funct_change_status(a,b) {
        if(b == 2) {
            $(".ticket" + a).attr('disabled', true);
        }
        $.ajax({
            url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
            type: 'POST',
            data: {'pacorderid': a,'op_ticket_print_status':b},
            dataType: "JSON",
            success: function(data){


//console.log(data.s[0].ticket_status);
                if(data.s[0].ticket_status == 2)
                {
                    a.attr('disabled',true);

                }
//console.log(data1);
            }
        });
//var b = $(this).val();
// alert(b);return false;

    }

    /*    $(function () {
    $("#op_ticket_print_status").change(function () {
    alert("hello"); return false;
    var a = $(this);
    if($("#op_ticket_print_status").val() == 2)
    {
    a.attr('disabled',true);

    }
    //return false;
    $.ajax({
    url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
    type: 'POST',
    data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
    dataType: "JSON",
    success: function(data){


    //console.log(data.s[0].ticket_status);
        if(data.s[0].ticket_status == 2)
        {
            a.attr('disabled',true);

        };
       //console.log(data1);
    }
    });
    });
    });*/

    function ticket_print(a) {
//alert(a);
        window.print();

    }

    function update_com(a, b) {
//alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display', 'block');
        $("#add_com").css('display', 'none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display', 'block');
        $("#update_com").css('display', 'none');
        return false;
    }


</script>



<script type="text/javascript" src="<?= base_url('assets/admin/js/jquery-1.11.3.min.js') ?>"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?= base_url('assets/admin/js/bootstrap-datepicker.min.js') ?>"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        $(".advance_btn").click(function(){
            $("#advance_search_d").slideToggle();
        });

    })
</script>

<script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo admin_url();?>js/jquery.min.js"></script>
<script type="text/javascript"

        src="<?php echo admin_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript"

        src="<?php echo admin_url();?>js/bootstrap.min.js"></script>

<script>
    $(document).ready(function(){


        var table = $('#table1').DataTable();
        $.noConflict();
        var date_input=$('input[name="filter_date_booking_from"],input[name="filter_date_booking_to"],input[name="filter_date_printed_from"],input[name="filter_date_printed_to"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        });


        var date_input=$('input[name="filter_date_session_from"],input[name="filter_date_session_to"],input[name="filter_date_ticket"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        });
        /*$('#filter_date1').datepicker('setDate', 'today');
        $('#filter_date2').datepicker('setDate', 'today');
        $('#filter_date3').datepicker('setDate', 'today');
        $('#filter_date4').datepicker('setDate', 'today');*/
    });
    //$('#table1').dataTable();
</script>







<script src="<?php echo admin_url();?>js/jspdf.min.js"></script>
<script>
    (function () {
        var
            form = $('.form'),
            cache_width = form.width(),
            a4 = [595.28, 841.89]; // for a4 size paper width and height

        $('#create_pdf').on('click', function () {
            $('body').scrollTop(0);
            createPDF();
        });
//create pdf
        function createPDF() {
            getCanvas().then(function (canvas) {
                var
                    img = canvas.toDataURL("image/png"),
                    doc = new jsPDF({
                        unit: 'px',
                        format: 'a4'
                    });
                doc.addImage(img, 'JPEG', 20, 20);
                doc.save('Ticket-Monthly-Sales-Report.pdf');
                form.width(cache_width);
            });
        }

// create canvas object
        function getCanvas() {
            form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
            return html2canvas(form, {
                imageTimeout: 2000,
                removeContainer: true
            });
        }

    }());
</script>
<script>
    /*
 * jQuery helper plugin for examples and tests
 */
    (function ($) {
        $.fn.html2canvas = function (options) {
            var date = new Date(),
                $message = null,
                timeoutTimer = false,
                timer = date.getTime();
            html2canvas.logging = options && options.logging;
            html2canvas.Preload(this[0], $.extend({
                complete: function (images) {
                    var queue = html2canvas.Parse(this[0], images, options),
                        $canvas = $(html2canvas.Renderer(queue, options)),
                        finishTime = new Date();

                    $canvas.css({ position: 'absolute', left: 0, top: 0 }).appendTo(document.body);
                    $canvas.siblings().toggle();

                    $(window).click(function () {
                        if (!$canvas.is(':visible')) {
                            $canvas.toggle().siblings().toggle();
                            throwMessage("Canvas Render visible");
                        } else {
                            $canvas.siblings().toggle();
                            $canvas.toggle();
                            throwMessage("Canvas Render hidden");
                        }
                    });
                    throwMessage('Screenshot created in ' + ((finishTime.getTime() - timer) / 1000) + " seconds<br />", 4000);
                }
            }, options));

            function throwMessage(msg, duration) {
                window.clearTimeout(timeoutTimer);
                timeoutTimer = window.setTimeout(function () {
                    $message.fadeOut(function () {
                        $message.remove();
                    });
                }, duration || 2000);
                if ($message)
                    $message.remove();
                $message = $('<div ></div>').html(msg).css({
                    margin: 0,
                    padding: 10,
                    background: "#000",
                    opacity: 0.7,
                    position: "fixed",
                    top: 10,
                    right: 10,
                    fontFamily: 'Tahoma',
                    color: '#fff',
                    fontSize: 12,
                    borderRadius: 12,
                    width: 'auto',
                    height: 'auto',
                    textAlign: 'center',
                    border:'2px solid #d2d1d1',
                    textDecoration: 'none'

                }).hide().fadeIn().appendTo('body');
            }
        };
    })(jQuery);

</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker31,#datetimepicker41').datetimepicker({
            format: 'LT'
        });

        $('.dpxx').datetimepicker({
            format: 'LT'
        });


    });

    $(document).ready(function() {
        $("#btnExport").click(function(e) {
            e.preventDefault();

//getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_Excel');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'Ticket-Monthly-Sales-Report_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
            a.click();
        });
        var x = $(window).height();
        var y = x-55;
        $(".page_contant").css({"height":y+"px","min-height":"auto"});
        var tp = $("#table1");
    });
</script>

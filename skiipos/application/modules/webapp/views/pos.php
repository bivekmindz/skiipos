<!DOCTYPE html>
<html class="loading">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/bootstrap.min.css">
    <link href="assets/frontend/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="assets/frontend/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/frontend/js/bootstrap.min.js"></script>

    <script SRC="assets/frontend/js/bootstrap-datepickerindex.js"></script>
    <script src="assets/frontend/js/jquery-ui.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <script src="assets/frontend/js/angular.min.js"></script>
    <script src="assets/frontend/js/angular-route.js"></script>
    <script language="javascript" src="assets/frontend/js/angular-animate.js"></script>
    <script language="javascript" src="assets/frontend/js/ui-bootstrap-tpls-0.14.3.js"></script>
	
</head>

<body class="inner_body" ng-app="myApp" ng-controller="PosCntrl">



<?php if($select_user->user_lock=='1') { ?>


    <div id="myNav" class="overlay">
        <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
       -->
        <form method="post" action="" autocomplete="off">
            <div class="pop_login">

                <div class="login">

                    <label class="text-error"> <?php echo $msg; ?></label>
                    <div class="pos_input_box">
                        <input type="text" autocomplete="off" name="emailmob" placeholder="Enter Your User Id" required
                               title="The input is not a valid User Id" maxlength="50">
                    </div>
                    <div class="pos_input_box">
                        <input type="password" autocomplete="new-password" name="pwd" class="form-control-1" placeholder="Enter Your Password" required
                               title="The input is not a valid Password" maxlength="35" min="6">
                    </div>
                    <div class="pos_input_box">
                        <input type="submit" value="Login" name="submit">
                    </div>
                </div>
                <div class="shadow"></div>


            </div>
        </form>

    </div>
    <script>
        document.getElementById("myNav").style.height = "100%";

        function openNav() {
        }

        function closeNav() {
            document.getElementById("myNav").style.height = "0%";
        }
    </script>
<?php } ?>

<div ng-view>
</div>




<script src="assets/frontend/js/app/app.js"></script>

<script src="assets/frontend/js/app/config-router.js"></script>
<script src="assets/frontend/js/app/controllers/PosCntrl.js"></script>
<script src="assets/frontend/js/app/controllers/AddonsCntrl.js"></script>
<script src="assets/frontend/js/app/controllers/TotalCntrls.js"></script>
<script src="assets/frontend/js/app/controllers/PrintpageCntrl.js"></script>



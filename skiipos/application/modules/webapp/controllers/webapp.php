<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Webapp extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }

    //Index page
    public function landing( ) {


        if( ( $this->session->userdata( 'skiindiauserid' ) ) == '' ) {
			
        if($this->input->post('submit')) {
			
             //$fileHand = fopen('temp/t44545_192.168.1.187_10_201712291712.txt', 'r');
         

              $parameter1 = array('act_mode' => 'get_user_detail',
                'Param1' => $this->input->post('emailmob'),
                'Param2' => base64_encode($this->input->post('pwd')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',);
            $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);

            $parameter1 = array('act_mode' => 's_frontuserlogin',
                'Param1' => $this->input->post('emailmob'),
                'Param2' => base64_encode($this->input->post('pwd')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',);
          
            $response['select_frontusers'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);
			
            if($response['select_frontusers']->cnt==0){
				
                $response['msg']="No Record Found";

            }
            else {
                 $parameter1 = array('act_mode' => 'check_login_status',
                'Param1' => $this->input->post('emailmob'),
                'Param2' => base64_encode($this->input->post('pwd')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',);
            $response['check_login_status'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);
            if($response['check_login_status']->cnt>0){
                 $response['msg']="No Record Found";
            }else{
               $parameter1 = array('act_mode' => 's_frontuserlogin_update',
                'Param1' => $this->input->post('emailmob'),
                'Param2' => base64_encode($this->input->post('pwd')),
                'Param3' => '1',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                );
            $detail= $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);
               $parameter1 = array('act_mode' => 'get_user_detail',
                'Param1' => $this->input->post('emailmob'),
                'Param2' => base64_encode($this->input->post('pwd')),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',);
            $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);
              
                $data = array(
                    'skiindiauserid' => $response['select_frontuser']->user_id,
                    'skiindiaemailid' => $response['select_frontuser']->emailmob,
                    'skiindialointype' => $response['select_frontuser']->logintype,
                );
               $this->session->set_userdata($data);

                $ip = $_SERVER['REMOTE_ADDR'];
                $parameter2 = array('act_mode' => 'memuserlog',
                    'user_id' => $response['select_frontuser']->user_id,
                    'remoteaddress' => gethostbyaddr($ip),
                    );

                $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);
                   
                //Order Create in file system
                 $createFileSystem = filesystem($response['select_frontuser']->user_tenentid,$response['select_frontuser']->user_id);
    
                if ($response['select_frontuser']->logintype == 'pos') {

                    header("Location:pos");
                }
                elseif ($response['select_frontuser']->logintype == 'all') {
                    header("Location:pos");
                }
                else {
                    header("Location:addons");
                }
            }
         }
        }
            $this->load->view("landingpage", $response);
        }
        else
      {
    header("Location:pos");
}}




    public function pos()
    {

        $hrs = date('H');
        //echo($t . "<br>");


      //  $this->session->unset_userdata('noofpersons');
        if($this->session->userdata('skiindiauserid')==''){
            redirect(base_url());
        }
        else
        {
            if( $this->session->userdata('datedataval')!='')
            {
                $response['dateval1']=  $this->session->userdata('datedataval');
            }
            else

            {
                $response['dateval1']=date("d/m/Y");
            }

            if ($this->session->userdata('complement') != '') {
                $response['complementval1'] = $this->session->userdata('complement');
            } else {
                $response['complementval1'] = 0;
            }




            if($this->input->post('submit')) {

                $parameter1 = array('act_mode' => 's_frontuserlogin',
                    'Param1' => $this->input->post('emailmob'),
                    'Param2' => base64_encode($this->input->post('pwd')),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '',);
                //  pend($parameter1);
                $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);


                if(empty($response['select_frontuser']) )
                {
                    $response['msg']="No Record Found";

                }
                else {

                    $data = array(
                        'skiindiauserid' => $response['select_frontuser']->user_id,
                        'skiindiaemailid' => $response['select_frontuser']->emailmob,

                    );

                    $this->session->set_userdata($data);
                   // pend($this->session->userdata);
                    $parameter2 = array('act_mode' => 'memuserlog',
                        'user_id' => $response['select_frontuser']->user_id,
                        'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
                    );
                     
                    $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);
//pend($response['select_userlog'] );

                    header("Location:pos");
                }
            }

            $parameter2 = array('act_mode' => 'selectmemuser',
                'user_id' =>  $this->session->userdata('skiindiauserid'),
                'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
            );
            //  pend($parameter2);
            $response['select_user'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameter2);

   
            $this->load->view("pos", $response);
        }





    }


    public function dateval()
    {
        $selected_data =$_REQUEST['datevalue'];

      //[datevalue] => 08/25/2017
     $selecteddate=explode("/",$selected_data);


$datedata=$selecteddate[2]."-".$selecteddate[0]."-".$selecteddate[1];

        $data = array(
            'datedataval' => $datedata,


        );

        $this->session->set_userdata($data);
      //  p($this->session->userdata('datedataval'));
        return true;
    }

    public function logout()
    {  

         
         $parametertimeslotadv = array( 'act_mode'=>'get_user_detail',
            'Param1'=>$this->session->userdata('skiindiauserid'),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $get_user_detail = $this->supper_admin->call_procedurerow('proc_timeslot_v',$parametertimeslotadv);

        /*--------------------||  Order Create in file system || ----------------------*/
       $createFileSystem = fclose_order_user_logout($get_user_detail->user_tenentid,$get_user_detail->user_id);
      //  $createFileSystem = fclose_order_system($get_user_detail->user_tenentid,$get_user_detail->user_id);
        //p($createFileSystem);exit;
       /*--------------------||  End Order Create in file system || ----------------------*/

     $parametertimeslotadv = array( 'act_mode'=>'user_update_logout',
            'Param1'=>$this->session->userdata('skiindiauserid'),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['update_logout'] = $this->supper_admin->call_procedure('proc_timeslot_v',$parametertimeslotadv);

        $this->session->unset_userdata('skiindiauserid');
        $this->session->unset_userdata('orderid');
        $this->session->unset_userdata('datedataval');
    }

    public function posData()
    {

        if($this->session->userdata('nameadv')!='')
        {
            $response['nameadvance'] =$this->session->userdata('nameadv');
        }
        else
        {
            $response['nameadvance']='';
        }
//Date Select
if( $this->session->userdata('datedataval')!='')
{
    $dateval1=  $this->session->userdata('datedataval');
    $response['dateval1']=  $this->session->userdata('datedataval');

    $daten=explode("-",$this->session->userdata('datedataval'));
    $datedata2=$daten[2].'-'.$daten[1].'-'.$daten[0];
    $dateval2= $datedata2;
}
else

{
    $dateval1=date("Y-m-d");
    $response['dateval1']=date("Y-m-d");
    $dateval2=date("d-m-y");
}

        if ($this->session->userdata('complement') != '') {
            $response['complementval1'] = $this->session->userdata('complement');
        } else {
            $response['complementval1'] = 0;
        }

//No Of Persons Select
        if( $this->session->userdata('noofpersons')!='')
        {
                $response['noofpersons']=  $this->session->userdata('noofpersons');
        }
        else

        {
              $response['noofpersons']=1;
        }

        $hrs = date('H');

//Display Advance booking timing
        $parametertimeslotadv = array( 'act_mode'=>'s_viewdailytimeslotadvance',
            'Param1'=>$dateval1,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslotadv'] = $this->supper_admin->call_procedure('proc_timeslot_v',$parametertimeslotadv);

        //Display Advance booking Packages

        $parameterpackageadv = array( 'act_mode'=>'s_viewdailypackageadv',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');
        $response['s_parameterpackageadv'] = $this->supper_admin->call_procedure('proc_packages_s',$parameterpackageadv);

        $parameter2 = array( 'act_mode'=>'front_viewaddons',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewaddons'] = $this->supper_admin->call_procedure('proc_addon_s',$parameter2);

// Display Inventory and packages
        if ($dateval1 == date("Y-m-d")) {

            $parameter2 = array('act_mode' => 'front_viewpackage',
                'Param1' => $dateval1,
                'Param2' => $hrs,
                'Param3' => $dateval2,
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',
                'Param16'=>'',
                'Param17'=>'');
        } else {
            $parameter2 = array('act_mode' => 'front_viewpackagewithouthrs',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => $dateval2,
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',
                'Param16'=>'',
                'Param17'=>'');
        }
//p( $parameter2 );
        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
//p($response['s_viewpackages']);

        //Display Hold Release data
        $parameterholdrelease = array('act_mode' => 'front_holdreleasedate',
            'Param1' => $dateval1,
            'Param2' => $this->session->userdata('skiindiauserid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
        );

        $response['holdrelease'] = $this->supper_admin->call_procedurerow('proc_holddate', $parameterholdrelease);

        //single packages

        $parameterpackagecomplete = array('act_mode' => 'front_viewpackagecomplete',
            'Param1' => $dateval1,
            'Param2' => $this->session->userdata('skiindiauserid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');

        $response['s_packagename'] = $this->supper_admin->call_procedure('proc_packages_s', $parameterpackagecomplete);




        // display user name
        $parameterlog = array('act_mode' => 'memusersesiid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );

        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameterlog);
// display last order total
        // pend($response['select_userlog'] );


        $parameterorder = array('act_mode' => 'memuserorderid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_orderlog'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameterorder);

// display packages

        foreach($response['s_viewpackages'] as $key=>$value)

        {


            $parameter4 = array( 'act_mode'=>'front_viewpackageqty',
                'Param1'=>$dateval1,
                'Param2'=>$value->dailyinventory_from,
                'Param3'=>'',
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',
                'Param16'=>'',
                'Param17'=>'');
            $response['s_viewpackagesqty'][$key] = $this->supper_admin->call_procedurerow('proc_packages_s',$parameter4);



            $data = explode(',',$response['s_viewpackages'][$key]->package_id);

            $response['s_viewpackages'][$key]->package_id = ((object)
            $data);

            $packaiteamcount=explode(",",$response['s_viewpackagesqty'][$key]->iteamcount);
            $packahename=explode(",",$response['s_viewpackages'][$key]->package_name);
            $packaheprice=explode(",",$response['s_viewpackages'][$key]->package_price);
            $packahetax=explode(",",$response['s_viewpackages'][$key]->package_tax);
            $packcolor=explode(",",$response['s_viewpackages'][$key]->package_color);
            $packtextcolor=explode(",",$response['s_viewpackages'][$key]->package_fontcolor);
            $package_shadowcolour=explode(",",$response['s_viewpackages'][$key]->package_shadowcolour);


            $response['s_viewpackages'][$key]->package_name = ((object)
            $packahename);
            $response['s_viewpackages'][$key]->package_price = ((object)
            $packaheprice);
            $response['s_viewpackages'][$key]->package_tax = ((object)
            $packahetax);
            $response['s_viewpackages'][$key]->package_color = ((object)
            $packcolor);
            $response['s_viewpackages'][$key]->package_fontcolor = ((object)
            $packtextcolor);
            $response['s_viewpackages'][$key]->package_iteamcount= ((object)
            $packaiteamcount);
            $response['s_viewpackages'][$key]->package_shadowcolour= ((object)
            $package_shadowcolour);
        }
        $datedata=explode("/",$dateval1);

        $dateval2=$datedata[0]."/".$datedata[1]."/".$datedata[2];
        $parameter1 = array('act_mode' => 's_advanceboking',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>''

        );

        $response['advanceboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter1);


        $parameter10 = array('act_mode' => 's_boking',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>''
        );

        $response['totalboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter10);

//footer data

        $parameter11 = array('act_mode' => 's_footerdata',
            'Param1' => $dateval1,
            'Param2' => $this->session->userdata('skiindiauserid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['footerboking'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter11);
        $parameter6 = array( 'act_mode'=>'s_testprintdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);


        $response['footertestprint'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter6);


        $parameter20 = array( 'act_mode'=>'s_reprintdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);


        $response['footerreprint'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter20);

        $parameter21 = array( 'act_mode'=>'s_reprinttransdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);
         $response['footerreprinttransaction'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter21);


        $parameter22 = array( 'act_mode'=>'s_reprinttransuserdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);
        $response['footerreprintuserdetails'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter22);

        $parameter23 = array( 'act_mode'=>'s_reprinttransonlinedisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);
        $response['footerreprintorderonlinedetails'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter23);



        $parameter1 = array('act_mode' => 's_selectnofflineuserselectdata',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['onlineuser'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
       // $this->session->unset_userdata('skiipages');
        if($this->session->userdata('skiipages')==1)
        {
            $response['skiipages']=1;
        }
        else
        {
            $response['skiipages']=0;
        }

//p( $response['advanceboking']);
   echo json_encode( $response);

     //  print_r(json_encode( $response['s_viewpackages']));

    }




    public function packages()
    {


      $dateval=date("Y-m-d");
        $parameter2 = array( 'act_mode'=>'front_viewpackage',
            'Param1'=>$dateval,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');
        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $this->load->view("packages", $response);
        $this->load->view("helper/left", $response);
    }

    public function packagesadd()
    {
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');

            $daten=explode("-",$this->session->userdata('datedataval'));
            $datedata2=$daten[2].'-'.$daten[1].'-'.$daten[0];
            $dateval2= $datedata2;
        }
        else

        {
            $dateval1=date("Y-m-d");
            $dateval2=date("d-m-Y");
        }
        $selected_data =json_decode($_REQUEST['packages'],true);

//        p($selected_data);
/*        [packageId] => 1
            [inventory_id] => 27
            [package_name] => Normal
            [package_price] => 1000
            [inventory_seats] => 200
            [inventory_from] => 15
            [inventory_to] => 16
            [inventory_minfrom] => 00
            [inventory_minto] => 30
*/

        $ip = $_SERVER['REMOTE_ADDR'];


        $parameter1 = array( 'act_mode'=>'s_addorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
            'Param3'=> 'Packages',
            'Param4'=> $dateval1,
            'Param5' => gethostbyaddr($ip),
            'Param6'=>'',
            'Param7'=>'User',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);

        $data = array(
            'skiindiauseruniqid' => uniqid(),
            'orderid' => $response['s_vieworder'][0]->order_id,
            'skiipages' => 1,
                );

        $this->session->set_userdata($data);

        foreach($selected_data as $k => $v):

            if ($v['inventory_from'] > 12) {
                $fromdate = ($v['inventory_from'] - 12);
                $v['inventory_fromd'] = "PM";
            } elseif ($v['inventory_from'] == 12) {
                $fromdate = ($v['inventory_from']);
                $v['inventory_fromd'] = "PM";
            } elseif ($v['inventory_from'] < 12) {
                $fromdate= ($v['inventory_from']);
                $v['inventory_fromd'] = "AM";
            }

            if ($v['inventory_to'] > 12) {
                $todate = ($v['inventory_to'] - 12);
                $v['inventory_tod'] = "PM";
            } elseif ($v['inventory_from'] == 12) {
                $todate = ($v['inventory_to']);
                $v['inventory_fromd'] = "PM";
            } elseif ($v['inventory_to'] < 12) {
                $todate= ($v['inventory_to']);
                $v['inventory_tod'] = "AM";
            }


            $parameter2 = array( 'act_mode'=>'s_addpackage_master',
                'Param1'=>$v['packageId'],
                'Param2'=> $v['inventory_id'],
                'Param3'=> $v['package_name'],
                'Param4'=> $v['package_price'],
                'Param5'=> $v['inventory_seats'],
                'Param6'=>$fromdate,
                'Param7'=>$todate,
                'Param8'=>$v['inventory_minfrom'],
                'Param9'=>$v['inventory_minto'],
                'Param10'=>$response['s_vieworder'][0]->order_id,
                'Param11' => $v['inventory_packagetype'],
                'Param12'=>$dateval1,
                'Param13' => gethostbyaddr($ip),
                'Param14' => $v['inventory_fromd'],
                'Param15' => $v['inventory_tod'],
                'Param16' =>$v['inventory_from'],
                'Param17' =>$v['inventory_to'],
                'Param18' =>'User',
                'Param19' =>'',

            );
            $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_order_filter_s',$parameter2);

        endforeach;





        $this->session->unset_userdata('noofpersons');


        // tax add .client request last time if we start initial it take more time in  i hrs it is best option
        $parametertax = array( 'act_mode'=>'s_selectorder_tax',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_viewordertax'] = $this->supper_admin->call_procedure('proc_order',$parametertax);


        foreach($response['s_viewordertax'] as $v)
        {


            $parametertaxinsert = array( 'act_mode'=>'s_inserttorder_tax',
                'Param1' => $v->orderproduct_id,
                'Param2'=>$v->tax_name,
                'Param3'=> $v->tax_value,
                'Param4' => $v->tax_name1,
                'Param5' => $v->tax_name2,
                'Param6' => $v->tax_name3,
                'Param7' => $v->tax_percentage2,
                'Param8' => $v->tax_percentage3,
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15' => $this->session->userdata('orderid'),

            );


            $response['s_viewordertaxinsert'] = $this->supper_admin->call_procedure('proc_order',$parametertaxinsert);
        }




        $parameter1 = array( 'act_mode'=>'s_selectorder_total',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedurerow('proc_order',$parameter1);


        $parameter2 = array( 'act_mode'=>'s_updateorder_total',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>$response['s_vieworder']->pacprice,
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['updateorder'] = $this->supper_admin->call_procedurerow('proc_order',$parameter2);

        return true;
    }

    //Total Booking
    public function totalbooking()
    {
        $hrs = date('H');

        if ($this->session->userdata('skiindiauserid') == '') {
            redirect(base_url());
        } else {
            if ($this->session->userdata('datedataval') != '') {
                $response['dateval1'] = $this->session->userdata('datedataval');
            } else {
                $response['dateval1'] = date("Y-m-d");
            }

            if ($this->session->userdata('complement') != '') {
                $response['complementval1'] = $this->session->userdata('complement');
            } else {
                $response['complementval1'] = 0;
            }


            if ($this->input->post('submit')) {

                $parameter1 = array('act_mode' => 's_frontuserlogin',
                    'Param1' => $this->input->post('emailmob'),
                    'Param2' => base64_encode($this->input->post('pwd')),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '',);
                //  pend($parameter1);
                $response['select_frontuser'] = $this->supper_admin->call_procedurerow('proc_frontuser_s', $parameter1);


                if (empty($response['select_frontuser'])) {
                    $response['msg'] = "No Record Found";

                } else {

                    $data = array(
                        'skiindiauserid' => $response['select_frontuser']->user_id,
                        'skiindiaemailid' => $response['select_frontuser']->emailmob,

                    );

                    $this->session->set_userdata($data);
                    $parameter2 = array('act_mode' => 'memuserlog',
                        'user_id' => $response['select_frontuser']->user_id,
                        'remoteaddress' => $_SERVER['REMOTE_ADDR'],
                    );
                    //  pend($parameter2);
                    $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);


                    header("Location:totalbooking");
                }
            }

        }
        $parameter2 = array('act_mode' => 'selectmemuser',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' => $_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_user'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameter2);

        $this->load->view("totalbooking", $response);

    }

    public function advancebookingadd()
    {
        $selected_data =json_decode($_REQUEST['advancebooking'],true);

        $dateval1data=explode("/",$selected_data['date']);

        $datenew=$dateval1data[2]."-".$dateval1data[0]."-".$dateval1data[1];

  /*
        [name] => ytryrty
    [mobile] => 4564564
    [noofpersons] => 5
    [email] => snowworld@gmail.com
    [date] => 08/30/2017
    [packagetime] => 206
    [packagename] => 3
    [payment] => 5*/


        $parameter3 = array( 'act_mode'=>'s_addadvpackagetime',
            'Param1'=>$selected_data['packagetime'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>'',
        );

        $response['advpackagetime'] = $this->supper_admin->call_procedurerow('proc_advanceboking',$parameter3);

        $parameter4 = array( 'act_mode'=>'s_addadvpackagename',
            'Param1'=>$selected_data['packagename'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>'',
        );

        $response['advpackagepackagename'] = $this->supper_admin->call_procedurerow('proc_advanceboking',$parameter4);

$packagename=explode("-",$selected_data['packagename']);
$addonsname=explode("-",$selected_data['addonsname']);
$totaddon=0;

$first_part_arr = [];
$second_part_arr = [];

for($i=0;$i<(count($selected_data['addonsname']));$i++)
{
    $addonsdata=explode("-",$selected_data['addonsname'][$i]);

    $totaddon += $addonsdata[0];
    array_push($first_part_arr, $addonsdata[0]) ;
    array_push($second_part_arr, $addonsdata[1]) ;
}


//p( implode(',', $second_part_arr) ) ;

        //p(implode(",",$selected_data['addonsname']));
//p(count($selected_data['addonsname']));


        $parameter2 = array( 'act_mode'=>'s_addadv',
            'Param1'=>$selected_data['name'],
            'Param2'=> $selected_data['mobile'],
            'Param3'=> $selected_data['noofpersons'],
            'Param4'=> $selected_data['email'],
            'Param5'=> $selected_data['packagetime'],
            'Param6'=>$selected_data['packagename'],
            'Param7'=>$selected_data['payment'],
            'Param8'=>$datenew,
            'Param9'=>implode(",",$selected_data['addonsname']),
            'Param10'=>($selected_data['noofpersons']*($selected_data['packagename']+$totaddon)),
            'Param11'=>$selected_data['discamount'],
            'Param12'=>($selected_data['noofpersons']*($selected_data['packagename']+$totaddon)-$selected_data['discamount']-$selected_data['payment']),
            'Param13'=>$packagename[0],
            'Param14'=>$packagename[1],
            'Param15'=>$totaddon,
            'Param16'=>( implode(',', $second_part_arr) ),
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>'',
 );

        $response['advancebooking'] = $this->supper_admin->call_procedurerow('proc_advanceboking',$parameter2);


        $parameter5 = array( 'act_mode'=>'s_updateadv',
            'Param1'=>$response['advancebooking']->dd,
            'Param2'=>  $response['advpackagetime']->dailyinventory_from,
            'Param3'=>  $response['advpackagetime']->dailyinventory_to,
            'Param4'=>  $response['advpackagetime']->dailyinventory_minfrom,
            'Param5'=>  $response['advpackagetime']->dailyinventory_minto,
            'Param6'=>$response['advpackagepackagename']->tax_value,
            'Param7'=>$response['advpackagepackagename']->tax_percentage2,
            'Param8'=>$response['advpackagepackagename']->tax_percentage3,
            'Param9'=>$response['advpackagepackagename']->package_name,
            'Param10'=>$response['advpackagepackagename']->package_price,
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>'',

            );

        $response['updateadvancebooking'] = $this->supper_admin->call_procedurerow('proc_advanceboking',$parameter5);





        return $selected_data;
    }


 public function advancebookingaddorder()
    {
        $ip = $_SERVER['REMOTE_ADDR'];

        if ($this->session->userdata('datedataval') != '')
        {
            $dateval1=  $this->session->userdata('datedataval');
        }
        else

        {
            $dateval1=date("Y-m-d");
        }

         $parameter1 = array( 'act_mode'=>'s_addorder_master',
            'Param1'=>uniqid(),
            'Param2'=> $this->session->userdata('skiindiauserid'),
             'Param3' => 'Package',
            'Param4'=> $dateval1,
             'Param5' => gethostbyaddr($ip),
             'Param6' => ($_REQUEST['advancepayment_noofpersons'] * $_REQUEST['package_price'] - $_REQUEST['advancepayment_payment']),
            'Param7'=>'Advance Booking',
            'Param8'=>$_REQUEST['advancepayment_id'],
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'', );
        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);


        $parameteradv = array( 'act_mode'=>'s_addorder_advanceid',
            'Param1'=>uniqid(),
            'Param2'=> $response['s_vieworder'][0]->order_id,
            'Param3' => $_REQUEST['advancepayment_id'],
            'Param4'=> $this->session->userdata('skiindiauserid'),
            'Param5' => '',
            'Param6' => '',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'', );
        $response['s_vieworderadv'] = $this->supper_admin->call_procedure('proc_order',$parameteradv);


        $data = array(
            'skiindiauseruniqid' => uniqid(),
            'orderid' => $response['s_vieworder'][0]->order_id,

                );

        $this->session->set_userdata($data);
for($i=0; $i<$_REQUEST['advancepayment_noofpersons'];$i++) {
    // echo $_REQUEST['dailyinventory_from']."--".$_REQUEST['dailyinventory_to'];

    if ($_REQUEST['dailyinventory_from'] > 12) {
        $from = ($_REQUEST['dailyinventory_from'] - 12);
        $_REQUEST['inventory_fromd'] = "PM";

    } elseif ($_REQUEST['dailyinventory_from'] == 12) {
        $from = ($_REQUEST['dailyinventory_from']);
        $_REQUEST['inventory_fromd'] = "PM";

    } elseif ($_REQUEST['dailyinventory_from'] < 12) {
        $from = ($_REQUEST['dailyinventory_from']);
        $_REQUEST['inventory_fromd'] = "AM";
    }

    if ($_REQUEST['dailyinventory_to'] > 12) {
        $to = ($_REQUEST['dailyinventory_to'] - 12);
        $_REQUEST['inventory_tod'] = "PM";

    } elseif ($_REQUEST['dailyinventory_to'] == 12) {
        $to = ($_REQUEST['dailyinventory_to']);
        $_REQUEST['inventory_fromd'] = "PM";

    } elseif ($_REQUEST['dailyinventory_to'] < 12) {
        $to = ($_REQUEST['dailyinventory_to']);
        $_REQUEST['inventory_tod'] = "AM";

    }


    $parameter2 = array('act_mode' => 's_addpackage_master',
                'Param1'=>$_REQUEST['package_id'],
                'Param2'=> $_REQUEST['dailyinventory_id'],
                'Param3'=> $_REQUEST['package_name'],
                'Param4'=> $_REQUEST['package_price'],
                'Param5'=> $_REQUEST['dailyinventory_seats'],
        'Param6' => $from,
        'Param7' => $to,
                'Param8'=>$_REQUEST['dailyinventory_minfrom'],
                'Param9'=>$_REQUEST['dailyinventory_minto'],
                'Param10'=>$response['s_vieworder'][0]->order_id,
        'Param11' => 'Package',
                'Param12'=>$dateval1,
        'Param13' => gethostbyaddr($ip),
        'Param14' => $_REQUEST['inventory_fromd'],
        'Param15' => $_REQUEST['inventory_tod'],


    );

 $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_order',$parameter2);





}


  //  [advancepayment_id] => 3

  //   [advancepayment_name] => yty
  //   [advancepayment_mobile] => 6575675656
  //   [advancepayment_noofpersons] => 4
  //   [advancepayment_email] => durgesh@chiliadprocons.in
   //  [advancepayment_entrydate] => 08/28/2017
   //  [dailyinventory_from] => 13
   //  [dailyinventory_minfrom] => 00
   //  [dailyinventory_to] => 14
   //  [dailyinventory_minto] => 30
   //  [package_name] => School
  //   [package_price] => 700
  //   [advancepayment_payment] => 44
  //   [rembalancepaidby] => y
//[package_id] => 3
  //  [dailyinventory_id] => 194
  //  [dailyinventory_seats] => 200

    }

    public function groupbooking()
    {
        $groupbooking_data =json_decode($_REQUEST['groupbooking'],true);

        $data = array(
       'noofpersons' =>$groupbooking_data['noofpersons'],

     );

       $this->session->set_userdata($data);


        echo json_encode( $groupbooking_data['noofpersons']);
    }
    public function unsetsessions()
    {
        $this->session->unset_userdata('noofpersons');
    }
    public function userlock()
    {

        $parameter2 = array('act_mode' => 'memuserlock',
            'user_id' =>  $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameter2);
return true;
           }


    public function refundlockvaladdons($data)
    {
        $selected_data = json_decode($_REQUEST['printing'], true);

        // $selected_data['transid']

        $parameter1 = array('act_mode' => 's_refundaddonslockval',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }

    public function refundlockvalticket($data)
    {
        $selected_data = json_decode($_REQUEST['printing'], true);

        // $selected_data['transid']

        $parameter1 = array('act_mode' => 's_refundticketlockval',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }

    public function printingaddonslockval($data)
    {
        $selected_data = json_decode($_REQUEST['printing'], true);

        // $selected_data['transid']

        $parameter1 = array('act_mode' => 's_printingaddonslockval',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        $parametercompamast = array('act_mode' => 's_selectorder_compamast',
            'Param1' => $response['s_vieworder'][0]->order_id,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_viewcompamast'] = $this->supper_admin->call_procedure('proc_order', $parametercompamast);

        $parameteraddons = array('act_mode' => 's_addorder_printdisplayvaladdons',
            'Param1' => $response['s_vieworder'][0]->order_id,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );


        $response['s_vieworderaddons'] = $this->supper_admin->call_procedure('proc_order', $parameteraddons);

        $response['s_vieworderid'] = $response['s_vieworder'][0]->order_id;
        $response['s_order_statusdetail'] = $response['s_vieworder'][0]->order_statusdetail;
        $response['s_order_total'] = $response['s_vieworder'][0]->order_total;

        $response['s_order_changeval'] = $response['s_vieworder'][0]->order_changeval;

        echo json_encode($response);
//p(($response['s_vieworder']));
    }


    public function printinglockval($data)
    {
        $selected_data =json_decode($_REQUEST['printing'],true);

       // $selected_data['transid']

        $parameter1 = array( 'act_mode'=>'s_printinglockval',
            'Param1'=>$selected_data['transid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);
        echo json_encode($response['s_vieworder']);
//p(($response['s_vieworder']));
    }
    public function printinglockvaldata($data)
    {
     //   $selected_data =json_decode($_REQUEST['orderproduct_id'],true);

        // $selected_data['transid']
      //  pend($_REQUEST['orderproduct_id']);
for($i=0;$i<=count($_REQUEST['orderproduct_id']);$i++) {


    $parameter1 = array('act_mode' => 's_addorder_printdisplayval',
        'Param1' => $_REQUEST['orderproduct_id'][$i],
        'Param2' => '',
        'Param3' => '',
        'Param4' => '',
        'Param5' => '',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',
        'Param10' => '',
        'Param11' => '',
        'Param12' => '',
        'Param13' => '',
        'Param14' => '',
        'Param15' => '',

    );

   $response['printdisplayval'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
  echo json_encode($response['printdisplayval']);
//p($response['printdisplayval']);
}   }

//cancelorderbooking
    public function cancelorderbooking($orderDetails,$typeofpayment)
    {

        $selected_data =json_decode($_REQUEST['grouporderDetails'],true);
        $ip = $_SERVER['REMOTE_ADDR'];
        $parameter1 = array( 'act_mode'=>'s_cancelorderbooking',
            'Param1'=>$selected_data[0]['order_id'],
            'Param2'=>$_REQUEST['typeofpayment'],
            'Param3'=>'Refund',
            'Param4' => gethostbyaddr($ip),
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );

        $response['s_cancelorderbooking'] = $this->supper_admin->call_procedure('proc_order',$parameter1);


        return true;


    }


    public function cancelorderbookingaddons($orderDetails, $typeofpayment)
    {

        $selected_data = json_decode($_REQUEST['grouporderDetails'], true);
        $ip = $_SERVER['REMOTE_ADDR'];
        $parameter1 = array('act_mode' => 's_cancelorderbookingaddons',
            'Param1' => $selected_data[0]['order_id'],
            'Param2' => $_REQUEST['typeofpayment'],
            'Param3' => 'Refund',
            'Param4' => gethostbyaddr($ip),
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_cancelorderbooking'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        return true;


    }


    public function cancelorderbookingtransactions($orderDetails, $typeofpayment)
    {

        $selected_data = json_decode($_REQUEST['grouporderDetails'], true);

        $parameter1 = array('act_mode' => 's_cancelorderbookingtransactions',
            'Param1' => $selected_data[0]['orderid'],
            'Param2' => $_REQUEST['typeofpayment'],
            'Param3' => 'Refund',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_cancelorderbooking'] = $this->supper_admin->call_procedure('proc_order', $parameter1);

        return true;


    }

//Transaction Online print


    public function transactionofflineuserlockvaldata()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactionofflineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        echo json_encode($response['s_transactiononlineprint']);

    }




    //Transaction Online print


    public function transactiononlinelockvaldata()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactionofflineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        echo json_encode($response['s_transactiononlineprint']);

    }

    public function transactiononlinelockuservaldata()
    {

        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
            $response['dateval1']=  $this->session->userdata('datedataval');
        }
        else

        {
            $dateval1=date("Y-m-d");
            $response['dateval1']=date("Y-m-d");
        }




        $selected_data['userssid'] = $_REQUEST['transaction'];

//Select

        $parameter1 = array('act_mode' => 's_selecttransactionofflineuserprintdata',
            'Param1' => $selected_data['userssid'],
            'Param2' => $dateval1,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        echo json_encode($response['s_transactiononlineprint']);

    }


    public function transactiononlinelockval()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactiononlineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_order', $parameter1);


        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);


        $parameter3 = array('act_mode' => 's_selecttransactiononlineprint',
            'Param1' => $selected_data['transid'],
            'Param2' => $response['s_transactiononlineprint']->pacorderid,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintdata'] = $this->supper_admin->call_procedure('proc_order', $parameter3);

        echo json_encode($response['s_transactiononlineprintdata']);

    }
//Offline user value


//Online user value
    public function onlineuserval()
    {

        if ($this->session->userdata('datedataval') != '') {
            $dateval1 = $this->session->userdata('datedataval');
        } else {
            $dateval1 = date("Y-m-d");
        }


//31/08/2017
        // $selected_date = '13/08/2017';
//Select

        $parameter1 = array('act_mode' => 's_selectnofflineuserselectdata',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['onlineuser'] = $this->supper_admin->call_procedure('proc_order', $parameter1);


        echo json_encode($response['onlineuser']);


    }

    public function updatepassword()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);

        /*     [/updatepassword] =>
         [transaction] => {"newpassword":"ssssss","cpassword":"ssssss"}
         [uid] => 1

        [newpassword] => 111111
         [cpassword] => 111111

        */

        $parameter1 = array(
            'UserId' => $_REQUEST['uid'],
            'newpassword' => base64_encode($selected_data['newpassword']),


        );

        $response['changepass'] = $this->supper_admin->call_procedure('proc_changePassword', $parameter1);

        return true;
    }

//Transaction Online print
    public function transactiononlinerefundlockval()
    {

        $selected_data = json_decode($_REQUEST['transaction'], true);
        if ($selected_data['transid'] != '') {
            $selected_data['transid'] = $selected_data['transid'];
        } else {
            $selected_data['transid'] = $_REQUEST['transaction'];
        }

//Select

        $parameter1 = array('act_mode' => 's_selecttransactiononlineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_order', $parameter1);


        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);


        $parameter3 = array('act_mode' => 's_selecttransactiononlineprint',
            'Param1' => $selected_data['transid'],
            'Param2' => $response['s_transactiononlineprint']->pacorderid,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintdata'] = $this->supper_admin->call_procedure('proc_order', $parameter3);


        echo json_encode($response['s_transactiononlineprintdata']);


    }

//bookinggtransactions
    public function addbookinggtransactions($transactionbooking)
    {
        $selected_data = json_decode($_REQUEST['transactionbooking'], true);


//p( $response['s_bookinggtransactions']);

//Select

        $parameter1 = array('act_mode' => 's_selecttransactiononlineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' => $selected_data['name'],
            'Param3' => $selected_data['bookingno'],
            'Param4' => $selected_data['phoneno'],
            'Param5' => $selected_data['emailid'],
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_orderbooking', $parameter1);


        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);


        $parameter3 = array('act_mode' => 's_orderbooking_master',
            'Param1' => $selected_data['transid'],
            'Param2' => $selected_data['name'],
            'Param3' => $selected_data['bookingno'],
            'Param4' => $selected_data['phoneno'],
            'Param5' => $selected_data['emailid'],
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_bookinggtransactions'] = $this->supper_admin->call_procedure('proc_orderbooking', $parameter3);

        echo json_encode($response);


    }


//bookinggOfflinetransactions
    public function addbookinggofflinetransactions($transactionbooking)
    {
        $selected_data = json_decode($_REQUEST['transactionbooking'], true);


//p( $response['s_bookinggtransactions']);

//Select

        $parameter1 = array('act_mode' => 's_selecttransactionofflineprintdata',
            'Param1' => $selected_data['transid'],
            'Param2' =>'',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprint'] = $this->supper_admin->call_procedurerow('proc_orderbooking', $parameter1);


/*        $parameter2 = array('act_mode' => 's_selecttransactiononlineprintdataupdate',
            'Param1' => $response['s_transactiononlineprint']->pacorderid,
            'Param2' => $response['s_transactiononlineprint']->ticketid,
            'Param3' => $response['s_transactiononlineprint']->departuredate,
            'Param4' => $response['s_transactiononlineprint']->departuretime,
            'Param5' => $response['s_transactiononlineprint']->frommin,
            'Param6' => $response['s_transactiononlineprint']->tohrs,
            'Param7' => $response['s_transactiononlineprint']->tomin,
            'Param8' => $response['s_transactiononlineprint']->txtfromd,
            'Param9' => $response['s_transactiononlineprint']->txttod,
            'Param10' => $response['s_transactiononlineprint']->comp_gstname,
            'Param11' => $response['s_transactiononlineprint']->comp_name,
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_transactiononlineprintupdate'] = $this->supper_admin->call_procedure('proc_order', $parameter2);*/


        $parameter3 = array('act_mode' => 's_orderbookingoffline_master',
            'Param1' => $selected_data['transid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['s_bookinggtransactions'] = $this->supper_admin->call_procedure('proc_orderbooking', $parameter3);

        echo json_encode($response);


    }


    public function nocomplement()
    {
        $data = array(
            'complement' => 1,


        );

        $this->session->set_userdata($data);
        return true;

    }

    public function complementval()
    {
        $data = array(
            'complement' => 0,


        );

        $this->session->set_userdata($data);
        return true;

    }

    public function totalbookingposData()
    {
        if($this->session->userdata('nameadv')!='')
        {
            $response['nameadvance'] =$this->session->userdata('nameadv');
        }
        else
        {
            $response['nameadvance']='';
        }
//Date Select
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
            $response['dateval1']=  $this->session->userdata('datedataval');
            $daten=explode("-",$this->session->userdata('datedataval'));
            $datedata2=$daten[2].'-'.$daten[1].'-'.$daten[0];
            $dateval2= $datedata2;


        }
        else

        {
            $dateval1=date("Y-m-d");
            $response['dateval1']=date("Y-m-d");
            $dateval2=date("d-m-Y");
        }

        if ($this->session->userdata('complement') != '') {
            $response['complementval1'] = $this->session->userdata('complement');
        } else {
            $response['complementval1'] = 0;
        }

//No Of Persons Select
        if( $this->session->userdata('noofpersons')!='')
        {
            $response['noofpersons']=  $this->session->userdata('noofpersons');
        }
        else

        {
            $response['noofpersons']=1;
        }

        $hrs = date('H');

//Display Advance booking timing
        $parametertimeslotadv = array( 'act_mode'=>'s_viewdailytimeslotadvance',
            'Param1'=>$dateval1,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslotadv'] = $this->supper_admin->call_procedure('proc_timeslot_v',$parametertimeslotadv);

        //Display Advance booking Packages

        $parameterpackageadv = array( 'act_mode'=>'s_viewdailypackageadv',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');
        $response['s_parameterpackageadv'] = $this->supper_admin->call_procedure('proc_packages_s',$parameterpackageadv);


        $parameter1 = array('act_mode' => 's_selectnofflineuserselectdata',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['onlineuser'] = $this->supper_admin->call_procedure('proc_order', $parameter1);

// Display Inventory and packages
        if ($dateval1 == date("Y-m-d")) {
            $parameter2 = array('act_mode' => 'front_viewpackagewithouthrs',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => $dateval2,
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',
                'Param16'=>'',
                'Param17'=>'');
        } else {
            $parameter2 = array('act_mode' => 'front_viewpackagewithouthrs',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => $dateval2,
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',
                'Param16'=>'',
                'Param17'=>'');
        }

        $response['s_viewpackages'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);


        //single packages

        $parameterpackagecomplete = array('act_mode' => 'front_viewpackagecomplete',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'');

        $response['s_packagename'] = $this->supper_admin->call_procedure('proc_packages_s', $parameterpackagecomplete);




        // display user name
        $parameterlog = array('act_mode' => 'memusersesiid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );

        $response['select_userlog'] = $this->supper_admin->call_procedure('proc_memberlog_v', $parameterlog);
// display last order total
        // pend($response['select_userlog'] );


        $parameterorder = array('act_mode' => 'memuserorderid',
            'user_id' => $this->session->userdata('skiindiauserid'),
            'remoteaddress' =>$_SERVER['REMOTE_ADDR'],
        );
        //  pend($parameter2);
        $response['select_orderlog'] = $this->supper_admin->call_procedurerow('proc_memberlog_v', $parameterorder);

// display packages

        foreach($response['s_viewpackages'] as $key=>$value)

        {


            $parameter4 = array( 'act_mode'=>'front_viewpackageqty',
                'Param1'=>$dateval1,
                'Param2'=>$value->dailyinventory_from,
                'Param3'=>'',
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
                'Param13'=>'',
                'Param14'=>'',
                'Param15'=>'',
                'Param16'=>'',
                'Param17'=>'');
            $response['s_viewpackagesqty'][$key] = $this->supper_admin->call_procedurerow('proc_packages_s',$parameter4);



            $data = explode(',',$response['s_viewpackages'][$key]->package_id);

            $response['s_viewpackages'][$key]->package_id = ((object)
            $data);

            $packaiteamcount=explode(",",$response['s_viewpackagesqty'][$key]->iteamcount);
            $packahename=explode(",",$response['s_viewpackages'][$key]->package_name);
            $packaheprice=explode(",",$response['s_viewpackages'][$key]->package_price);
            $packahetax=explode(",",$response['s_viewpackages'][$key]->package_tax);
            $packcolor=explode(",",$response['s_viewpackages'][$key]->package_color);
            $packtextcolor=explode(",",$response['s_viewpackages'][$key]->package_fontcolor);


            $response['s_viewpackages'][$key]->package_name = ((object)
            $packahename);
            $response['s_viewpackages'][$key]->package_price = ((object)
            $packaheprice);
            $response['s_viewpackages'][$key]->package_tax = ((object)
            $packahetax);
            $response['s_viewpackages'][$key]->package_color = ((object)
            $packcolor);
            $response['s_viewpackages'][$key]->package_fontcolor = ((object)
            $packtextcolor);
            $response['s_viewpackages'][$key]->package_iteamcount= ((object)
            $packaiteamcount);

        }


      //  $datedata=explode("/",$dateval1);

       // $dateval2=$datedata[2]."-".$datedata[0]."-".$datedata[1];
        $parameter1 = array('act_mode' => 's_advanceboking',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>''

        );

        $response['advanceboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter1);

        $parameter10 = array('act_mode' => 's_boking',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
            'Param16'=>'',
            'Param17'=>'',
            'Param18'=>'',
            'Param19'=>''
        );

        $response['totalboking'] = $this->supper_admin->call_procedure('proc_advanceboking', $parameter10);

//footer data

        $parameter11 = array('act_mode' => 's_footerdata',
            'Param1' => $dateval1,
            'Param2' => $this->session->userdata('skiindiauserid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['footerboking'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter11);

//Print Page List



        $parameter1 = array( 'act_mode'=>'s_cancelorddisplay',
            'Param1'=>$this->session->userdata('orderid'),
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',

        );


        $response['s_vieworder'] = $this->supper_admin->call_procedure('proc_order',$parameter1);


//footer data

        $parameter11 = array('act_mode' => 's_footerdata',
            'Param1' => $dateval1,
            'Param2' => $this->session->userdata('skiindiauserid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['footerboking'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter11);
        $parameter6 = array( 'act_mode'=>'s_testprintdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);


        $response['footertestprint'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter6);


        $parameter20 = array( 'act_mode'=>'s_reprintdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);


        $response['footerreprint'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter20);

        $parameter21 = array( 'act_mode'=>'s_reprinttransdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);
        $response['footerreprinttransaction'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter21);


        $parameter22 = array( 'act_mode'=>'s_reprinttransuserdisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);
        $response['footerreprintuserdetails'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter22);

        $parameter23 = array( 'act_mode'=>'s_reprinttransonlinedisplay',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=> '',
            'Param4'=> '',
            'Param5'=> '',);
        $response['footerreprintorderonlinedetails'] = $this->supper_admin->call_procedurerow('proc_printingpage_f',$parameter23);



        $parameter1 = array('act_mode' => 's_selectnofflineuserselectdata',
            'Param1' => $dateval1,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

        );

        $response['onlineuser'] = $this->supper_admin->call_procedure('proc_order', $parameter1);
        // $this->session->unset_userdata('skiipages');
        if($this->session->userdata('skiipages')==1)
        {
            $response['skiipages']=1;
        }
        else
        {
            $response['skiipages']=0;
        }



        if($this->session->userdata('skiipages')>=1)
{
    $response['skiipages']=1;
}
else
    {
        $response['skiipages']=0;
    }

//p( $response['advanceboking']);
        echo json_encode( $response);

        //  print_r(json_encode( $response['s_viewpackages']));

    }
    public function groupbookingvalue()
    {
        $groupbooking_data =json_decode($_REQUEST['groupbooking'],true);


        /*   Array(    [name] => bivek
        [email] => snowworld@gmail.com
    [packagetime] => 356
        [mobile] => 4645646456
        [noofpersons] => 3
        [date] => 11/30/2017)*/
        $parameter11 = array('act_mode' => 's_groupbookingfrontadd',
            'Param1' => $groupbooking_data['name'],
            'Param2' => $groupbooking_data['email'],
            'Param3' => $groupbooking_data['packagetime'],
            'Param4' => $groupbooking_data['mobile'],
            'Param5' => $groupbooking_data['noofpersons'],
            'Param6' => $groupbooking_data['date'],
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['groupbookingfrontadd'] = $this->supper_admin->call_procedurerow('proc_groupbooking', $parameter11);

        $parameter11 = array('act_mode' => 's_groupbookingfrontselect',
            'Param1' => $groupbooking_data['noofpersons'],
            'Param2' => $groupbooking_data['packagetime'],
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['groupbookingfrontselect'] = $this->supper_admin->call_procedure('proc_groupbooking', $parameter11);








        $data = array(
            'namegroup' =>$groupbooking_data['name'],
            'emailgroup' =>$groupbooking_data['email'],
            'packagetimegroup' =>$groupbooking_data['packagetime'],
            'mobilegroup' =>$groupbooking_data['mobile'],
            'noofpersonsgroup' =>$groupbooking_data['noofpersons'],
            'dategroup' =>$groupbooking_data['date'],

        );

        $this->session->set_userdata($data);
        //return true;
        echo json_encode( $response['groupbookingfrontselect']);
    }

//Offline booking status update
public function offlinebookingstatusupdate()
{

    $offlinebookingstatus_data =json_decode($_REQUEST['onlinebookingstatus'],true);
$id=$_REQUEST['uid'];


$parameterpackagetime = array('act_mode' => 'selectdate',
    'Param1' => $offlinebookingstatus_data['packagetime'],
    'Param2' => '',
    'Param3' => '',
    'Param4' => '',
    'Param5' => '',
    'Param6' => '',
    'Param7' => '',
    'Param8' => '',
    'Param9' => '',

);

    $response['packagetime'] = $this->supper_admin->call_procedurerow('proc_dailyinventory', $parameterpackagetime);

if($response['packagetime']->dailyinventory_from>12)
{
    $fromdate=$response['packagetime']->dailyinventory_from-12;
    $fromampm='PM';
}
elseif($response['packagetime']->dailyinventory_from<12)
{
    $fromdate=$response['packagetime']->dailyinventory_from;
    $fromampm='AM';
}
elseif($response['packagetime']->dailyinventory_from==12)
{
    $fromdate=$response['packagetime']->dailyinventory_from;
    $fromampm='PM';
}

    if($response['packagetime']->dailyinventory_to>12)
    {
        $todate=$response['packagetime']->dailyinventory_to-12;
        $toampm='PM';
    }
    elseif($response['packagetime']->dailyinventory_to<12)
    {
        $todate=$response['packagetime']->dailyinventory_to;
        $toampm='AM';
    }
    elseif($response['packagetime']->dailyinventory_to<=12)
    {
        $todate=$response['packagetime']->dailyinventory_to;
        $toampm='PM';
    }
if($response['packagetime']->dailyinventory_from!='') {
    $parameter11 = array('act_mode' => 's_offlinefrontststuschage',
        'Param1' => $offlinebookingstatus_data['op_ticket_print_status'],
        'Param2' => $id,
        'Param3' => $response['packagetime']->dailyinventory_minfrom,
        'Param4' => $todate,
        'Param5' => $response['packagetime']->dailyinventory_minto,
        'Param6' => $fromampm,
        'Param7' => $toampm,
        'Param8' => $fromdate,
        'Param9' => $offlinebookingstatus_data['comments'],

    );
}
else
{
    $parameter11 = array('act_mode' => 's_offlinefrontststuschagenotselect',
        'Param1' => $offlinebookingstatus_data['op_ticket_print_status'],
        'Param2' => $id,
        'Param3' => $response['packagetime']->dailyinventory_minfrom,
        'Param4' => $todate,
        'Param5' => $response['packagetime']->dailyinventory_minto,
        'Param6' => $fromampm,
        'Param7' => $toampm,
        'Param8' => $fromdate,
        'Param9' => $offlinebookingstatus_data['comments'],

    );
}
    $response['onlinefrontstatus'] = $this->supper_admin->call_procedure('proc_onlineorder_s', $parameter11);

}




public function onlinebookingstatusupdate()
{

    $onlinebookingstatus_data =json_decode($_REQUEST['onlinebookingstatus'],true);
$id=$_REQUEST['uid'];


$parameterpackagetime = array('act_mode' => 'selectdate',
    'Param1' => $onlinebookingstatus_data['packagetime'],
    'Param2' => '',
    'Param3' => '',
    'Param4' => '',
    'Param5' => '',
    'Param6' => '',
    'Param7' => '',
    'Param8' => '',
    'Param9' => '',

);

    $response['packagetime'] = $this->supper_admin->call_procedurerow('proc_dailyinventory', $parameterpackagetime);

if($response['packagetime']->dailyinventory_from>12)
{
    $fromdate=$response['packagetime']->dailyinventory_from-12;
    $fromampm='PM';
}
elseif($response['packagetime']->dailyinventory_from<12)
{
    $fromdate=$response['packagetime']->dailyinventory_from;
    $fromampm='AM';
}
elseif($response['packagetime']->dailyinventory_from==12)
{
    $fromdate=$response['packagetime']->dailyinventory_from;
    $fromampm='PM';
}

    if($response['packagetime']->dailyinventory_to>12)
    {
        $todate=$response['packagetime']->dailyinventory_to-12;
        $toampm='PM';
    }
    elseif($response['packagetime']->dailyinventory_to<12)
    {
        $todate=$response['packagetime']->dailyinventory_to;
        $toampm='AM';
    }
    elseif($response['packagetime']->dailyinventory_to<=12)
    {
        $todate=$response['packagetime']->dailyinventory_to;
        $toampm='PM';
    }
if($response['packagetime']->dailyinventory_from!='') {
    $parameter11 = array('act_mode' => 's_onlinefrontststuschage',
        'Param1' => $onlinebookingstatus_data['op_ticket_print_status'],
        'Param2' => $id,
        'Param3' => $response['packagetime']->dailyinventory_minfrom,
        'Param4' => $todate,
        'Param5' => $response['packagetime']->dailyinventory_minto,
        'Param6' => $fromampm,
        'Param7' => $toampm,
        'Param8' => $fromdate,
        'Param9' => $onlinebookingstatus_data['comments'],

    );
}
else
{
    $parameter11 = array('act_mode' => 's_onlinefrontststuschagenotselect',
        'Param1' => $onlinebookingstatus_data['op_ticket_print_status'],
        'Param2' => $id,
        'Param3' => $response['packagetime']->dailyinventory_minfrom,
        'Param4' => $todate,
        'Param5' => $response['packagetime']->dailyinventory_minto,
        'Param6' => $fromampm,
        'Param7' => $toampm,
        'Param8' => $fromdate,
        'Param9' => $onlinebookingstatus_data['comments'],

    );
}
    $response['onlinefrontstatus'] = $this->supper_admin->call_procedure('proc_onlineorder_s', $parameter11);

}

public function holdpbooking()
{
    if( $this->session->userdata('datedataval')!='') {
        $dateval1=  $this->session->userdata('datedataval');
    }
    else {
        $dateval1=date("Y-m-d");
    }
    $holdpbooking_data =json_decode($_REQUEST['holdbooking'],true);


    $parameter11 = array('act_mode' => 's_holddatefrontadd',
        'Param1' => $holdpbooking_data['noofpersons'],
        'Param2' => $holdpbooking_data['packagetime'],
        'Param3' => $dateval1,
        'Param4' => $this->session->userdata('skiindiauserid'),
        'Param5' => '',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',

    );

    $response['holddatestatus'] = $this->supper_admin->call_procedure('proc_holddate', $parameter11);

}

    public function releasepbooking()
    {
        if( $this->session->userdata('datedataval')!='') {
            $dateval1=  $this->session->userdata('datedataval');
        }
        else {
            $dateval1=date("Y-m-d");
        }
        $holdpbooking_data =json_decode($_REQUEST['holdbooking'],true);

        $parameter11 = array('act_mode' => 's_releasefrontadd',
            'Param1' => $holdpbooking_data['noofpersons'],
            'Param2' => $holdpbooking_data['packagetime'],
            'Param3' => $dateval1,
            'Param4' => $this->session->userdata('skiindiauserid'),
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',

        );

        $response['holddatestatus'] = $this->supper_admin->call_procedure('proc_holddate', $parameter11);

    }



}//end of class
?>

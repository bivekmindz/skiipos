</!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login</title>
    <script src="assets/frontend/js/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/jquery.mobile-1.2.0.min.css">
</head>

<body>

<style>

    html { height: 100% }
    ::-moz-selection { background: #fe57a1; color: #fff; text-shadow: none; }
    ::selection { background: #fe57a1; color: #fff; text-shadow: none; }
    body { background-image: radial-gradient( cover, rgba(92,100,111,1) 0%,rgba(31,35,40,1) 100%) }
    .login {
        background: #eceeee;
        border: 1px solid #42464b;
        border-radius: 6px;
        height: 305px;
        margin: auto 0;
        width: 302px;
    }
    .login h1 {
        background-image: linear-gradient(top, #f1f3f3, #d4dae0);
        border-bottom: 1px solid #a6abaf;
        border-radius: 6px 6px 0 0;
        box-sizing: border-box;
        color: #727678;
        display: block;
        height: 43px;
        font: 600 14px/1 'Open Sans', sans-serif;
        padding-top: 14px;
        margin: 0;
        text-align: center;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.2), 0 1px 0 #fff;
    }
    input[type="password"], input[type="text"] {

        border: 1px solid #a1a3a3;
        border-radius: 4px;
        box-shadow: 0 1px #fff;
        box-sizing: border-box;
        color: #696969;
        height: 39px;
        margin: 36px 0 0 29px;
        padding-left: 37px;
        transition: box-shadow 0.3s;
        width: 240px;
    }
    input[type="password"]:focus, input[type="text"]:focus {
        box-shadow: 0 0 4px 1px rgba(55, 166, 155, 0.3);
        outline: 0;
    }
    .show-password {
        display: block;
        height: 16px;
        margin: 26px 0 0 28px;
        width: 87px;
    }
    input[type="checkbox"] {
        cursor: pointer;
        height: 16px;
        opacity: 0;
        position: relative;
        width: 64px;
    }
    input[type="checkbox"]:checked {
        left: 29px;
        width: 58px;
    }
    .toggle {
        display: block;
        height: 16px;
        margin-top: -20px;
        width: 87px;
        z-index: -1;
    }
    input[type="checkbox"]:checked + .toggle { background-position: 0 -16px }
    .forgot {
        color: #7f7f7f;
        display: inline-block;
        float: right;
        font: 12px/1 sans-serif;
        left: -19px;
        position: relative;
        text-decoration: none;
        top: 5px;
        transition: color .4s;
    }
    .forgot:hover { color: #3b3b3b }
    input[type="submit"] {
        width:240px;
        height:35px;
        display:block;
        font-family:Arial, "Helvetica", sans-serif;
        font-size:16px;
        font-weight:bold;
        color:#fff;
        text-decoration:none;
        text-transform:uppercase;
        text-align:center;
        text-shadow:1px 1px 0px #37a69b;
        padding-top:6px;
        margin: 55px 0 0 29px;
        position:relative;
        cursor:pointer;
        border: none;
        background-color: #37a69b;
        background-image: linear-gradient(top,#3db0a6,#3111);
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius:5px;
        box-shadow: inset 0px 1px 0px #2ab7ec, 0px 5px 0px 0px #497a78, 0px 10px 5px #999;
    }

    .shadow {
        background: #000;
        border-radius: 12px 12px 4px 4px;
        box-shadow: 0 0 20px 10px #000;
        height: 12px;
        margin: 30px auto;
        opacity: 0.2;
        width: 270px;
    }


    input[type="submit"]:active {
        top:3px;
        box-shadow: inset 0px 1px 0px #2ab7ec, 0px 2px 0px 0px #31524d, 0px 5px 3px #999;
    }

    .text-login{
        font-weight: 700;
        margin-left:105px;
        color:#37a69b;
        font-size:26px;
    }
    .login-page-div{
        float:left;
        margin-left:50%;
        transform:translateX(-50%);
        padding-top:10%;
        height: auto;


    }
    .text-error{
        float:left;
        width:100%;
        text-align:center;
        padding-top:10px;
        color:#FF0000;
    }
    #n_keypad{
    	width:300px;
    	padding: 8px 8px;
    	background: #fff;
    	position: absolute;
    	z-index: 9999;

    }
     #n_keypad td{
     	text-align:center;
     }
     #n_keypad td a {
    padding: 8px 8px;
    display: block;
    background: #565656;
    color: #fff;
    text-shadow: none;
    font-size: 18px;
}

#n_keypadpassword{
      width:300px;
      padding: 8px 8px;
      background: #fff;
      position: absolute;
      z-index: 9999;

    }
     #n_keypadpassword td{
      text-align:center;
     }
     #n_keypadpassword td a {
    padding: 8px 8px;
    display: block;
    background: #565656;
    color: #fff;
    text-shadow: none;
    font-size: 18px;
}
</style>


<form method="post">
<div class="login-page-div">

    <div class="login">
        <label class="text-login"> Login</label>

        <label class="text-error"> <?php echo $msg; ?></label>
        <input type="text" name="emailmob"  placeholder="Enter Your User Id" required
               title="The input is not a valid User Id"    autocomplete="user-id"  maxlength="50"  readonly="readonly" id="myInput" class="input_key"/>
               <span class="emailid"></span>
               <div>

               <input type="submit" value="Next" id="nextid" name="Next" >


         </div>
        <input type="password"  name="pwd"   placeholder="Enter Your Password"
               required title="The input is not a valid Password"  autocomplete="new-password" id="myInputpassword"  maxlength="35" min="6"    class="input_key" >
         <span class="pass"></span>
        <table class="ui-bar-a" id="n_keypad" style="display: none; -khtml-user-select: none;">
            <tr>
                <td><a data-role="button" data-theme="b" class="numero">7</a></td>
                <td><a data-role="button" data-theme="b" class="numero">8</a></td>
                <td><a data-role="button" data-theme="b" class="numero">9</a></td>
                <td><a data-role="button" data-theme="e" class="del">Del</a></td>
            </tr>
            <tr>
                <td><a data-role="button" data-theme="b" class="numero">4</a></td>
                <td><a data-role="button" data-theme="b" class="numero">5</a></td>
                <td><a data-role="button" data-theme="b" class="numero">6</a></td>
                <td><a data-role="button" data-theme="e" class="clear">Clear</a></td>
            </tr>
            <tr>
                <td><a data-role="button" data-theme="b" class="numero">1</a></td>
                <td><a data-role="button" data-theme="b" class="numero">2</a></td>
                <td><a data-role="button" data-theme="b" class="numero">3</a></td>
                <td><a data-role="button" data-theme="e">&nbsp;</a></td>
            </tr>
            <tr>
                <td><a data-role="button" data-theme="e" class="neg">-</a></td>
                <td><a data-role="button" data-theme="b" class="zero">0</a></td>
                <td><a data-role="button" data-theme="e" class="pos">+</a></td>
                <td><a data-role="button" data-theme="e" class="done">Done</a></td>
            </tr>
        </table>

<table class="ui-bar-a" id="n_keypadpassword" style="display: none; -khtml-user-select: none;">
            <tr>
                <td><a data-role="button" data-theme="b" class="numero2">7</a></td>
                <td><a data-role="button" data-theme="b" class="numero2">8</a></td>
                <td><a data-role="button" data-theme="b" class="numero2">9</a></td>
                <td><a data-role="button" data-theme="e" class="del2">Del</a></td>
            </tr>
            <tr>
                <td><a data-role="button" data-theme="b" class="numero2">4</a></td>
                <td><a data-role="button" data-theme="b" class="numero2">5</a></td>
                <td><a data-role="button" data-theme="b" class="numero2">6</a></td>
                <td><a data-role="button" data-theme="e" class="clear2">Clear</a></td>
            </tr>
            <tr>
                <td><a data-role="button" data-theme="b" class="numero2">1</a></td>
                <td><a data-role="button" data-theme="b" class="numero2">2</a></td>
                <td><a data-role="button" data-theme="b" class="numero2">3</a></td>
                <td><a data-role="button" data-theme="e">&nbsp;</a></td>
            </tr>
            <tr>
                <td><a data-role="button" data-theme="e" class="neg2">-</a></td>
                <td><a data-role="button" data-theme="b" class="zero2">0</a></td>
                <td><a data-role="button" data-theme="e" class="pos2">+</a></td>
                <td><a data-role="button" data-theme="e" class="done2">Done</a></td>
            </tr>
        </table>
<input type="submit" value="Login" id="loginid" name="submit" >
    </div>
    <div class="shadow"></div>
</div>
</form>
</body>
</html>
<script>
if (navigator.userAgent.toLowerCase().indexOf('chrome') >= 0) {
    setTimeout(function () {
        document.getElementById('myInputpassword').autocomplete = 'off';
    }, 1);
}
</script>
  <script src="assets/frontend/js/jquery.mobile-1.2.0.min.js"></script>
     <script type="text/javascript">
     $('.text-error').delay(3000).fadeOut();
          $('#myInputpassword').hide();
          $('#loginid').hide();
          $('#nextid').click(function(){
          if($('#myInput').val()){
            $(".emailid").html(" ").css({ 'color': 'red' });
             $('#myInputpassword').show();
             $('#loginid').show();
             $('#nextid').hide();
             $('#myInput').hide();
             $('#n_keypad').hide();
         }else{
             $(".emailid").html("Please enter email id.").css({ 'color': 'red' });
             return false;
           }
        });
       $('#loginid').click(function(){
           if($('#myInputpassword').val()=="")
            {
             $(".pass").html("Please enter password.").css({ 'color': 'red' });
             return false;
           } else{
             $(".pass").html(" ").css({ 'color': 'red' });
           }
        });

        $(document).ready(function(){
      $('#myInput').click(function(){
          $('#n_keypad').fadeToggle('fast');
      });

        $('#myInputpassword').click(function(){
          $('#n_keypadpassword').fadeToggle('fast');
      });

      $('.done').click(function(){
          $('#n_keypad').hide('fast');
      });

       $('.done2').click(function(){
          $('#n_keypadpassword').hide('fast');
      });
      $('.numero').click(function(){
        if (!isNaN($('#myInput').val())) {
           if (parseInt($('#myInput').val()) == 0) {
             $('#myInput').val($(this).text());
           } else {
             $('#myInput').val($('#myInput').val() + $(this).text());
           }
        }
      });

      $('.numero2').click(function(){
        if (!isNaN($('#myInputpassword').val())) {
           if (parseInt($('#myInputpassword').val()) == 0) {
             $('#myInputpassword').val($(this).text());
           } else {
             $('#myInputpassword').val($('#myInputpassword').val() + $(this).text());
           }
        }
      });


      $('.neg').click(function(){
          if (!isNaN($('#myInput').val()) && $('#myInput').val().length > 0) {
            if (parseInt($('#myInput').val()) > 0) {
              $('#myInput').val(parseInt($('#myInput').val()) - 1);
            }
          }
      });

      $('.neg2').click(function(){
          if (!isNaN($('#myInputpassword').val()) && $('#myInputpassword').val().length > 0) {
            if (parseInt($('#myInputpassword').val()) > 0) {
              $('#myInputpassword').val(parseInt($('#myInputpassword').val()) - 1);
            }
          }
      });

      $('.pos').click(function(){
          if (!isNaN($('#myInput').val()) && $('#myInput').val().length > 0) {
            $('#myInput').val(parseInt($('#myInput').val()) + 1);
          }
      });

            $('.pos2').click(function(){
          if (!isNaN($('#myInputpassword').val()) && $('#myInputpassword').val().length > 0) {
            $('#myInputpassword').val(parseInt($('#myInputpassword').val()) + 1);
          }
      });


      $('.del').click(function(){
          $('#myInput').val($('#myInput').val().substring(0,$('#myInputpassword').val().length - 1));
      });

        $('.del2').click(function(){
          $('#myInputpassword').val($('#myInputpassword').val().substring(0,$('#myInputpassword').val().length - 1));
      });


      $('.clear').click(function(){
          $('#myInput').val('');
      });

      $('.clear2').click(function(){
          $('#myInputpassword').val('');
      });

      $('.zero').click(function(){
        if (!isNaN($('#myInput').val())) {
          if (parseInt($('#myInput').val()) != 0) {
            $('#myInput').val($('#myInput').val() + $(this).text());
          }
        }
      });

         $('.zero2').click(function(){
        if (!isNaN($('#myInputpassword').val())) {
          if (parseInt($('#myInputpassword').val()) != 0) {
            $('#myInputpassword').val($('#myInputpassword').val() + $(this).text());
          }
        }
      });


    });</script>


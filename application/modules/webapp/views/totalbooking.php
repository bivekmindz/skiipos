<!DOCTYPE html>
<html class="loading">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/bootstrap.min.css">
    <link href="assets/frontend/css/jquery-ui.css" rel="stylesheet"/>
    <script type="text/javascript" src="assets/frontend/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/frontend/js/bootstrap.min.js"></script>

    <script SRC="assets/frontend/js/bootstrap-datepickerindex.js"></script>
    <script src="assets/frontend/js/jquery-ui.js"></script>


    <script src="assets/frontend/js/angular.min.js"></script>
    <script src="assets/frontend/js/angular-route.js"></script>
    <script language="javascript" src="assets/frontend/js/angular-animate.js"></script>
    <script language="javascript" src="assets/frontend/js/ui-bootstrap-tpls-0.14.3.js"></script>
    <!--<style>
        html {
            -webkit-transition: background-color 1s;
            transition: background-color 1s;
        }
        html, body {
            /* For the loading indicator to be vertically centered ensure */
            /* the html and body elements take up the full viewport */
            min-height: 100%;
        }
        html.loading {
            /* Replace #333 with the background-color of your choice */
            /* Replace loading.gif with the loading image of your choice */
            background: #333 url('assets/admin/images/pageLoader.gif') no-repeat 50% 50%;

            /* Ensures that the transition only runs in one direction */
            -webkit-transition: background-color 0;
            transition: background-color 0;
        }
        body {
            -webkit-transition: opacity 1s ease-in;
            transition: opacity 1s ease-in;
        }
        html.loading body {
            /* Make the contents of the body opaque during loading */
            opacity: 0;

            /* Ensures that the transition only runs in one direction */
            -webkit-transition: opacity 0;
            transition: opacity 0;
        }
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('assets/admin/images/pageLoader.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }

        html {
            -webkit-transition: background-color 1s;
            transition: background-color 1s;
        }
        html, body { min-height: 100%; }
        html.loading {
            background: #333 url('https://code.jquery.com/mobile/1.3.1/images/ajax-loader.gif') no-repeat 50% 50%;
            -webkit-transition: background-color 0;
            transition: background-color 0;
        }
        body {
            -webkit-transition: opacity 1s ease-in;
            transition: opacity 1s ease-in;
        }
        html.loading body {
            opacity: 0;
            -webkit-transition: opacity 0;
            transition: opacity 0;
        }
        button {
            background: #00A3FF;
            color: white;
            padding: 0.2em 0.5em;
            font-size: 1.5em;
        }
    </style>
    <script>
    var html = document.getElementsByTagName('html')[0];
    var removeLoading = function() {
    // In a production application you would remove the loading class when your
    // application is initialized and ready to go.  Here we just artificially wait
    // 3 seconds before removing the class.
    setTimeout(function() {
    html.className = html.className.replace(/loading/, '');
    }, 200);
    };
    removeLoading();
    </script>-->

</head>

<body class="inner_body" ng-app="myApp" ng-controller="TotalCntrls">


<?php if ($select_user->user_lock == '1') { ?>


    <div id="myNav" class="overlay">
        <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
       -->
        <form method="post" action="" autocomplete="off">
            <div class="pop_login">

                <div class="login">

                    <label class="text-error"> <?php echo $msg; ?></label>
                    <div class="pos_input_box">
                        <input type="text" autocomplete="off" name="emailmob" placeholder="Enter Your User Id" required
                               title="The input is not a valid User Id" maxlength="50">
                    </div>
                    <div class="pos_input_box">
                        <input type="password" autocomplete="new-password" name="pwd" class="form-control-1"
                               placeholder="Enter Your Password" required
                               title="The input is not a valid Password" maxlength="35" min="6">
                    </div>
                    <div class="pos_input_box">
                        <input type="submit" value="Login" name="submit">
                    </div>
                </div>
                <div class="shadow"></div>


            </div>
        </form>

    </div>
    <script>
        document.getElementById("myNav").style.height = "100%";

        function openNav() {
        }

        function closeNav() {
            document.getElementById("myNav").style.height = "0%";
        }
    </script>
<?php } ?>
<div ng-view>
</div>


<script src="assets/frontend/js/app/app.js"></script>

<script src="assets/frontend/js/app/totalval-router.js"></script>
<script src="assets/frontend/js/app/controllers/TotalCntrls.js"></script>


</!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/bootstrap.min.css">
    <script type="text/javascript" src="assets/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/frontend/js/jquery-3.2.1.min.js"></script>
    <!--    <script language="javascript" src="assets/frontend/js/angular.js"></script>-->
    <script src="assets/frontend/js/angular.min.js"></script>
    <script src="assets/frontend/js/angular-route.js"></script>
    <script language="javascript" src="assets/frontend/js/angular-animate.js"></script>
    <script language="javascript" src="assets/frontend/js/ui-bootstrap-tpls-0.14.3.js"></script>
    <script SRC="assets/frontend/js/jquery-1.11.2.min.js"></script>
    <script SRC="assets/frontend/js/common_scripts_min.js"></script>
    <script SRC="assets/frontend/js/bootstrap-datepickerindex.js"></script>

</head>
<body class="inner_body" ng-app="myApp" ng-controller="AdvPosCntrl">
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<div ng-view>
</div>
<script src="assets/frontend/js/app/app.js"></script>
<script src="assets/frontend/js/app/advconfig-router.js"></script>
<script src="assets/frontend/js/app/controllers/AdvPosCntrl.js"></script>
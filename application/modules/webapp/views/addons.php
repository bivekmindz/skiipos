<!DOCTYPE html>
<html class="loading">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/frontend/css/bootstrap.min.css">
    <link href="assets/frontend/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="assets/frontend/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/frontend/js/bootstrap.min.js"></script>

    <script SRC="assets/frontend/js/bootstrap-datepickerindex.js"></script>
    <script src="assets/frontend/js/jquery-ui.js"></script>


    <script src="assets/frontend/js/angular.min.js"></script>
    <script src="assets/frontend/js/angular-route.js"></script>
    <script language="javascript" src="assets/frontend/js/angular-animate.js"></script>
    <script language="javascript" src="assets/frontend/js/ui-bootstrap-tpls-0.14.3.js"></script>




</head>

<body class="inner_body" ng-app="myApp" ng-controller="AddonsCntrl">

<div ng-view>
</div>




<script src="assets/frontend/js/app/app.js"></script>

<script src="assets/frontend/js/app/config-router.js"></script>
<script src="assets/frontend/js/app/controllers/PosCntrl.js"></script>
<script src="assets/frontend/js/app/controllers/AddonsCntrl.js"></script>
<script src="assets/frontend/js/app/controllers/TotalCntrls.js"></script>


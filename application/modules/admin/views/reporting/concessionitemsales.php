

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Ticket concessionitemsales report</h2>
                    </div>
                    
 <div class="parent_table">
    <div class="table_box">
    <table class="table table-bordered">
    <thead>	
    <tr>
        <th></th>
        <th>UOM</th>
        <th>Quantity</th>
        <th>Discount</th>
        <th>Net</th>
        <th>Tax</th>
        <th>Sales Value</th>
        <th>Cost (Excl. Tax)</th>
        <th>Margin (On Net)</th>
        <th>% Of Total Sales</th>
    </tr>
    </thead>
    
    <tbody>
        <tr>
            <td colspan="10"><strong>Cinema: Cinema Operator</strong> <br />Location: Cinema Stores</td>
        </tr>
        <tr>
        	<td colspan="10"><strong>Cold Beverages</strong></td>
        </tr>
        
        <tr>
        	<td>Water Bottle</td>
            <td>Pcs</td>
            <td>1.00</td>
            <td>0.00</td>
            <td>28.58</td>
            <td>1.42</td>
            <td>30.00</td>
            <td>12.86</td>
            <td>15.72</td>
            <td>4.11 %</td>
        </tr>
        <tr>
        	<td colspan="2"></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
            <td><strong>12.86</strong></td>
            <td><strong>15.72</strong></td>
            <td><strong>4.11 %</strong></td>
        </tr>
        
        <tr>
        	<td colspan="10"><strong>Food</strong></td>
        </tr>
        
        <tr>
        	<td>Water Bottle</td>
            <td>Pcs</td>
            <td>1.00</td>
            <td>0.00</td>
            <td>28.58</td>
            <td>1.42</td>
            <td>30.00</td>
            <td>12.86</td>
            <td>15.72</td>
            <td>4.11 %</td>
        </tr>
        <tr>
        	<td colspan="2"></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
            <td><strong>12.86</strong></td>
            <td><strong>15.72</strong></td>
            <td><strong>4.11 %</strong></td>
        </tr>
        
          <tr>
        	<td colspan="10"><strong>Hot Beverages</strong></td>
        </tr>
        <tr>
        	<td>Hot Chocolate</td>
            <td>Pcs</td>
            <td>1.00</td>
            <td>0.00</td>
            <td>28.58</td>
            <td>1.42</td>
            <td>30.00</td>
            <td>12.86</td>
            <td>15.72</td>
            <td>4.11 %</td>
        </tr>
        <tr>
        	<td>Tea</td>
            <td>Pcs</td>
            <td>1.00</td>
            <td>0.00</td>
            <td>28.58</td>
            <td>1.42</td>
            <td>30.00</td>
            <td>12.86</td>
            <td>15.72</td>
            <td>4.11 %</td>
        </tr>
        <tr>
        	<td colspan="2"></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
            <td><strong>12.86</strong></td>
            <td><strong>15.72</strong></td>
            <td><strong>4.11 %</strong></td>
        </tr>
        <tr>
        	<td colspan="2"><strong>Total for Location: Cinema Stores</strong></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
            <td><strong>12.86</strong></td>
            <td><strong>15.72</strong></td>
            <td><strong>4.11 %</strong></td>
        </tr>
        <tr>
        	<td colspan="2"><strong>Total for Cinema: Cinema Operator</strong></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
            <td><strong>12.86</strong></td>
            <td><strong>15.72</strong></td>
            <td><strong>4.11 %</strong></td>
        </tr>
         <tr>
        	<td colspan="2"><strong>GRAND TOTAL</strong></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
            <td><strong>12.86</strong></td>
            <td><strong>15.72</strong></td>
            <td><strong>4.11 %</strong></td>
        </tr>
        
        
    </tbody>
    
    </table>
                     
    </div>         
</div> 
                    
                    
                    
                    
                    
                    
                    
                    
        </div>
    </div>
</div>



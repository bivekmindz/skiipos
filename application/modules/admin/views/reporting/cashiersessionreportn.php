<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo admin_url(); ?>js/moment.js"></script>

<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
      rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $(function () {
    });
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    .input-group-addon {
        position: absolute;
        left: 0;
        width: 100% !important;
        padding: 12px 12px !important;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: transparent !important;
        border: none !important;
        border-radius: 4px;
        top: 0px;
    }

    .input-group-addon .glyphicon-time {
        float: right;
    }

    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    /*.tab_ble{ width:100%; float:left;}
    .tab_ble table{ width:100%; float:left;}
    .tab_ble table tr{ width:100%; float:left;}*/
    .tab_ble table tr th {
        /*padding: 5px 35px 15px 35px;*/
        text-align: center;
    }

    .tab_ble table tr td {
        /*border: 1px solid #CCC;*/
        text-align: center;

        /*min-width: 100px;*/
    }

    .lbl_box {
        float: left;
        margin-right: 10px;
    }

    @media screen and (min-width: 320px) and (max-width: 680px) {

        .lbl_box {
            width: 100%;
            float: left;
            margin-right: 10px;
        }

    }

    .checkbox_lbl {
        display: none;
        z-index: -9999;
    }

    .checkbox_lbl + label {
        position: relative;
        padding-left: 25px;
        font-weight: normal;
        cursor: pointer;
    }

    .checkbox_lbl + label:before {
        position: absolute;
        content: "";
        width: 19px;
        height: 19px;
        border: solid 1px #abaaaa;
        left: 0;
    }

    .checkbox_lbl + label:after {
        position: absolute;
        content: "";
        width: 10px;
        height: 6px;
        border-left: solid 2px #444;
        border-bottom: solid 2px #444;
        left: 5px;
        transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        transition: 0.3s;
        -o-transition: 0.3s;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        top: 5px;
        visibility: hidden;
        opacity: 0;
    }

    .checkbox_lbl:checked + label:after {
        visibility: visible;
        opacity: 1;
    }

    .page_contant {
        margin-top: 0px;
    }

    .page_box {
        box-shadow: none;
    }

    /* .tbl_overflow{
         float:left;
         width:100%;
         overflow-x: auto;
         padding-bottom: 10px;
     }*/
    #advance_search_d {
        display: none;
        margin-bottom: 15px;
        float: left;
        width: 100%;
    }

    .advance_btn {
        float: left;
        background: #0f71c3;
        color: #fff;
        padding: 10px 15px;
        cursor: pointer;
    }

    .table > thead > tr > th {
        vertical-align: middle !important;

    }

    #container {
        width: 100%;
    }

    #left {
        float: left
    }

    #right {
        float: right
    }

    #center {
        margin: 0 auto;
        width: 100px;
        float: left;
        padding: 1px 17px;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">


                    <div class="page_name">

                        <h2>Cashier Session Reconciliation</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">


                                <div id="container">
                                    <div id="left">
                                        <div class="advance_btn">Advance Search</div>
                                    </div>

                                   

                                    <div id="right">


                                        


                                        <button id="btnExport" class="btn btn-primary">Download EXCEL</button>
                                    </div>
                                </div>


                            </div>
                        </div>
                       


                        <div class="col-md-12">
                            <div class="tab_ble">


                                <div id="advance_search_d">
                                    <form method="post"
                                          action="<?= base_url('admin/reportingcontrol/cashiersessionreport') ?>">




                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date_ticket" placeholder="Visit Date"
                                                           name="filter_date_ticket" required="required">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="sep_box">
                                            <div class="col-md-12">
                                                <div class="submit_tbl">
                                                    <input type="submit" value="Search" class="btn_button sub_btn">
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                             

                                <form class="form" >

                                    <div id="table_Excel">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                <tbody><tr>
                                                    <td height="63" align="center" style="font-size:24px;" colspan="6">SKI INDIA - Cashier Session Reconciliation</td>
                                                </tr>
                                                <tr>
                                                    <td height="43" align="center" style="font-size:18px;" colspan="6">Brand of Chiliad Procons</td>
                                                </tr>
                                                <tr>
                                                    <td height="43" style="font-size:18px;" colspan="6">&nbsp;</td>
                                                </tr>

                                            </tbody></table>
                                     <?php    foreach ($vieww_cashiersessionreport as $v) {
                                   if($v->packagename!='') {                          

                                                                ?>
                                        <div class="parent_table">
    <div class="table_box">
        <div class="time_data">
    <div class="col-md-12">
        <span style="padding-right: 4px;">User :</span>
        <span ><?php echo $v->user_firstname; ?></span>
        <span style="padding-right: 4px;">Business Day :</span>
        <span ><?php  echo $currentdate;?></span>
        <span style="padding-right: 4px;">Detail :</span>
        <span>Detailed</span>
    </div>
    
 </div>
    <table class="table table-bordered">
    <thead>
    <tr>
        <th  style="text-align:center;"><strong>User:</strong></th>
        <th  style="text-align:center;"><?php echo $v->user_firstname; ?></th>
       
        <th colspan="6" style="text-align:center;"><strong>Point of Sale Session Number:</strong></th>
         <th colspan="2" style="text-align:center;"><?php echo $v->user_username; ?></th>
    </tr>       
    
    <tr>
        <th  style="text-align:center;"><strong>User Number:</strong></th>
        <th  style="text-align:center;"><?php echo $v->user_username; ?></th>
        
        <th colspan="4" style="text-align:center;"><strong>Business Day:</strong></th>
         <th colspan="4" style="text-align:center;"><?php  echo $currentdate;?></th>
        
        
    </tr>
    </thead>
    
    <tbody>
        <tr>
            <td colspan="2" align="left"></td>
            <td colspan="2" align="center">Sales</td>
            <td colspan="2" align="center">Cancelled</td>
           
            <td colspan="2" align="center">Refund</td>
           
            <td colspan="2" align="center">Gross</td>
        </tr>

       <tr>
            <td colspan="2" align="right">Price</td>
            <td align="center">Qty</td>
            <td align="center">Value</td>
            <td align="center">Qty</td>
            <td align="center">Value</td>
            <td align="center">Qty</td>
          
            <td align="center">Value</td>
            <td align="center">Qty</td>
          
            <td align="center">Value</td>
        </tr>

       
<?php $packname=explode(",",$v->packagename); 
$itemp=explode(",",$v->itemprice); 
$item=explode(",",$v->itemqty); 
$productpr=explode(",",$v->productprice); 
$cancelledqtye=explode(",",$v->cancelledqty); 
$cancelledpricee=explode(",",$v->cancelledprice); 

$refundqtye=explode(",",$v->refundqty); 
$refundpricee=explode(",",$v->refundprice); 

$saleqty=0;
$cancelqty=0;
$refundqty=0;
$grossqty=0;

$productp=0;
$cancelproductp=0;
$refundproductp=0;
$grossproductp=0;
for($i=0;$i<count( $packname);$i++)
{ 
?>
      

         <tr>
            <td align="left"><?php  echo($packname[$i]);  ?></td>
            <td align="right"><?php  echo($itemp[$i]);  ?></td>
            <td align="center"><?php  echo($item[$i]);  $saleqty +=$item[$i];  ?></td>
            <td align="center"><?php  echo($productpr[$i]);  $productp +=$productpr[$i];   ?></td>
            <td align="center"><?php  echo($cancelledqtye[$i]);  $cancelqty +=$cancelledqtye[$i]; ?></td>
            <td align="center"><?php  echo($cancelledpricee[$i]); $cancelproductp +=$cancelledpricee[$i]; ?></td>
            <td align="center"><?php  echo($refundqtye[$i]); $refundqty +=$refundqtye[$i]; ?></td>
            <td align="center"><?php  echo($refundpricee[$i]); $refundproductp +=$refundpricee[$i];  ?></td>
         
            <td align="center"><?php echo $grossqtyn=($item[$i]-$cancelledqtye[$i] -$refundqtye[$i]); $grossqty += ($item[$i]-$cancelledqtye[$i] -$refundqtye[$i]); ?></td>
            <td align="center"><?php echo $grossproductpn=($productpr[$i]-$cancelledpricee[$i] -$refundpricee[$i]);  $grossproductp+= ($productpr[$i]-$cancelledpricee[$i] -$refundpricee[$i]);?></td>
        </tr>
<?php } ?>
         

        <tr>
            <td align="left">Total</td>
            <td align="right"></td>
            <td align="center"><?php echo $saleqty; ?></td>
            <td align="center"><?php echo $productp; ?></td>
            <td align="center"><?php echo $cancelqty; ?></td>
            <td align="center"><?php echo $cancelproductp; ?></td>
            <td align="center"><?php echo $refundqty; ?></td>
            <td align="center"><?php echo $refundproductp; ?></td>
           
          
            <td align="center"><?php echo $grossqty; ?></td>
            <td align="center"><?php echo $grossproductp; ?></td>
        </tr>

         <!-- <tr>
            <td align="left">Total Concessions</td>
            <td align="right"></td>
            <td align="center">40</td>
            <td align="center">2,050.00</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td colspan="2" align="center"></td>
            <td align="center">40</td>
            <td align="center">2,050.00 40</td>
        </tr> -->

         <tr>
            <td colspan="9" align="left">Total Sales</td>
            <td align="center"><?php echo $grossproductp; ?></td>
        </tr>

       
       <!-- 

        <tr>
            <td colspan="10" align="left">Redemptions</td>
        </tr>

        <tr>
            <td colspan="10" align="left">Complimentary Tickets</td>
        </tr>

         <tr>
            <td colspan="3" align="left">Complimentary Tickets</td>
            <td colspan="5" align="left">Number of Transactions</td>
            <td colspan="5" align="left">Average Transaction Value</td>
        </tr>

        <tr>
            <td colspan="3" align="left"><strong>Concessions</strong></td>
            <td colspan="5" align="left">27</td>
            <td colspan="5" align="left">75.93</td>
        </tr>

        <tr>
            <td colspan="3" align="left"><strong>Total</strong></td>
            <td colspan="5" align="left">27</td>
            <td colspan="5" align="left">75.93</td>
        </tr>

         <tr>
            <td colspan="3" align="left"><strong>Final Cash Drawer Balance</strong></td>
            <td colspan="5" align="left">Value</td>
            <td colspan="5" align="left">Quantity</td>
        </tr>


         <tr>
            <td colspan="3" align="left"></td>
            <td colspan="5" align="left">Calculated</td>
            <td colspan="5" align="left">Calculated</td>
        </tr>

         <tr>
            <td colspan="3" align="left"><strong>Receipts</strong></td>
            <td colspan="5" align="left">2,050.00</td>
            <td colspan="5" align="left">0</td>
        </tr>

        <tr>
            <td colspan="13" align="left"><strong>Number of Occurrences</strong></td>
        </tr>

        <tr>
            <td colspan="3" align="left">Deletes:</td>
             <td colspan="2" align="left">5</td>
              <td colspan="2" align="left">Aborts:</td>
               <td colspan="2" align="left">0</td>
               <td colspan="2" align="left">Late Refunds:</td>
               <td colspan="2" align="left">0</td>
        </tr>

        <tr>
            <td colspan="3" align="left">Manual Cash Drawer Open:</td>
             <td colspan="2" align="left">0</td>
              <td colspan="2" align="left"></td>
               <td colspan="2" align="left"></td>
               <td colspan="2" align="left"></td>
               <td colspan="2" align="left"></td>
        </tr> -->

         <tr>
              <td colspan="5" align="right">Logon Status</td>
               <td colspan="3" align="center">Date</td>
               <td colspan="3" align="center">Time</td>
        </tr>
<?php $logedons=explode(",",$v->logedon); 
for($i=0;$i<count( $logedons);$i++)
{ 
?>
         <tr>
              <td colspan="5" align="right">Log Off</td>
               <td colspan="3" align="center"><?php  echo $currentdate;?></td>
               <td colspan="3" align="center"><?php  echo($logedons[$i]);  ?></td>
        </tr>
        
         
<?php } ?>
          <tr>
              <td colspan="6" align="left" style="padding:20px 8px;">Manager to sign to confirm funds declared are correct</td>
               <td colspan="7" align="left" style="padding:20px 8px;">Cashier to sign to confirm funds declared are correct</td>
        </tr>
 
    </tbody>
    
    </table>
                     
    </div>         
</div>     
        </div>
        
          <?php } } ?>                          </div>  </div>
                            </form>  </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script>

    function funct_change_status(a,b) {
        if(b == 2) {
            $(".ticket" + a).attr('disabled', true);
        }
        $.ajax({
            url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
            type: 'POST',
            data: {'pacorderid': a,'op_ticket_print_status':b},
            dataType: "JSON",
            success: function(data){


//console.log(data.s[0].ticket_status);
                if(data.s[0].ticket_status == 2)
                {
                    a.attr('disabled',true);

                }
//console.log(data1);
            }
        });
//var b = $(this).val();
// alert(b);return false;

    }

    /*    $(function () {
    $("#op_ticket_print_status").change(function () {
    alert("hello"); return false;
    var a = $(this);
    if($("#op_ticket_print_status").val() == 2)
    {
    a.attr('disabled',true);

    }
    //return false;
    $.ajax({
    url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
    type: 'POST',
    data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
    dataType: "JSON",
    success: function(data){


    //console.log(data.s[0].ticket_status);
        if(data.s[0].ticket_status == 2)
        {
            a.attr('disabled',true);

        };
       //console.log(data1);
    }
    });
    });
    });*/

    function ticket_print(a) {
//alert(a);
        window.print();

    }

    function update_com(a, b) {
//alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display', 'block');
        $("#add_com").css('display', 'none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display', 'block');
        $("#update_com").css('display', 'none');
        return false;
    }


</script>



<script type="text/javascript" src="<?= base_url('assets/admin/js/jquery-1.11.3.min.js') ?>"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?= base_url('assets/admin/js/bootstrap-datepicker.min.js') ?>"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        $(".advance_btn").click(function(){
            $("#advance_search_d").slideToggle();
        });

    })
</script>

<script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo admin_url();?>js/jquery.min.js"></script>
<script type="text/javascript"

        src="<?php echo admin_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript"

        src="<?php echo admin_url();?>js/bootstrap.min.js"></script>

<script>
    $(document).ready(function(){


        var table = $('#table1').DataTable();
        $.noConflict();
        var date_input=$('input[name="filter_date_booking_from"],input[name="filter_date_booking_to"],input[name="filter_date_printed_from"],input[name="filter_date_printed_to"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        });


        var date_input=$('input[name="filter_date_session_from"],input[name="filter_date_session_to"],input[name="filter_date_ticket"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        });
        /*$('#filter_date1').datepicker('setDate', 'today');
        $('#filter_date2').datepicker('setDate', 'today');
        $('#filter_date3').datepicker('setDate', 'today');
        $('#filter_date4').datepicker('setDate', 'today');*/
    });
    //$('#table1').dataTable();
</script>







<script src="<?php echo admin_url();?>js/jspdf.min.js"></script>
<script>
    (function () {
        var
            form = $('.form'),
            cache_width = form.width(),
            a4 = [595.28, 841.89]; // for a4 size paper width and height

        $('#create_pdf').on('click', function () {
            $('body').scrollTop(0);
            createPDF();
        });
//create pdf
        function createPDF() {
            getCanvas().then(function (canvas) {
                var
                    img = canvas.toDataURL("image/png"),
                    doc = new jsPDF({
                        unit: 'px',
                        format: 'a4'
                    });
                doc.addImage(img, 'JPEG', 20, 20);
                doc.save('Cashier-Session-Reconciliation-Report.pdf');
                form.width(cache_width);
            });
        }

// create canvas object
        function getCanvas() {
            form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
            return html2canvas(form, {
                imageTimeout: 2000,
                removeContainer: true
            });
        }

    }());
</script>
<script>
    /*
 * jQuery helper plugin for examples and tests
 */
    (function ($) {
        $.fn.html2canvas = function (options) {
            var date = new Date(),
                $message = null,
                timeoutTimer = false,
                timer = date.getTime();
            html2canvas.logging = options && options.logging;
            html2canvas.Preload(this[0], $.extend({
                complete: function (images) {
                    var queue = html2canvas.Parse(this[0], images, options),
                        $canvas = $(html2canvas.Renderer(queue, options)),
                        finishTime = new Date();

                    $canvas.css({ position: 'absolute', left: 0, top: 0 }).appendTo(document.body);
                    $canvas.siblings().toggle();

                    $(window).click(function () {
                        if (!$canvas.is(':visible')) {
                            $canvas.toggle().siblings().toggle();
                            throwMessage("Canvas Render visible");
                        } else {
                            $canvas.siblings().toggle();
                            $canvas.toggle();
                            throwMessage("Canvas Render hidden");
                        }
                    });
                    throwMessage('Screenshot created in ' + ((finishTime.getTime() - timer) / 1000) + " seconds<br />", 4000);
                }
            }, options));

            function throwMessage(msg, duration) {
                window.clearTimeout(timeoutTimer);
                timeoutTimer = window.setTimeout(function () {
                    $message.fadeOut(function () {
                        $message.remove();
                    });
                }, duration || 2000);
                if ($message)
                    $message.remove();
                $message = $('<div ></div>').html(msg).css({
                    margin: 0,
                    padding: 10,
                    background: "#000",
                    opacity: 0.7,
                    position: "fixed",
                    top: 10,
                    right: 10,
                    fontFamily: 'Tahoma',
                    color: '#fff',
                    fontSize: 12,
                    borderRadius: 12,
                    width: 'auto',
                    height: 'auto',
                    textAlign: 'center',
                    border:'2px solid #d2d1d1',
                    textDecoration: 'none'

                }).hide().fadeIn().appendTo('body');
            }
        };
    })(jQuery);

</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker31,#datetimepicker41').datetimepicker({
            format: 'LT'
        });

        $('.dpxx').datetimepicker({
            format: 'LT'
        });


    });

    $(document).ready(function() {
        $("#btnExport").click(function(e) {
            e.preventDefault();

//getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_Excel');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'Cashier-Session-Reconciliation-Report_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
            a.click();
        });
        var x = $(window).height();
        var y = x-55;
        $(".page_contant").css({"height":y+"px","min-height":"auto"});
        var tp = $("#table1");
    });
</script>

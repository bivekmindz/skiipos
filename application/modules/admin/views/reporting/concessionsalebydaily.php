

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Ticket concessionsalebydaily report</h2>
                    </div>
                    

<div class="parent_table">
    <div class="table_box">
    <table class="table table-bordered">
    <thead>	
    <tr>
    	<th></th>
        <th>Cost Price</th>
        <th>Retail Price</th>
        <th>Quantity</th>
        <th>Gross Sale</th>
        <th>GST</th>
        <th>Nett</th>
        
    </tr>
    </thead>
    
    <tbody>
        <tr>
            <td colspan="7"><strong>Cinema: Cinema Operator</strong> <br />Location: Cinema Stores <br /><strong>Concessions</strong></td>
        </tr>
            
        <tr>
        	<td colspan="7"><strong>Gold Beverages</strong></td>
        </tr>
        
        <tr>
        	<td>Water Bottle</td>
            <td>13.50</td>
            <td>30.00</td>
            <td>8.00</td>
            <td>468.64</td>
            <td>102.86</td>
            <td>365.78</td>
     
        </tr>
        <tr>
        	<td colspan="3"></td>
            
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>1.42</strong></td>
            <td><strong>30.00</strong></td>
        </tr>
        
       <tr>
        	<td colspan="7"><strong>Food</strong></td>
        </tr>
        <tr>
        	<td>Food Combo</td>
            <td>50.00</td>
            <td>100.00</td>
            <td>76.00</td>
            <td>14,838.24</td>
            <td>3,619.05</td>
            <td>11,219.19</td>
        </tr>
        <tr>
        	<td>Maggi</td>
            <td>12.00</td>
            <td>50.00</td>
            <td>8.00</td>
            <td>780.96</td>
            <td>91.43</td>
            <td>689.53</td>
        </tr>
        <tr>
        	<td colspan="3"></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>17,558.19</strong></td>
        </tr>
        <tr>
        	<td colspan="3"><strong>Total for Workstation Group: Concessions</strong></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>17,558.19</strong></td>
        </tr>
        <tr>
        	<td colspan="3"><strong>Total for Location: Cinema Stores</strong></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>17,558.19</strong></td>
        </tr>
         <tr>
        	<td colspan="3"><strong>Total for Cinema: Cinema Operator</strong></td>
            <td><strong>1.00</strong></td>
            <td><strong>0.00</strong></td>
            <td><strong>28.58</strong></td>
            <td><strong>17,558.19</strong></td>
        </tr>
        
         <tr>
        	<td colspan="3"><strong>GRAND TOTAL</strong></td>
            <td><strong>140.00</strong></td>
            <td><strong>22,677.12</strong></td>
            <td><strong>5,118.93</strong></td>
            <td><strong>17,558.19</strong></td>
        </tr>
        
        
    </tbody>
    
    </table>
                     
    </div>         
</div>          
                    
                    
        </div>
    </div>
</div>



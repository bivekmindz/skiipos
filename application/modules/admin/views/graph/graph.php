


<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script type="text/javascript">
    // Load the Visualization API and the line package.
    google.charts.load('current', {'packages':['corechart']});
    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    function drawChart()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Agent');
        data.addColumn('number', 'Total order');
        data.addRows([
            <?php foreach($result as $key=>$value)  {?>
            ['<?php echo $value->agent ?>',<?php echo $value->totalorder  ?>],
            <?php  } ?>
        ]);

        var options = {
            legend: '',
            pieSliceText: 'label',
            title: 'Top Ten Orders By Users',
            is3D: true,
            width: 900,
            height: 500,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);

    }

    function validatedate()
    {
        var startdate=$('#datepicker1').val();
        var enddate=$('#datepicker2').val();
        // alert(startdate);
        var eDate = new Date(enddate);
        var sDate = new Date(startdate);
        if(startdate!= '' && startdate!= '' && sDate> eDate)
        {
            alert("Please ensure that the End Date is greater than or equal to the Start Date.");
            return false;
        }

    }

</script>

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>User Graph</h2>
                    </div>
                    <div class="page_box">

                        <div class="count">
                            <div class="total-order">
                                Total Order : <span><?php echo $countorder->totalorder;  ?></span>
                            </div>
                            <div class="total-order">
                                Total Users : <span><?php echo $countuser->totaluser;  ?></span>

                            </div>
                        </div>
                        <form id="target" method="post" action="">
                            <div class="col-md-8">
                                <div class="box12">
                                    <input type="text" placeholder="From-date" required value ="<?php echo ($_POST['datepicker1']) ? $_POST['datepicker1'] : '' ?>" name="datepicker1" id="datepicker1">
                                    <input type="text" placeholder="To-date" required value ="<?php echo ($_POST['datepicker2']) ? $_POST['datepicker2'] : '' ?>" name="datepicker2" id="datepicker2">


                                    <input type="submit" value ="Search" name="search" id="search" onclick="validatedate();" class="btn btn-primary" >
                                    <a  href="<?= base_url('admin/graphmanagement/usergraph'); ?>" class="btn btn-primary">Reset</a>
                                    <!--                                <input type="submit" value ="Reset" name="reset" id="reset" onclick="window.location.reload(true);" >-->
                                </div>
                            </div>
                        </form>
                        <div style="width:75%; float: left;min-height: 450px;">
                            <div id="piechart" style="width: 900px; height: 500px;">
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
    <style>
        .count{
            width: 25%;
            float: left;
            padding: 0 15px;
            box-sizing: border-box;
        }
        .total-order{
            font-size: 18px;
            line-height: 24px;
            color: #393939;
        }
        .total-order{
            padding: 10px 0;
            border-bottom: 1px solid #c1c1c1;
        }
        .total-order span{
            color: #1B78C7;
            font-size: 22px;
            line-height: :24px;
            font-weight: 600;
        }
    </style>

    <script>
        $(function() {
            $.noConflict();
            $("#datepicker1").datepicker({
                dateFormat: 'yy-mm-dd',
                // minDate: 0,
//            maxDate: "-1M "
            });

            $("#datepicker2").datepicker({
                dateFormat: 'yy-mm-dd',
                // minDate: 0,
                //maxDate: "-1M "
            });
        });

        //    $("#search").click(function(e) {
        //        e.preventDefault();
        //        drawChart();
        //    });
    </script>




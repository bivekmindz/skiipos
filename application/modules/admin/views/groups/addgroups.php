<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>



    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_taxes").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                group_name : "required",
                group_noofpersonfrom :{
                    "required": true,
                    "number": true
                    },
                group_noofpersonto :{
                    "required": true,
                    "number": true
                },
                packages : {
                    "required":  true
                }


            },
            // Specify validation error messages
            messages: {
                group_name: "Please enter name",
                group_noofpersonfrom :{
                    required: "Enter Number Of Person From",
                    number: "Enter Number Only"
                },
                group_noofpersonto :{
                    required: "Enter Number Of Person To",
                    number: "Enter Number Only"
                },
                packages : {
                    required: "Select Packages"
                }

            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {

                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .btne_active{    text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;}
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add Groups</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                             <li class="active"><a href="<?php  echo base_url('admin/Groups/s_group') ?>" >Groups</a></li>
                                                               </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form action="" name="add_taxes" id="add_taxes" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Name<span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="group_name" name="group_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box" >
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> No Of Person From </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="group_noofpersonfrom" name="group_noofpersonfrom" onkeyup="checkfrom();" onkeypress="return isNumberKey(event)"   >
                                                <label for="" class="from_error"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box" >
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> No Of Person To</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="group_noofpersonto" name="group_noofpersonto" onkeyup="checkto();" onkeypress="return isNumberKey(event)">
                                                <label for="" class="to_error"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Packages</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">

                                                <select name="packages">
                                                    <?php
                                                    foreach($vieww_pack as $v)
                                                    {
                                                    ?>

                                                    <option value="<?php echo $v->package_id ?>"><?php echo $v->package_name ?></option>
                                                     <?php } ?>

                                                </select>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th> Name</th>
                                         <th>From</th>
                                         <th>To</th>

                                        <th>Package Name</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($s_viewgroups as $v) {
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="u_pack_name"><?php echo $v->groupname; ?></td>
                                            <td id="u_pack_desc"><?php echo $v->groupfrom; ?></td>

                                            <td id="u_pack_desc"><?php echo $v->groupto; ?></td>


                                            <td id="u_pack_desc"><?php echo $v->grouppackageidname; ?></td>


                                            <td>

                                                <a class="btn btn-primary" href="<?= base_url('admin/Groups/Groupsupdate/')."/".$v->group_id ?>">
                                                  Edit  <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/Groups/GroupsDelete1/')."/".$v->group_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                                <a href="<?= base_url('admin/Groups/Groupstatus/'.$v->group_id .'/'.$v->groupstatus.'') ?>"   class="btn btn-primary"><?php echo $v->groupstatus == 0 ? "Inactive": "Active"; ?></a>





                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function checkfrom()
    {
       var f=$('#group_noofpersonfrom').val();
       $.ajax({
           url : '<?php echo base_url();  ?>admin/Groups/checkgroupfrom',
           type :'POST',
           data : { 'from' : f},
           success : function(data)
           {
               if(data==1)
               {
                   $('#group_noofpersonfrom').focus();
                   $('#group_noofpersonfrom').innerHTML='';
                   $(".from_error").html("Number Of Person From Already Exits.").css({ 'color': 'red' });
                   return false;
               }
               else
               {
                   $(".from_error").html("");
               }

           }

       });

    }


    function checkto()
    {

        var t=$('#group_noofpersonto').val();
        $.ajax({
            url : '<?php echo base_url();  ?>admin/Groups/checkgroupto',
            type :'POST',
            data : { 'to':t},
            success : function(data)
            {
                if(data==1)
                {
                    $('#group_noofpersonto').focus();
                    $('#group_noofpersonto').html='';
                    $(".to_error").html("Number Of Person To Already Exits.").css({ 'color': 'red' });
                    return false;
                }
                else
                {
                    $(".to_error").html(" ");
                }



            }

        });

    }

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


</script>



<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_taxes").validate({
            ignore: ".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                tax_name: "required",

                tax_percentage: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                $("#branchids").attr('disabled', false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    .btne_active {
        text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;
    }
    .sep_box h2{
        float:left;
        width:100%;
        padding-left:15px;
        font-size:20px;
        padding-bottom:10px;
    }
    .message_view{
    	float:left;
    	width:100%;
    	padding-left:15px;
    	line-height: 22px;
    	margin-bottom: 10px!important;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Edit Print Page</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                          
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li>
                                                <a href="<?php echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a>
                                            </li>
                                            <li><a href="<?php echo base_url('admin/Taxes/s_tax') ?>">Taxes</a></li>
                                            <li>
                                                <a href="<?php echo base_url('admin/packages/addPackage') ?>">Package</a>
                                            </li>
                                            <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                            </li>
                                            <li><a href="<?php echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily
                                                    Timeslot</a></li>
                                            <li><a href="<?php echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                            <li><a href="<?php echo base_url('admin/companys/s_addCompany') ?>">Add
                                                    Company</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                    Payment</a></li>
                                            <li><a
                                                        href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer
                                                    Data
                                                    Payment</a></li>
                                            <li class="active"><a
                                                        href="<?php echo base_url('admin/Prints/s_printpage') ?>">Printpage
                                                    </a></li>
                                        </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if ($this->session->flashdata('message')) {
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            
                        </div>

                        <form action="<?= base_url('admin/Prints/Printpageupdate') ?>" name="add_taxes" id="add_taxes"
                              method="post" enctype="multipart/form-data">

                            <div class="sep_box">
                                <p class="message_view">If Ticket Top Print From Right To Left :On Print Table Td Table ,Transform is 270 Margin Left  :130 ,Margin Top:10,Margin Right:0,Margin Bottom:0   </p>
                                <p class="message_view">If  Ticket Top Print From Left To right  :On Print Table Td Table ,Transform is 90 Margin Left  :70 ,Margin Top:0,Margin Right:0,Margin Bottom:10   </p></div>
                                <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Name<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                            <input type="text" required id="printsetting_name" name="printsetting_name"
                                                       value="<?php echo $s_viewtaxes->printsetting_name; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <h2>Print Table Css</h2>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Margin<span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtablemargin" name="printsetting_printtablemargin"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtablemargin; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                          
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Padding<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtablepadding" name="printsetting_printtablepadding"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtablepadding; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Border collapse<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtablebordercollapse" name="printsetting_printtablebordercollapse"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtablebordercollapse; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <h2>Print Table Td</h2>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Vertical Align<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtabletdvertical_align" name="printsetting_printtabletdvertical_align"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtabletdvertical_align; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Height<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtabletdheight" name="printsetting_printtabletdheight"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtabletdheight; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Padding<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" style="width:40%;margin-right:10%" required id="printsetting_printtabletdpadding" name="printsetting_printtabletdpadding"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtabletdpadding; ?>">


                                                <input type="text" style="width:40%;" required id="printsetting_printtabletdpadding1" name="printsetting_printtabletdpadding1"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtabletdpadding1; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <h2>Print Table Td Table</h2>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Transform<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtabletdrotate" name="printsetting_printtabletdrotate"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtabletdrotate; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Display<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_printtabletddisplay" name="printsetting_printtabletddisplay"
                                                       value="<?php echo $s_viewtaxes->printsetting_printtabletddisplay; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Width<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtablewidth" name="printsetting_print_tabletdtablewidth"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtablewidth; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Font Size<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtablefont_size" name="printsetting_print_tabletdtablefont_size"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtablefont_size; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Height<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtableheight" name="printsetting_print_tabletdtableheight"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtableheight; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Margin Left<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtablemargin_left" name="printsetting_print_tabletdtablemargin_left"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtablemargin_left; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Margin Top<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtablemargin_top" name="printsetting_print_tabletdtablemargin_top"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtablemargin_top; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Margin Right<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtablemargin_right" name="printsetting_print_tabletdtablemargin_right"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtablemargin_right; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Margin Bottom<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_print_tabletdtablemargin_bottom" name="printsetting_print_tabletdtablemargin_bottom"
                                                       value="<?php echo $s_viewtaxes->printsetting_print_tabletdtablemargin_bottom; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>


                            <div class="sep_box">
                                <h2>Print Table Td Table Td CSS</h2>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Padding<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" style="width:40%;margin-right:10%" required id="printsetting_padding" name="printsetting_padding"
                                                       value="<?php echo $s_viewtaxes->printsetting_padding; ?>">

                                                <input type="text" style="width:40%;" required id="printsetting_padding1" name="printsetting_padding1"
                                                       value="<?php echo $s_viewtaxes->printsetting_padding1; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Height<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_height" name="printsetting_height"
                                                       value="<?php echo $s_viewtaxes->printsetting_height; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Text Align<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_text_align" name="printsetting_text_align"
                                                       value="<?php echo $s_viewtaxes->printsetting_text_align; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Width<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_width" name="printsetting_width"
                                                       value="<?php echo $s_viewtaxes->printsetting_width; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <h2>Tables Data </h2>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Table Width<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_tablewidth" name="printsetting_tablewidth"
                                                       value="<?php echo $s_viewtaxes->printsetting_tablewidth; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> GST No<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_GSTNO" name="printsetting_GSTNO"
                                                       value="<?php echo $s_viewtaxes->printsetting_GSTNO; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Date<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_DATE" name="printsetting_DATE"
                                                       value="<?php echo $s_viewtaxes->printsetting_DATE; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> SESSION&nbsp;TIME<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_SESSIONTIME" name="printsetting_SESSIONTIME"
                                                       value="<?php echo $s_viewtaxes->printsetting_SESSIONTIME; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> SESSION&nbsp;EXIT<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_SESSIONEXIT" name="printsetting_SESSIONEXIT"
                                                       value="<?php echo $s_viewtaxes->printsetting_SESSIONEXIT; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> ENTRY FEES<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_ENTRYFEES" name="printsetting_ENTRYFEES"
                                                       value="<?php echo $s_viewtaxes->printsetting_ENTRYFEES; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Total &nbsp;AMOUNT<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_TotalAMOUNT" name="printsetting_TotalAMOUNT"
                                                       value="<?php echo $s_viewtaxes->printsetting_TotalAMOUNT; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> TRANS NO<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required id="printsetting_TRANSNO" name="printsetting_TRANSNO"
                                                       value="<?php echo $s_viewtaxes->printsetting_TRANSNO; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>












                            <input type="hidden" name="printsetting_id" value="<?php echo $s_viewtaxes->printsetting_id; ?>">

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit"
                                                       class="btn_button sub_btn"/>

                                                <a href="<?php echo base_url()."admin/Prints/s_printpage"; ?>" class="btn_button sub_btn backbtn" style="margin-left: 10px">Back</a>



                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



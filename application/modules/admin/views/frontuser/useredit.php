<?php //pend($vieww_employee); ?>


<!DOCTYPE html>
<html><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<style>
    .error{ color: red; }

    .tbl_input input[type="url"], .tbl_input input[type="number"], .tbl_input input[type="text"], .tbl_input input[type="email"], .tbl_input input[type="password"], .tbl_input select, .tbl_input textarea {
        padding: 9px 6px;
        border: solid 1px #E6E6E6;
        width: 100%;
    }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-inti="myFunc()">
<div data-ng-init="onloadFun()">
</div>
<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Edit Employee</h2>
                    </div>
                    <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section, you can add Employee!
                                <!-- <a href="admin/retailer/viewmanagers"><button type="button" style="float:right;">CANCEL</button></a> --></p>
                            <p><div class='flashmsg'>
                                <?php echo validation_errors(); ?>
                                <?php
                                if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                            </div></p>
                        </div>
                    </div>
                    <form action="" id="mgrForm" name="mgrForm" method="post" enctype="multipart/form-data" >
                        <div class="page_box">
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Name <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" title="Please fill valid details." name="s_username" id="s_username" required value="<?php echo $vieww_frontuser->user_firstname; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">USER ID <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input title="Please fill valid details." id="s_loginemail" type="text" name="s_loginemail" required value="<?php echo $vieww_frontuser->user_username; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Login Type <span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input" style="position: relative;">
                                                <select name="logintype">
                                                    <option value="all" <?php if ($vieww_frontuser->logintype == 'all') { ?> selected="selected"<?php } ?> >
                                                        BOTH
                                                    </option>
                                                    <option value="pos" <?php if ($vieww_frontuser->logintype == 'pos') { ?> selected="selected"<?php } ?> >
                                                        POS
                                                    </option>
                                                    <option value="addons" <?php if ($vieww_frontuser->logintype == 'addons') { ?> selected="selected"<?php } ?>>
                                                        ADDONS
                                                    </option>
                                                </select>
                                            </div>

                                        </div>



                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Permissions <span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input" style="position: relative;">
                                                <select name="mainmenu">

                                                    <?php  foreach ($vieww_permission as $mainmenu) {

                                                        ?>
                                                        <option value="<?php echo $mainmenu->userpermission_id; ?>"<?php if($mainmenu->userpermission_id==$vieww_frontuser->mainmenu) {?> selected <?php } ?> >
                                                            <?php echo $mainmenu->userpermission_name; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                        </div>



                                    </div>

                                </div>


                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input" style="position: relative;">
                                                <p id="msg"></p>
                                                <input type="text" title="Please fill valid details." name="s_loginpassword" id="s_loginpassword" autocomplete="off" required value="<?php echo base64_decode($vieww_frontuser->user_password); ?>"/>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Contact No <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">

                                                <input type="number" title="Please fill valid details." name="contact_no" id="contact_no" onkeyup="checknumber();" required value="<?php echo $vieww_frontuser->user_mobileno; ?>"/>

                                                <input type="hidden" title="Please fill valid details." name="uuid" value="<?php echo $vieww_frontuser->user_id; ?>"/>
                                                <!-- <input type="text" name="contact_no" id="contact_no" onkeyup="checknumber();" /> -->
                                            </div>
                                            <div class="tbl_input" id="errorsss"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="empoid"
                                   value="<?php echo base64_decode($this->uri->segment(4)); ?>"

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Tenent Id <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" title="Please fill valid Tenent Id." name="s_tenentid" id="s_tenentid" required value="<?php echo $vieww_frontuser->user_tenentid; ?>" minlength="1" maxlength="16"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input class="submitbtn" type="submit" name="submit" value="Submit"/>

                                                <a href="<?php echo base_url()."admin/user/reguser"; ?>" class="btn_button sub_btn backbtn" style="margin-left: 10px">Back</a>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>






                            </div>

                        </div>





                </div>
                </form>


            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</body>

</html>

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

<script>


    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#mgrForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                s_username: "required",
                empid: "required",
                s_loginemail: "required",

                contact_no: {
                    "required": true,
                    "number": true
                },
                countryid: "required",
                stateid: "required",
                cityid: "required",
                locationid: "required",

            },
            // Specify validation error messages
            messages: {
                s_username: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });


</script>

<script>
    $(function () {
        jQuery.noConflict();
        $('#mainmenu,#mainmenu_sub').select2({
            allowClear: true
        });
    });


    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $timeout) {

        $scope.onloadFun = function () {
            //alert(<?php echo base64_decode($this->uri->segment(4)); ?>);


            var data = $.param({
                roleid: 100,
                empid: <?php echo base64_decode($this->uri->segment(4)); ?>,
            });
            //alert(data);
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };
            $http.post('<?php echo base_url('admin/user/get_menu_submenu'); ?>', data, config)
                .then(function (data, status, headers, config) {
                    console.log(data.data['0'].submenu);
                    if (data != "") {
                        if (data.data['0'].mainmenu) {

                            var mmenuu = (data.data['0'].mainmenu).split(',');
                            var subbmenuu = (data.data['0'].submenu).split(',');
//console.log(data);
                            for (var i = 0; i < mmenuu.length; i++) {
                                $('#mainmenu option[value="' + mmenuu[i] + '"]').attr('selected', true);
                                //$("#mainmenu").trigger('change');
                            }


//alert();
                            var data = $.param({
                                mainmenuid: $("#mainmenu").val()
                            });
                            //alert(data);
                            var config = {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                                }
                            };
                            $http.post('<?php echo base_url('admin/managerole/getsubmenu'); ?>', data, config)
                                .then(function (data, status, headers, config) {
                                    console.log(data);
                                    $scope.data = data.data.vieww_submenu;
                                    //console.log(data.vieww_submenu['0'].id);
                                    for (var j = 0; j < subbmenuu.length; j++) {
                                        //console.log(subbmenuu[j]);
                                        //console.log($('#mainmenu_sub option'));
                                        //$('#mainmenu_sub option[value="' + subbmenuu[j] + '"]').attr('selected', true);
                                    }
                                });

                            $timeout(function () {
                                for (var j = 0; j < subbmenuu.length; j++) {
                                    //console.log(subbmenuu[j]);
                                    $('#mainmenu_sub option[value="' + subbmenuu[j] + '"]').attr('selected', true);
                                }
                                jQuery.noConflict();
                                $('#mainmenu,#mainmenu_sub').select2();
                            }, 1000);


                        }
                    }

                });
        };


        $scope.submenu = function () {
            var data = $.param({
                mainmenuid: $scope.mainmenu
            });
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };
            $http.post('<?php echo base_url('admin/managerole/getsubmenu'); ?>', data, config)
                .then(function (data, status, headers, config) {
                    //console.log(data.data.vieww_submenu);
                    $scope.data = data.data.vieww_submenu;
                });
        };


    });

</script>
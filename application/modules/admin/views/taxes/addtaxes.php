<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_taxes").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                tax_name: "required",

                tax_percentage: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                $("#branchids").attr('disabled',false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .btne_active{    text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;}
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add Taxes</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                            <li ><a href="<?php  echo base_url('admin/timeslots/addTimeslot') ?>">Timeslot</a></li>
                                            <li class="active"><a href="<?php  echo base_url('admin/Taxes/s_tax') ?>" >Taxes</a></li>
                                            <li><a href="<?php echo base_url('admin/packages/addPackage') ?>" >Package</a></li>
                                            <li><a href="<?php echo base_url('admin/packages/addaddons') ?>">Addons</a>
                                            </li>
                                            <li ><a href="<?php  echo base_url('admin/timeslots/dailyTimeslot') ?>">Daily Timeslot</a></li>
                                            <li ><a href="<?php  echo base_url('admin/cash/addcash') ?>">Cash</a></li>

                                            <li ><a href="<?php  echo base_url('admin/companys/s_addCompany') ?>">Add Company</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_printpage') ?>">Printpage
                                                    Payment</a></li>
                                            <li ><a
                                                        href="<?php echo base_url('admin/Taxes/s_footerpage') ?>">Footer Data
                                                    Payment</a></li>
                                            <li><a
                                                        href="<?php echo base_url('admin/Prints/s_printpage') ?>">Printpage
                                                </a></li>
                                            <li ><a  href="<?php echo base_url('admin/timeslotspackages/addTimeslotPackage') ?>">Manual Time slot</a></li>

                                        </ul>
                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form action="<?= base_url('admin/Taxes/s_tax') ?>" name="add_taxes" id="add_taxes" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Name<span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_name" name="tax_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box" style="display: none">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Tax Name 1</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_name1" name="tax_name1" value="CGST">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">CGST Percentage</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_percentage" name="tax_percentage" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box" style="display: none">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Tax Name 2</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_name2" name="tax_name2" value="SGST">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">SGST Percentage</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_percentage2" name="tax_percentage2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box" style="display: none">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Tax Name 3</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_name3" name="tax_name3" value="IGST">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">IGST Percentage</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="tax_percentage3" name="tax_percentage3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Tax Name</th>

                                        <th>CGST</th>

                                        <th>SGST</th>

                                        <th>IGST</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($s_viewtaxes as $v) {
                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="u_pack_name"><?php echo $v->tax_name; ?></td>
                                            <td id="u_pack_desc"><?php echo $v->tax_value; ?></td>

                                            <td id="u_pack_desc"><?php echo $v->tax_percentage2; ?></td>


                                            <td id="u_pack_desc"><?php echo $v->tax_percentage3; ?></td>


                                            <td>

                                                <a class="btn btn-primary" href="<?= base_url('admin/Taxes/Taxesupdate/')."/".$v->tax_id ?>">
                                                  Edit  <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/Taxes/TaxesDelete1/')."/".$v->tax_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                                <a href="<?= base_url('admin/Taxes/Taxesstatus/').'/'.$v->tax_id.'/'.$v->tax_status.'' ?>"
                                                   class="btn btn-primary">
                                                    <?php echo $v->tax_status==0 ? 'Inactive':'Active'  ?>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



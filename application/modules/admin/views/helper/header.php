<?php
if($this->session->userdata('snowworld')->LoginID == "")
{
    //pend($this->session->userdata);
    //redirect(base_url('admin/login'));
}
 //pend($this->session->userdata('snowworld')); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='shortcut icon' href="<?php echo base_url();?>assets/fevicon.png"/>
    <link href="<?php echo admin_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo admin_url();?>fontawsome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo admin_url();?>css/StyleSheet.css" rel="stylesheet" />
    <script type="text/javascript">
       var site_url="<?php echo base_url(); ?>";
    </script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="<?php echo admin_url();?>js/JavaScript.js"></script>
    <!-- for select2 js -->
     <link href="<?php echo base_url(); ?>assets/js/select/dist/css/select2.min.css" rel="stylesheet" />
     <script src="<?php echo base_url(); ?>assets/js/select/dist/js/select2.min.js"></script>
    <!-- for select2 js -->
<!-- datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

   <!--  <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <!-- datepicker -->
    <script type="text/javascript">
    $(document).ready(function () {
        console.log = function() {};
			 $(".menu-sm").click(function(){
		  $(".left_nav").toggleClass("active_mm");
        });
		 }); 
    </script>
    <!-- Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" HREF="<?php echo base_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          HREF="<?php echo base_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" HREF="<?php echo base_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" HREF="<?php echo base_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" HREF="<?php echo base_url(); ?>assets/favicon/favicon-16x16.png">
</head>
<body>
    <div class="wrapper">
        <div class="col-lg-12">
            <div class="row">
                <div class="top_header">
                    <div class="col-lg-4">
                     <span class="menu-sm hidden-lg"> <i class="fa fa-align-justify menu-sm-b"></i></span>
                        <a href="<?php echo  base_url();?>admin/login/dashboard" class="logo_admin">
                            
                        </a>
                    </div>
                    <div class="col-lg-8">
                  
                     <a class="user" href="<?php echo base_url();?>admin/login/logout" style="color: #444;font-size: 13px;padding: 14px 0px;float: right;text-decoration:none;margin-left:30px;">
                  <?php if(isset($this->session->userdata('admin')->UserName)){ echo 'Logout';}?>
                  <i class="fa fa-user" style="margin-right:5px;"></i>Logout</a>
                        <a href="<?php echo  base_url();?>admin/login/adminupdatepassword" style="color: #444;font-size: 13px;padding: 14px 0px;float: right;text-decoration:none;"><i class="fa fa-key"></i> Change Password</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
 
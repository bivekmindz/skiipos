<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Reportingusermi extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
    }


    /*Mi User Listing*/
    public function miuserdata(){
    if($this->input->post('submit') == 'Search'){
    $parameter = array('act_mode' => 'get_file_detail_date',
    'Param1' => $this->input->post('startdate'),
    'Param2' => $this->input->post('enddate'),
    'Param3' => '',
    'Param4' => '',
    'Param5' => '',
    'Param6' => '',
    'Param7' => '',
    'Param8' => '',
    'Param9' => ''
    );

   $response['getfiledetail'] = $this->supper_admin->call_procedure('proc_order_s', $parameter);

    }else{
     $parameter = array('act_mode' => 'get_file_detail',
        'Param1' => '',
        'Param2' => '',
        'Param3' => '',
        'Param4' => '',
        'Param5' => '',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => ''
        );
    $response['getfiledetail'] = $this->supper_admin->call_procedure('proc_order_s', $parameter);
 }
$this->load->view('helper/header');
$this->load->view('helper/nav');
$this->load->view('miuserfile/userfile',$response);
   }
  
   
  public function downloadFilename(){
        $filename = base_url().'temp/'.$this->input->get('q');
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false); // required for certain browsers 
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="'. basename($filename) . '";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit;
  }

  public function deleteFile(){
    $getid = $this->input->post('id');
    $parameter = array('act_mode' => 'update_file_data_status',
    'Param1' => $getid,
    'Param2' => '',
    'Param3' => '',
    'Param4' => '',
    'Param5' => '',
    'Param6' => '',
    'Param7' => '',
    'Param8' => '',
    'Param9' => ''
    );
$getcount = $this->supper_admin->call_procedure('proc_order_s', $parameter);
 if($getcount){
  echo 1;
 }else{
  echo 0;
 }
}
  
}// end class
?>
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Timeslots extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        $this->load->library('session');


    }

    public  function timeslotedit($id)
    {
        //pend($id);

        if($this->input->post('submit'))
        {
           // $branchid = $this->input->post('branchids');
            $from = $this->input->post('field_name1');
            $from_minute = $this->input->post('minute_name1');
            $to = $this->input->post('field_name2');
            $to_minute = $this->input->post('minute_name2');
            $seats = $this->input->post('no_of_seats');

            foreach($from as $a =>$b)
            {
                $parameter1 = array( 'act_mode'=>'s_edittimeslot',
                    'Param1'=>$id,
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>$seats[$a],
                    'Param5'=>$from_minute[$a],
                    'Param6'=>$to_minute[$a],
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'');
                //pend($parameter1);
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);

                redirect("admin/timeslots/addTimeslot?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

            }
        }


//        $parameter2 = array( 'act_mode'=>'s_viewbranch',
//            'Param1'=>'',
//            'Param2'=>'',
//            'Param3'=>'',
//            'Param4'=>'',
//            'Param5'=>'',
//            'Param6'=>'',
//            'Param7'=>'',
//            'Param8'=>'',
//            'Param9'=>'',
//            'Param10'=>'',
//            'Param11'=>'',
//            'Param12'=>'',
//            'Param13'=>'',
//            'Param14'=>'',
//            'Param15'=>'',
//            'Param16'=>'',
//            'Param17'=>'');
//        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'s_viewtimeslotval',
            'Param1'=>$id,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_timeslot_s',$parameter3);

       // pend($response['s_viewtimeslot']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/edittimeslot',$response);
    }


    public  function editdailytimeinvent()
    {
        if($this->input->post('submit'))
        {

            $from = $this->input->post('field_name1');
            $from_minute = $this->input->post('minute_name1');
            $to = $this->input->post('field_name2');
            $to_minute = $this->input->post('minute_name2');
            $seats = $this->input->post('no_of_seats');

            foreach($from as $a =>$b)
            {
                $parameter1 = array( 'act_mode'=>'s_edittimedailyslot',
                    'Param1'=>'',
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>$seats[$a],
                    'Param5'=>$from_minute[$a],
                    'Param6'=>$to_minute[$a],
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>$this->input->post('updateid'));
                //p($parameter1);
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);
                redirect("admin/timeslots/dailyTimeslot");

            }
        }




        $parameter3 = array( 'act_mode'=>'s_viewdailytimeslotval',
            'Param1'=>base64_decode($_GET['id']),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_timeslot_s',$parameter3);
     //pend($response['s_viewtimeslot']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/editdailytimeslot',$response);
    }



    public  function addTimeslot()
    {

        if($this->input->post('submit'))
        {
            $branchid = $this->input->post('branchids');
            $from = $this->input->post('field_name1');
             $from_minute = $this->input->post('minute_name1');
            $to = $this->input->post('field_name2');
              $to_minute = $this->input->post('minute_name2');
             $seats = $this->input->post('no_of_seats');
            foreach($from as $a =>$b)
            {
                $parameter11 = array( 'act_mode'=>'s_checktimeslot',
                    'Param1'=>$branchid,
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'');
                //p($parameter1);
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter11);
                if($response[0]->cou > 0)
                {
                    continue;
                }
                else
                {
                $parameter1 = array( 'act_mode'=>'s_addtimeslot',
                    'Param1'=>$branchid,
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>$seats[$a],
                    'Param5'=>$from_minute[$a],
                    'Param6'=>$to_minute[$a],
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'');
                //p($parameter1);
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);
                }
            }
        }

        $parameter3 = array( 'act_mode'=>'s_viewtimeslot',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter3);
   
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/timeslot',$response);
    }





    public  function dailyTimeslot()
    {
        $parameterselectdate=array(
            'act_mode' =>'selectdateadmin',
            'nextDate' =>'',
            'starttime' =>'',
            'branchid' =>'',

            'dailyinventory_to' =>'',
            'dailyinventory_seats' =>'',
            'dailyinventory_status' =>'',
            'dailyinventory_createdon' =>'',
            'dailyinventory_modifiedon' =>'',
            'dailyinventory_minfrom' =>'',
            'dailyinventory_minto' =>'',


        );

        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_crone_v',$parameterselectdate);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/dailytimeslot',$response);
    }

    public function deletesingletime()
    {

        $parameter3 = array( 'act_mode'=>'deletesingletime',
            'Param1'=>$this->input->post('id'),
            'Param2'=>$this->input->post('from'),
            'Param3'=>$this->input->post('to'),
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter3);


    }
}// end class
?>
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller
{
    public function __construct()
    {

        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        $this->load->library('session');

    }

    public function index()
    {
        if($this->input->post('submit'))
        {
            $parameter= array('act_mode'=>'count_order_by_month_date',
                'Param1'=>date('Y-m-d',strtotime($this->input->post('datepicker1'))),
                'Param2'=>date('Y-m-d',strtotime($this->input->post('datepicker2'))),
                'Param3'=>'',
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>''
            );
            $response['data']=$this->supper_admin->call_procedure('proc_order_s',$parameter);
        }
        else
        {
            $parameter= array('act_mode'=>'count_order_by_month',
                'Param1'=>'',
                'Param2'=>'',
                'Param3'=>'',
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>''
            );
            $response['data']=$this->supper_admin->call_procedure('proc_order_s',$parameter);

        }



        $parameter= array('act_mode'=>'total_offline_order',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>''
            );
        $response['offline_order']=$this->supper_admin->call_procedureRow('proc_order_s',$parameter);

        $parameter= array('act_mode'=>'total_online_order',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        $response['online_order']=$this->supper_admin->call_procedureRow('proc_order_s',$parameter);

        $parameter= array('act_mode'=>'total_offline_trans_amount',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        $response['offlineamount']=$this->supper_admin->call_procedureRow('proc_order_s',$parameter);

        $parameter= array('act_mode'=>'total_online_trans_amount',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        $response['onlineamount']=$this->supper_admin->call_procedureRow('proc_order_s',$parameter);

        $parameter= array('act_mode'=>'total_users',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        $response['totalusers']=$this->supper_admin->call_procedureRow('proc_order_s',$parameter);

      // pend($response['countorder']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('user/dashboard',$response);
    }


}
<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Prints extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
 $this->load->library('session');

        //$this->userfunction->loginAdminvalidation();
    }




    public function Printpageupdate()
    {
        if ($this->input->post('submit')) {




                $parameter11 = array('act_mode' => 's_editprintpayment',
                    'Param1' => $this->input->post('printsetting_printtablemargin'),
                    'Param2' => $this->input->post('printsetting_printtablepadding'),
                    'Param3' => $this->input->post('printsetting_printtablebordercollapse'),
                    'Param4' => $this->input->post('printsetting_printtabletdvertical_align'),
                    'Param5' => $this->input->post('printsetting_printtabletdheight'),
                    'Param6' => $this->input->post('printsetting_printtabletdpadding'),
                    'Param7' => $this->input->post('printsetting_printtabletdrotate'),
                    'Param8' => $this->input->post('printsetting_printtabletddisplay'),
                    'Param9' => $this->input->post('printsetting_print_tabletdtablewidth'),
                    'Param10' => $this->input->post('printsetting_print_tabletdtablefont_size'),
                    'Param11' => $this->input->post('printsetting_print_tabletdtableheight'),
                    'Param12' => $this->input->post('printsetting_print_tabletdtablemargin_left'),
                    'Param13' => $this->input->post('printsetting_print_tabletdtablemargin_top'),
                    'Param14' => $this->input->post('printsetting_padding'),
                    'Param15' => $this->input->post('printsetting_height'),
                    'Param17' => $this->input->post('printsetting_text_align'),
                    'Param18' => $this->input->post('printsetting_width'),
                    'Param19' => $this->input->post('printsetting_GSTNO'),
                    'Param20' => $this->input->post('printsetting_DATE'),
                    'Param21' => $this->input->post('printsetting_SESSIONTIME'),
                    'Param22' => $this->input->post('printsetting_SESSIONEXIT'),
                    'Param23' => $this->input->post('printsetting_ENTRYFEES'),
                    'Param24' => $this->input->post('printsetting_TotalAMOUNT'),
                    'Param25' => $this->input->post('printsetting_TRANSNO'),
                    'Param26' => $this->input->post('printsetting_name'),
                    'Param27' => $this->input->post('printsetting_printtabletdpadding1'),
                    'Param28' => $this->input->post('printsetting_padding1'),
                    'Param29' => $this->input->post('printsetting_tablewidth'),
                    'Param30' => $this->input->post('printsetting_id'),
                    'Param31' =>  $this->input->post('printsetting_print_tabletdtablemargin_right'),
                    'Param32' =>  $this->input->post('printsetting_print_tabletdtablemargin_bottom'),
                    'Param33' => '',
                    'Param34' => '',
                    'Param35' => '',
                    'Param16' => '',
                   );

                $response = $this->supper_admin->call_procedure('proc_printingpage_s', $parameter11);

              redirect(base_url() . 'admin/Prints/s_printpage');
                // $this->session->set_flashdata('message', 'inserted sucessfully');


        }


        $parameter2 = array('act_mode' => 's_viewprintpageupdateval',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',

            'Param17' => '',
            'Param18' => '',
            'Param19' => '',
            'Param20' => '',
            'Param21' => '',
            'Param22' => '',
            'Param23' => '',
            'Param24' => '',
            'Param25' => '',
            'Param26' => '',
            'Param27' => '',
            'Param28' => '',
            'Param29' => '',
            'Param30' => $this->uri->segment('4'),
            'Param31' => '',
            'Param32' => '',
            'Param33' => '',
            'Param34' => '',
            'Param35' => '',
            'Param16' => '',
           );
        $response['s_viewtaxes'] = $this->supper_admin->call_procedurerow('proc_printingpage_s', $parameter2);


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('printpages/editPrintpageupdate', $response);

    }


    public function s_printpage()
    {


        $parameter2 = array('act_mode' => 's_viewprintpayment',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '',
            'Param20' => '',
            'Param21' => '',
            'Param22' => '',
            'Param23' => '',
            'Param24' => '',
            'Param25' => '',
            'Param26' => '',
            'Param27' => '',
            'Param28' => '',
            'Param29' => '',
            'Param30' => '',
            'Param31' => '',
            'Param32' => '',
            'Param33' => '',
            'Param34' => '',
            'Param35' => '',

                        );

        $response['s_viewprintpayment'] = $this->supper_admin->call_procedure('proc_printingpage_s', $parameter2);
        //pend($response['s_viewbranch']);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('printpages/addprintpage', $response);

    }



}// end class
?>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reportingcontrolticketsalereport extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
    }

    /*listing filter */
    public function order_sess()
    {

        if ($_POST['Clear'] == 'Show All Records') {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ", $this->input->post('filter_date_session'));
//p($a);
//p($b);

        if ($b['1'] == 'PM') {
            $session_time_param = $a['0'] + 12;
        } else {
            $session_time_param = $a['0'];
        }
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);
        $array = array('branchids' => $branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' => $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        $this->session->set_userdata('order_filter', $array);
        redirect('admin/reportingcontrolticketsalereport/order');


    }


    /*view all orders*/


    public function ordergeneratepdf()
    {
       ?>
        <form class="uiform">


            <table width="100%" border="0" style="border: 2px solid #d2d1d1;" cellspacing="0" cellpadding="5">
                <tr>
                    <td height="63" align="center" style="font-size:24px;" colspan="6">SKI INDIA - Ticket Sales Report</td>
                </tr>
                <tr>
                    <td height="43" align="center" style="font-size:18px;" colspan="6">Brand of Chiliad Procons</td>
                </tr>
                <tr>
                    <td height="43" style="font-size:18px;" colspan="6">&nbsp;</td>
                </tr>

                <tr>
                    <td height="40" bgcolor="#d2d1d1" colspan="6" align="center" style="font-weight: bold; margin-left:5px;" scope="col">Counter: <?php echo $vieww_dateval13; ?></td>

                </tr>
                <tr>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: bold;">Session Time</th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">No. of Tickets</th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Gross Sale</th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">CGST</th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">SGST</th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Entry Net Sale</th>

                </tr>
                <?php  $counterno=0;
                $countergross=0;
                $countercgst=0;
                $countersgst=0;
                $counternetsell=0;
                foreach ($vieww_ordercounter as $v) {
                    if ($v->dailyinventory_from > 12){$fromd=  $v->dailyinventory_from-12;$fromhrs="PM"; }
                    elseif ($v->dailyinventory_from == 12){$fromd=  $v->dailyinventory_from;$fromhrs="PM"; }
                    else{$fromd=  $v->dailyinventory_from;$fromhrs="AM"; }
                    if ($v->dailyinventory_to > 12){$tod=  $v->dailyinventory_to-12;$tohrs="PM"; }
                    elseif ($v->dailyinventory_to == 12){$tod=  $v->dailyinventory_to;$tohrs="PM"; }
                    else{$tod=  $v->dailyinventory_to;$tohrs="AM"; }

                    ?>
                    <tr>
                        <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php echo $fromd; ?>:<?php echo $v->dailyinventory_minfrom; ?> <?php echo $fromhrs; ?> - <?php echo $tod; ?>:<?php echo $v->dailyinventory_minto; ?> <?php echo $tohrs; ?></td>
                        <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php $counterno +=$v->prnum; echo $v->prnum; ?></td>
                        <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php $countergross +=($v->offlineprice-$v->offlinepriceCGST-$v->offlinepriceSGST);
                            echo number_format(($v->offlineprice-$v->offlinepriceCGST-$v->offlinepriceSGST),2); ?></td>
                        <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                            $countercgst+=$v->offlinepriceCGST;
                            echo number_format($v->offlinepriceCGST,2); ?></td>
                        <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                            $countersgst +=$v->offlinepriceSGST;
                            echo number_format($v->offlinepriceSGST,2); ?></td>
                        <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                            $counternetsell +=$v->offlineprice;
                            echo number_format($v->offlineprice,2); ?></td>

                    </tr>
                <?php } ?> <tr>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: bold;">Total Booking</th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo $counterno; ?></th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo $countergross;?></th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo number_format($countercgst,2); ?></th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo number_format($countersgst,2); ?></th>
                    <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo number_format($counternetsell,2); ?></th>

                </tr>
                </tbody></table>
            <div class="two fields">

                &nbsp;
            </div>
            <div class="three fields">
                <table width="100%" border="0" style="border: 2px solid #d2d1d1;" cellspacing="0" cellpadding="5">
                    <tbody><tr>
                        <td height="40" bgcolor="#d2d1d1" colspan="12" align="center" style="font-weight: bold; margin-left:5px;" scope="col">Online: <?php echo $vieww_dateval13; ?></td>

                    </tr>
                    <tr>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: bold;">Session Time</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">No. of Tickets</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Gross Sale</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">CGST</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">SGST</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Entry Net Sale</th>

                    </tr> <?php   $counterno=0;
                    $countergross=0;
                    $countercgst=0;
                    $countersgst=0;
                    $counternetsell=0;

                    foreach ($vieww_orderonline as $v) {
                        if ($v->dailyinventory_from > 12){$fromd=  $v->dailyinventory_from-12;$fromhrs="PM"; }
                        elseif ($v->dailyinventory_from == 12){$fromd=  $v->dailyinventory_from;$fromhrs="PM"; }
                        else{$fromd=  $v->dailyinventory_from;$fromhrs="AM"; }
                        if ($v->dailyinventory_to > 12){$tod=  $v->dailyinventory_to-12;$tohrs="PM"; }
                        elseif ($v->dailyinventory_to == 12){$tod=  $v->dailyinventory_to;$tohrs="PM"; }
                        else{$tod=  $v->dailyinventory_to;$tohrs="AM"; }

                        ?>
                        <tr>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php echo $fromd; ?>:<?php echo $v->dailyinventory_minfrom; ?> <?php echo $fromhrs; ?> - <?php echo $tod; ?>:<?php echo $v->dailyinventory_minto; ?> <?php echo $tohrs; ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php $counterno +=$v->pronline;
                                echo $v->pronline+0; ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countergross +=($v->onlineprice-($v->onlineprice*18/100));
                                echo number_format(($v->onlineprice-($v->onlineprice*18/100)),2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countercgst +=($v->onlineprice*9/100);
                                echo number_format(($v->onlineprice*9/100),2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php  $countersgst +=($v->onlineprice*9/100);
                                echo number_format(($v->onlineprice*9/100),2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $counternetsell +=$v->onlineprice;
                                echo number_format($v->onlineprice,2); ?></td>

                        </tr>
                    <?php } ?>
                    <tr>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: bold;">Total Booking</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo $counterno; ?></th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo $countergross;?></th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo number_format($countercgst,2); ?></th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo number_format($countersgst,2); ?></th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;"><?php echo number_format($counternetsell,2); ?></th>

                    </tr>
                    </tbody></table>
            </div>




            <div class="ui bottom attached segment">

            </div>


            <div class="field">
                &nbsp;
            </div>
            <div class="field">
                <table width="100%" border="0" style="border: 2px solid #d2d1d1;" cellspacing="0" cellpadding="5">
                    <tbody><tr>
                        <td height="40" bgcolor="#d2d1d1" colspan="12" align="center" style="font-weight: bold; margin-left:5px;" scope="col">Addons: <?php echo $vieww_dateval13; ?></td>

                    </tr>
                    <tr>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: bold;">Addons</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">No. of Tickets</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Gross Sale</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">CGST</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">SGST</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Entry Net Sale</th>

                    </tr>
                    <?php    $counterno=0;
                    $countergross=0;
                    $countercgst=0;
                    $countersgst=0;
                    $counternetsell=0;
                    foreach ($vieww_orderaddoneoffline as $v) {
                        ?>
                        <tr>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php echo $v->package_name; ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php $counterno +=$v->addonsqty;
                                echo $v->addonsqty; ?>
                                <?php if($v->complement>0) { ?>&nbsp; complement(<?php echo $v->complement; ?>)<?php } ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php $countergross +=($v->addonsprice-$v->addonscgst-$v->addonsgst);
                                echo number_format(($v->addonsprice-$v->addonscgst-$v->addonsgst),2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countercgst +=$v->addonscgst;
                                echo number_format($v->addonscgst,2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countersgst +=$v->addonsgst;
                                echo number_format($v->addonsgst,2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $counternetsell+= $v->addonsprice;
                                echo number_format($v->addonsprice,2); ?></td>

                        </tr>
                    <?php } ?>
                    <tr>
                        <th scope="col">Total Booking</th>
                        <th scope="col"><?php echo $counterno; ?></th>
                        <th scope="col"><?php echo number_format($countergross,2); ?></th>
                        <th scope="col"><?php echo number_format($countercgst,2); ?></th>
                        <th scope="col"><?php echo number_format($countersgst,2); ?></th>
                        <th scope="col"><?php echo number_format($counternetsell,2); ?></th>
                    </tr>
                    </tbody></table>
            </div>

            <div class="field">
                &nbsp;
            </div>
            <div class="field">
                <table width="100%" border="0" style="border: 2px solid #d2d1d1;" cellspacing="0" cellpadding="5">
                    <tbody><tr>
                        <td height="40" bgcolor="#d2d1d1" colspan="12" align="center" style="font-weight: bold; margin-left:5px;" scope="col">Advanced Ticket Sales: <?php echo $vieww_dateval13; ?></td>

                    </tr>
                    <tr>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align: center; font-weight: bold;">Session Time</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">No. of Tickets</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Gross Sale</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">CGST</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">SGST</th>
                        <th height="30" style="border: 2px solid #d2d1d1; text-align:center; font-weight: bold;">Entry Net Sale</th>

                    </tr>
                    <?php
                    $counterno=0;
                    $countergross=0;
                    $countercgst=0;
                    $countersgst=0;
                    $counternetsell=0;

                    foreach ($vieww_orderadvanced as $v) {
                        if ($v->dailyinventory_from > 12){$fromd=  $v->dailyinventory_from-12;$fromhrs="PM"; }
                        elseif ($v->dailyinventory_from == 12){$fromd=  $v->dailyinventory_from;$fromhrs="PM"; }
                        else{$fromd=  $v->dailyinventory_from;$fromhrs="AM"; }
                        if ($v->dailyinventory_to > 12){$tod=  $v->dailyinventory_to-12;$tohrs="PM"; }
                        elseif ($v->dailyinventory_to == 12){$tod=  $v->dailyinventory_to;$tohrs="PM"; }
                        else{$tod=  $v->dailyinventory_to;$tohrs="AM"; }

                        ?>
                        <tr>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php echo $fromd; ?>:<?php echo $v->dailyinventory_minfrom; ?> <?php echo $fromhrs; ?> - <?php echo $tod; ?>:<?php echo $v->dailyinventory_minto; ?> <?php echo $tohrs; ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php $counterno +=$v->prnum; echo ($v->prnum +0); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countergross +=($v->offlineprice-$v->offlinepriceCGST-$v->offlinepriceSGST);
                                echo number_format(($v->offlineprice-$v->offlinepriceCGST-$v->offlinepriceSGST),2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countercgst +=$v->offlinepriceCGST;
                                echo number_format($v->offlinepriceCGST,2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $countersgst +=$v->offlinepriceSGST;
                                echo number_format($v->offlinepriceSGST,2); ?></td>
                            <td height="30" style="border: 2px solid #d2d1d1;" align="center"><?php
                                $counternetsell +=$v->offlineprice;
                                echo number_format($v->offlineprice,2); ?></td>

                        </tr>
                    <?php } ?>
                    <tr>
                        <th scope="col">Total Booking</th>
                        <th scope="col"><?php echo $counterno; ?></th>
                        <th scope="col"><?php echo number_format($countergross,2); ?></th>
                        <th scope="col"><?php echo number_format($countercgst,2); ?></th>
                        <th scope="col"><?php echo number_format($countersgst,2); ?></th>
                        <th scope="col"><?php echo number_format($counternetsell,2); ?></th>
                    </tr>
                    </tbody></table>
            </div>

            <div class="field">
                &nbsp;
            </div>


        </form>

        <script src="assets/admin/js/lib/jquery.min.js">
        </script>
        <script type="text/javascript" src="assets/admin/js/lib/jspdf.min.js">
        </script>
        <script type="text/javascript" src="assets/admin/js/lib/html2canvas.min.js">
        </script>

        <script type="text/javascript" src="assets/admin/js/app.js">
        </script>
        <?php
    }
    public function order()
    {
//Date Select
        if( $this->session->userdata('datedataval')!='')
        {
            $dateval1=  $this->session->userdata('datedataval');
            $response['dateval1']=  $this->session->userdata('datedataval');
            $daten=explode("-",$this->session->userdata('datedataval'));
            $datedata2=$daten[2].'-'.$daten[1].'-'.$daten[0];
            $dateval2= $datedata2;
        }
        else

        {

            $response['dateval1']=date("Y-m-d");
            $dateval1=date("Y-m-d");
            $dateval2=date("d-m-Y");
        }





        //p($_POST);

        //$this->session->unset_userdata('order_filter');


        if ($this->session->userdata('order_filter')) {


            if( $this->session->userdata('order_filter')['filter_date_ticket']!='')
            {


                $daten=explode("/",$this->session->userdata('order_filter')['filter_date_ticket']);
                $datedata3=$daten[0].'-'.$daten[1].'-'.$daten[2];
                $dateval3= $datedata3;
                $dateval4=$daten[2].'-'.$daten[1].'-'.$daten[0];
            }
            else

            {


                $dateval3=date("d-m-Y");
                $dateval4=date("Y-m-d");
            }

            //p($this->session->userdata('order_filter'));
            $parameter1 = array('act_mode' => 'S_vieworderticketsalereport',
                'Param1' => $dateval1,
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' =>'',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10' => '',
                'Param11' => '',
                'Param12' => '',
                'Param13' => '',
                'Param14' => $dateval3,
                'Param15' => $dateval4,
                'Param16' =>'',
                'Param17' => '',
                'Param18' => '',
                'Param19' => '');
            foreach ($parameter1 as $key => $val) {
                if ($parameter1[$key] == '') {
                    $parameter1[$key] = -1;
                }
                //  echo $parameter1[$key];
            }
//pend($this->session->userdata('order_filter')['filter_date_ticket']);
            $response['vieww_ordercounter'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);
            //p($response['vieww_ordercounter']);
            $response['vieww_dateval13']=$this->session->userdata('order_filter')['filter_date_ticket'];

            $parameter2 = array('act_mode' => 'S_vieworderticketsalereportonline',
                'Param1' => $dateval1,
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $dateval2,
                'Param4' => '',
                'Param5' => '',
                'Param6' =>'',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10' => '',
                'Param11' => '',
                'Param12' => '',
                'Param13' => '',
                'Param14' => $dateval3,
                'Param15' => $dateval4,
                'Param16' =>'',
                'Param17' => '',
                'Param18' => '',
                'Param19' => '');
            foreach ($parameter2 as $key => $val) {
                if ($parameter2[$key] == '') {
                    $parameter2[$key] = -1;
                }
                //  echo $parameter1[$key];
            }
           // p($parameter2);
//p($this->session->userdata('order_filter')['filter_date_ticket']);
            $response['vieww_ordercounter'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);


            $response['vieww_orderonline'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter2);


            $parameter9 = array('act_mode' => 'S_filtervieworderadvanced',
                'Param1' => $dateval1,
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' =>'',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10' => '',
                'Param11' => '',
                'Param12' => '',
                'Param13' => '',
                'Param14' => $dateval4,
                'Param15' => $dateval4,
                'Param15' => '',
                'Param16' =>'',
                'Param17' => '',
                'Param18' => '',
                'Param19' => '');
            foreach ($parameter9 as $key => $val) {
                if ($parameter9[$key] == '') {
                    $parameter9[$key] = -1;
                }
                //  echo $parameter1[$key];
            }
            $response['vieww_orderadvanced'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter9);


            $parameter4 = array('act_mode' => 'S_filtervieworderaddoneoffline',
                'Param1' => $dateval1,
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' =>'',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',
                'Param10' => '',
                'Param11' => '',
                'Param12' => '',
                'Param13' => '',
                'Param14' => $dateval3,
                'Param15' => $dateval4,
                'Param16' =>'',
                'Param17' => '',
                'Param18' => '',
                'Param19' => '');
            foreach ($parameter4 as $key => $val) {
                if ($parameter4[$key] == '') {
                    $parameter4[$key] = -1;
                }
                //  echo $parameter1[$key];
            }
            $response['vieww_orderaddoneoffline'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter4);





            $this->session->unset_userdata('order_filter');
            $response['vieww_dateval1']=$dateval1;

        } else {

            $dateval1data=explode("/",$dateval1);

            $datenew=$dateval1data[1]."/".$dateval1data[0]."/".$dateval1data[2];
            $parameter1 = array('act_mode' => 'S_viewordercounter',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_ordercounter'] = $this->supper_admin->call_procedure('proc_reportingorder_s', $parameter1);

            $parameter1 = array('act_mode' => 'S_vieworderonline',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => $dateval2,
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_orderonline'] = $this->supper_admin->call_procedure('proc_reportingorder_s', $parameter1);

if($this->session->userdata('order_filter')['filter_date_ticket']!='')
{
    $response['vieww_dateval13']=$this->session->userdata('order_filter')['filter_date_ticket'];

}
else{
    $response['vieww_dateval13']=$dateval1;

}
           //$response['vieww_dateval1']=$dateval1;

            $parameter2 = array('act_mode' => 'S_vieworderadvanced',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_orderadvanced'] = $this->supper_admin->call_procedure('proc_reportingorder_s', $parameter2);

            //pend($response['vieww_order']);

            $parameter5 = array('act_mode' => 'S_vieworderaddoneoffline',
                'Param1' => $dateval1,
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_orderaddoneoffline'] = $this->supper_admin->call_procedure('proc_reportingorder_s', $parameter5);





        }


        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/ticketsalereport', $response);
    }


}// end class
?>
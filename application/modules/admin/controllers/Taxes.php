<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Taxes extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
 $this->load->library('session');

        //$this->userfunction->loginAdminvalidation();
    }



    public function Taxesupdate()
    {
        if ($this->input->post('submit')) {


            $this->form_validation->set_rules('tax_name', 'tax_name', 'required');
            $this->form_validation->set_rules('tax_percentage', 'tax_percentage', 'required');

            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_edittax',
                    'Param1' => $this->input->post('tax_name'),
                    'Param2' => $this->input->post('tax_percentage'),
                    'Param3' => $this->input->post('tax_name1'),
                    'Param4' => $this->input->post('tax_percentage2'),
                    'Param5' => $this->input->post('tax_name2'),
                    'Param6' => $this->input->post('tax_percentage3'),
                    'Param7' => $this->input->post('tax_name3'),
                    'Param8' => '',
                    'Param9' => $this->input->post('tax_id'));

                $response = $this->supper_admin->call_procedure('proc_taxes_s', $parameter11);

                redirect(base_url() . 'admin/Taxes/s_tax');
               // $this->session->set_flashdata('message', 'inserted sucessfully');

            }
        }



        $parameter2 = array('act_mode' => 's_viewtaxesupdateval',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->uri->segment('4'));
        $response['s_viewtaxes'] = $this->supper_admin->call_procedurerow('proc_taxes_s', $parameter2);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('taxes/edittaxes', $response);

    }

    public function Printpaymentupdate()
    {
        if ($this->input->post('submit')) {


            $this->form_validation->set_rules('tax_name', 'tax_name', 'required');
            $this->form_validation->set_rules('tax_percentage', 'tax_percentage', 'required');

            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_editprintpayment',
                    'Param1' => $this->input->post('tax_name'),
                    'Param2' => $this->input->post('tax_percentage'),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => $this->input->post('tax_id'));
//pend($parameter11);
                $response = $this->supper_admin->call_procedure('proc_printpayment_s', $parameter11);

                redirect(base_url() . 'admin/Taxes/s_printpage');
                // $this->session->set_flashdata('message', 'inserted sucessfully');

            }
        }


        $parameter2 = array('act_mode' => 's_viewprintpaymentupdateval',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => $this->uri->segment('4'));
        $response['s_viewtaxes'] = $this->supper_admin->call_procedurerow('proc_printpayment_s', $parameter2);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('taxes/editprintpayments', $response);

    }

    /*Add branch by zzz*/
    public function s_tax()
    {
        if ($this->input->post('submit')) {


            $this->form_validation->set_rules('tax_name', 'tax_name', 'required');

            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_addtax',
                    'Param1' => $this->input->post('tax_name'),
                    'Param2' => $this->input->post('tax_percentage'),
                    'Param3' => $this->input->post('tax_name1'),
                    'Param4' => $this->input->post('tax_percentage2'),
                    'Param5' => $this->input->post('tax_name2'),
                    'Param6' => $this->input->post('tax_percentage3'),
                    'Param7' => $this->input->post('tax_name3'),
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter11);
                $response = $this->supper_admin->call_procedure('proc_taxes_s', $parameter11);
                $this->session->set_flashdata('message', 'inserted sucessfully');

                }
            }



        $parameter2 = array('act_mode' => 's_viewtaxes',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtaxes'] = $this->supper_admin->call_procedure('proc_taxes_s', $parameter2);
       //pend($response['s_viewtaxes']);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('taxes/addtaxes', $response);

    }



    public function Taxesstatus($a,$b)
    {
        $b==1 ? $b= 0:$b= 1;
        $parameter2 = array('act_mode' => 'update_tax_status',
            'Param1' => $a,
            'Param2' => $b,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtaxes'] = $this->supper_admin->call_procedure('proc_taxes_s', $parameter2);
        redirect(base_url() . 'admin/Taxes/s_tax');



    }

    public function s_printpage()
    {
        if ($this->input->post('submit'))
        {

            $this->form_validation->set_rules('tax_name', 'tax_name', 'required');
            $this->form_validation->set_rules('tax_percentage', 'tax_percentage', 'required');

            if ($this->form_validation->run() != FALSE)
            {

                $parameter11 = array('act_mode' => 's_addprintpayment',
                    'Param1' => $this->input->post('tax_name'),
                    'Param2' => $this->input->post('tax_percentage'),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter11);
                $response = $this->supper_admin->call_procedure('proc_printpayment_s', $parameter11);
                $this->session->set_flashdata('message', 'inserted sucessfully');

            }
        }


        $parameter2 = array('act_mode' => 's_viewprintpayment',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewprintpayment'] = $this->supper_admin->call_procedure('proc_printpayment_s', $parameter2);
       // pend($response['s_viewprintpayment']);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('taxes/addprintpayment', $response);

    }

    public function Printpaymentstatus($a,$b)
    {

        $b==1? $b=0:$b=1;
        $parameter2 = array('act_mode' => 'update_print_payment',
            'Param1' => $a,
            'Param2' => $b,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter2 );
        $response['printp'] = $this->supper_admin->call_procedure('proc_printpayment_s', $parameter2);
        redirect(base_url() . 'admin/Taxes/s_printpage');


    }


    /*Delete package By zzz*/
    public function TaxesDelete1()
    {
        $parameter = array('act_mode' => 'delete_taxes',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_taxes_s', $parameter);
        redirect(base_url() . 'admin/Taxes/s_tax');

    }

    public function PrintpaymentDelete1()
    {
        $parameter = array('act_mode' => 'delete_printpage',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_printpayment_s', $parameter);
        redirect(base_url() . 'admin/Taxes/s_printpage');

    }



    /*DELETE BRANCH BY zzz*/
    public function branchdelete1()
    {
        $parameter = array('act_mode' => 'delete_branch',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter);
        redirect(base_url() . 'admin/branchs/addbranch');

    }

    /*UPDATE BRANCH BY zzz*/
    public function updatebranch()
    {
        echo $this->input->post('update_branch_id');
        echo $this->input->post('locationid_update');
        echo $this->input->post('companyid_update');
        echo $this->input->post('branch_name_update');

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('locationid_update', 'locationid', 'required');
            $this->form_validation->set_rules('companyid_update', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name_update', 'branch_name', 'required');
            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_updatebranch',
                    'Param1' => $this->input->post('locationid_update'),
                    'Param2' => $this->input->post('companyid_update'),
                    'Param3' => $this->input->post('branch_name_update'),
                    'Param4' => $this->input->post('update_branch_id'),
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                $this->session->set_flashdata('message', 'updated sucessfully');
                redirect(base_url() . 'admin/branchs/addbranch');

            }
            else {
                $this->session->set_flashdata('message', 'Not updated please try again');
                redirect(base_url() . 'admin/branchs/addbranch');
            }
        }
    }

     public  function branchstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 'branchstatus',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter1);
        redirect(base_url() . 'admin/branchs/addbranch');

    }

//footer data
    public function s_footerpage()
    {


        if ($this->input->post('submit_update'))
        {
            $this->form_validation->set_rules('com_name_update', 'name', 'required');
            if ($this->form_validation->run() != FALSE)
            {

                $parameter = array('act_mode' => 's_updatefooter',
                    'Param1' => $this->input->post('com_name_update'),
                    'Param2' => $this->input->post('com_id'),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter);
                $this->session->set_flashdata('message', 'Updated sucessfully');
            }
        }

        $parameter2 = array('act_mode' => 's_viewfooter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewfooter'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter2);

        //pend($response['s_viewbranch']);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('footer/addfooter', $response);

    }

//    public function footeredit($a)
//    {
//        $parameter2 = array('act_mode' => 'edit_footer',
//            'Param1' => base64_decode($a),
//            'Param2' => '',
//            'Param3' => '',
//            'Param4' => '',
//            'Param5' => '',
//            'Param6' => '',
//            'Param7' => '',
//            'Param8' => '',
//            'Param9' => '');
//        $response['s_viewfooter'] = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter2);
//        //pend($response['s_viewbranch']);
//
//        $this->load->view('helper/header');
//        $this->load->view('helper/nav');
//        $this->load->view('footer/editfooter', $response);
//
//    }


    public function footerstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter4 = array('act_mode' => 's_statusaddon_admin',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter4);
        $response = $this->supper_admin->call_procedure('proc_footerdata_s', $parameter4);
        redirect("admin/Taxes/s_footerpage");
    }

}// end class
?>
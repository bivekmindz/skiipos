<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Graphmanagement extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
     $this->load->library('session');
    
  }
  

  public function usergraph()
  {
      if($this->input->post('search')=='Search')
      {
          $paramater = array(
              'act_mode'=>'count_top10_order_by_date',
              'Param1'=>$this->input->post('datepicker1'),
              'Param2'=>$this->input->post('datepicker2'),
              'Param3'=>'',
              'Param4'=>'',
              'Param5'=>'',
              'Param6'=>'',
              'Param7'=>'',
              'Param8'=>'',
              'Param9'=>''
          );
         // pend($paramater);
          $data['result'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);

      }
      else
      {
          $paramater = array(
              'act_mode'=>'count_top10_order_by_user',
              'Param1'=>'',
              'Param2'=>'',
              'Param3'=>'',
              'Param4'=>'',
              'Param5'=>'',
              'Param6'=>'',
              'Param7'=>'',
              'Param8'=>'',
              'Param9'=>''
          );
         // pend($paramater);
          $data['result'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);

      }


      $paramater = array(
          'act_mode'=>'count_total_order',
          'Param1'=>'',
          'Param2'=>'',
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>''
      );
      //pend($param);
      $data['countorder'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);

      $paramater = array(
          'act_mode'=>'count_total_users',
          'Param1'=>'',
          'Param2'=>'',
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>''
      );
      //pend($param);
      $data['countuser'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);



        $this->load->view('helper/header');
        $this->load->view('helper/nav');
       $this->load->view('graph/graph',$data);

  }

}//end class
?>
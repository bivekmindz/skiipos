<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Reportingcontrol extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
    }

    /*listing filter */
    public function b2creports_sess(){

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));
//p($a);
//p($b);

        if ($b['1'] == 'PM') {
            $session_time_param = $a['0'] + 12;
        } else {
            $session_time_param = $a['0'];
        }
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/reportingcontrol/b2creports');


    }

  public function order_sess(){

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));
//p($a);
//p($b);

        if ($b['1'] == 'PM') {
            $session_time_param = $a['0'] + 12;
        } else {
            $session_time_param = $a['0'];
        }
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/reportingcontrol/order');


    }
    /*view all orders*/
    public function order()
    {

        //p($_POST);

        //$this->session->unset_userdata('order_filter');

        if($this->session->userdata('order_filter'))
        {
            //p($this->session->userdata('order_filter'));
            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => '');
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
                //  echo $parameter1[$key];
            }

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);
            //p($response['vieww_order']);
            $this->session->unset_userdata('order_filter');



        }
        else {


            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
//pend($response['vieww_order']);

        }


        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/orderposlist', $response);
    }


    public function orderview()
    {
        $parameter = array('act_mode' => 'vieworderdet',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_orderdetailnew'] = $this->supper_admin->call_procedurerow('proc_order_s', $parameter);


        $parameter = array('act_mode' => 'vieworderdet',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_orderdetail'] = $this->supper_admin->call_procedurerow('proc_order_s', $parameter);

        //p($response['vieww_orderdetail']);
        $parameter = array('act_mode' => 'vieworderPackages',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_orderdetail'] = $this->supper_admin->call_procedure('proc_order_s', $parameter);

        $parameter = array('act_mode' => 'updatetotal',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_ordertotal'] = $this->supper_admin->call_procedure('proc_order_s', $parameter);
        $parameter = array('act_mode' => 'updateorderprice',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' =>$response['vieww_ordertotal'][0]->price,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_ordertotal'] = $this->supper_admin->call_procedure('proc_order_s', $parameter);



        //p(  $response['vieww_orderdetail'] );
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/orderview',$response);
    }



    public function refundauditreport()
    {
      
if(($this->input->post('filter_date_session_from')!=''))
{
        $arrdatecurrent=$this->input->post('filter_date_session_from');
        $response['currentdate']=$this->input->post('filter_date_session_from');

    }
    else
    {
        $arrdatecurrent=date("Y-m-d");
        $response['currentdate']=date("Y-m-d");
    }


if(($this->input->post('filter_date_session_to')!=''))
{
        $arrdatefuture=$this->input->post('filter_date_session_to');
        $response['futuredate']=$this->input->post('filter_date_session_to');

    }
    else
    {
       $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));
        $response['futuredate']=$arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));;
    }


// $arrdatecurrent=date("Y-m-d");
// $arrdatecurrent=date("2018-02-21");
        $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));

 $parameter1 = array('act_mode' => 'S_refundaudit',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_refundaudit'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);

       
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/refundauditreport', $response);
    }








 public function salebysessionreport()
    {

        $arrdatecurrent=date("Y-m-d");

        $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));

$start_date = strtotime($arrdatecurrent);
$end_date = strtotime("+7 day", $start_date);
$arrdatefuturesevendays= date('Y-m-d', $end_date);



        $parameter1 = array('act_mode' => 'S_salebysessionreport',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' => $arrdatefuturesevendays,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_salebysessionreport'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/salebysessionreport', $response);
    }

public function cashiersessionreport()
    {
//p($_POST); exit;
if(($this->input->post('filter_date_ticket')!=''))
{
        $arrdatecurrent=$this->input->post('filter_date_ticket');
        $response['currentdate']=$this->input->post('filter_date_ticket');

    }
    else
    {
        $arrdatecurrent=date("Y-m-d");
        $response['currentdate']=date("Y-m-d");
    }

// $arrdatecurrent=date("Y-m-d");
// $arrdatecurrent=date("2018-02-21");
        $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));

 $parameter1 = array('act_mode' => 'S_cashiersessionreport',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_cashiersessionreport'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);


       // echo "fff"; exit;
    	//  pend($response['vieww_cashiersessionreport']);
 
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/cashiersessionreportn', $response);
    }
public function cashiershiftport()
    {



if(($this->input->post('filter_date_session_from')!=''))
{
    $arrdatecurrent=$this->input->post('filter_date_session_from');
    $response['startdate']=$this->input->post('filter_date_session_from');
}else{ $arrdatecurrent=date("Y-m-d"); 
$response['startdate']=date("Y-m-d");
}

if(($this->input->post('filter_date_session_to')))
{$arrdatefuture= $this->input->post('filter_date_session_to');
  $response['enddate']=$this->input->post('filter_date_session_to');
}else{ $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent)); 
 $response['enddate']=date("Y-m-t", strtotime($arrdatecurrent)); 
}

     


 $parameter1 = array('act_mode' => 'S_cashiershiftport',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_refundaudit'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/cashiershiftport', $response);
    }

   public function cashiershiftreportsummarytype()
    {

//pend($_POST);
        if(($this->input->post('filter_date_session_from')!=''))
{
    $arrdatecurrent=$this->input->post('filter_date_session_from');
$response['startdate'] = $this->input->post('filter_date_session_from');
}
else{
 $arrdatecurrent=date("Y-m-d");
 $response['startdate'] = date("Y-m-d");
  }

if(($this->input->post('filter_date_session_to')))
{
    $arrdatefuture= $this->input->post('filter_date_session_to');
    $response['enddate'] =  $this->input->post('filter_date_session_to');
}else{ 
    $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));
        $response['enddate'] =  date("Y-m-t", strtotime($arrdatecurrent));
     }

 $parameter1 = array('act_mode' => 'S_cashiershiftreportsummarytype',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_cashiershiftreport'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);


    	  //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/cashiershiftreportsummarytype', $response);
    } 
 public function concessionitemsales()
    {
    	  //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/concessionitemsales', $response);
    } 

public function concessionsalebydaily()
    {
    	  //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/concessionsalebydaily', $response);
    } 


public function userauditbytime()
    {



//pend($_POST);
        if(($this->input->post('filter_date_ticket')!=''))
{
    $arrdatecurrent=$this->input->post('filter_date_ticket');
$response['startdate'] = $this->input->post('filter_date_ticket');
}
else{
 $arrdatecurrent=date("Y-m-d");
 $response['startdate'] = date("Y-m-d");
  }


  if(($this->input->post('uid')!=''))
{
$uidd=$this->input->post('uid');
//pend($this->input->post('uid'));
}
else
{
$uidd=base64_decode($this->uri->segment('4'));
}



 $parameter1 = array('act_mode' => 'S_userauditbytime',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' =>$uidd,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_userauditbytime'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
 $parameter2 = array('act_mode' => 'S_userdata',
            'Param1' => $uidd,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_userdata'] = $this->supper_admin->call_procedurerow('proc_order_s', $parameter2);

    	  //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/userauditbytime', $response);
    } 

    public function useraudit()
    {

          $parameter1 = array('act_mode' => 's_viewfrontuser',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
      
        $response['vieww_frontuser'] = $this->supper_admin->call_procedure('proc_frontuser_s', $parameter1);
         
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/useraudit', $response);
    } 



    public function userauditbytransaction()
    {


   if(($this->input->post('filter_date_session_from')!=''))
{
    $arrdatecurrent=$this->input->post('filter_date_session_from');
$response['startdate'] = $this->input->post('filter_date_session_from');
}
else{
 $arrdatecurrent=date("Y-m-d");
 $response['startdate'] = date("Y-m-d");
  }

if(($this->input->post('filter_date_session_to')))
{
    $arrdatefuture= $this->input->post('filter_date_session_to');
    $response['enddate'] =  $this->input->post('filter_date_session_to');
}else{ 
    $arrdatefuture= date("Y-m-t", strtotime($arrdatecurrent));
        $response['enddate'] =  date("Y-m-t", strtotime($arrdatecurrent));
     }


  if(($this->input->post('uid')!=''))
{
$uidd=$this->input->post('uid');
//pend($this->input->post('uid'));
}
else
{
$uidd=base64_decode($this->uri->segment('4'));
}



 $parameter1 = array('act_mode' => 'S_userauditbytransaction',
            'Param1' => $arrdatecurrent,
            'Param2' => $arrdatefuture,
            'Param3' =>$uidd,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_userauditbytransaction'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
 $parameter2 = array('act_mode' => 'S_userdata',
            'Param1' => $uidd,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_userdata'] = $this->supper_admin->call_procedurerow('proc_order_s', $parameter2);


    	  //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/userauditbytransaction', $response);
    } 
 public function b2creports()
    {

           if($this->session->userdata('order_filter'))
        {
          //p($this->session->userdata('order_filter'));
                 $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                 'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => '');
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
             //  echo $parameter1[$key];
            }

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_onlineorder_filter_s', $parameter1);

          $this->session->unset_userdata('order_filter');



        }
        else {


            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_onlineorder_s', $parameter1);
//pend($response['vieww_order']);

        }



    	  //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('reporting/b2creports', $response);
    } 


}// end class
?>
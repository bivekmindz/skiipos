
app.config(function($routeProvider) {
    $routeProvider

        .when("/", {
            templateUrl : "assets/frontend/templates/pos-screen.php",
            controller: "PosCntrl",
        })
        .when("/addons", {
            templateUrl : "assets/frontend/templates/addons-screen.php",
            controller: "AddonsCntrl"
        })
        .when("/print", {
            templateUrl : "assets/frontend/templates/printval-screen.php",
            controller: "PrintpageCntrl"
        })
       //.when("/totalbooking", {
         //  templateUrl : "assets/frontend/templates/totalbooking-screen.php",
         //  controller: "TotalCntrl",
    //  });
});


